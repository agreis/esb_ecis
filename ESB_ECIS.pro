#-------------------------------------------------
#
# Project created by QtCreator 2013-06-24T15:49:00
#
#-------------------------------------------------

QT       += core gui
CONFIG   += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ESB_ECIS
    target.files = ESB_ECIS
    target.path = /home/root

INSTALLS += target other_files

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

QMAKE_CXXFLAGS_RELEASE += -O2

TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    dlgdatafile.cpp \
    dlgsoftwareinfo.cpp \
    dlgtestrecords.cpp \
    dlgtestsettings.cpp \
    dlgpassword.cpp \
    inputpanelcontext.cpp \
    inputpanel.cpp \
    testdata.cpp \
    Simple_GPIO.cpp \
    eciscontrol.cpp \
    teccontrol.cpp \
    dlgprecheck.cpp \
    dlgfilename.cpp \
    dlgpopupindicator.cpp \
    intmonitor.cpp \
    movingwindow.cpp \
    dlgresultsform.cpp \
    dlgruntimeerror.cpp \
    dlgtemperror.cpp \
    dlgsoftwareupdate.cpp \
    tempmeasure.cpp \
    dlgcontrolsettings.cpp \
    dlgrctest.cpp \
    dlgautoshutoff.cpp \
    miscio.cpp \
    powercontrol.cpp \
    dlgmessagebox.cpp \
    usbmounter.cpp \
    clockcontroliic.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    dlgdatafile.h \
    dlgsoftwareinfo.h \
    dlgtestrecords.h \
    dlgtestsettings.h \
    dlgpassword.h \
    inputpanelcontext.h \
    inputpanel.h \
    testdata.h \
    Simple_GPIO.h \
    eciscontrol.h \
    teccontrol.h \
    dlgprecheck.h \
    dlgfilename.h \
    dlgpopupindicator.h \
    intmonitor.h \
    movingwindow.h \
    dlgresultsform.h \
    dlgruntimeerror.h \
    dlgtemperror.h \
    dlgsoftwareupdate.h \
    tempmeasure.h \
    dlgcontrolsettings.h \
    dlgrctest.h \
    dlgautoshutoff.h \
    miscio.h \
    powercontrol.h \
    dlgmessagebox.h \
    usbmounter.h \
    clockcontroliic.h

FORMS    += mainwindow.ui \
    dlggraphoptions.ui \
    dlgdatafile.ui \
    dlgsoftwareinfo.ui \
    dlgtestrecords.ui \
    dlgtestsettings.ui \
    dlgpassword.ui \
    inputpanel.ui \
    dlgprecheck.ui \
    dlgfilename.ui \
    dlgpopupindicator.ui \
    dlgresultsform.ui \
    dlgruntimeerror.ui \
    dlgtemperror.ui \
    dlgsoftwareupdate.ui \
    dlgcontrolsettings.ui \
    dlgrctest.ui \
    dlgautoshutoff.ui \
    dlgmessagebox.ui \
    dlgvideotutorial.ui

OTHER_FILES += \
    Resources/3200.txt \
    Resources/chime.wav \
    Graphics/Nanohmics_about.bmp

#define other files
other_files.files = Resources
other_files.path = /home/root
