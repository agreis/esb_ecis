#include "clockcontroliic.h"

clockcontroliic::clockcontroliic()
{
    //Initialize time
    dateTime.date = 0;
    dateTime.hours = 0;
    dateTime.minutes = 0;
    dateTime.month = 0;
    dateTime.seconds = 0;
    dateTime.year = 0;

    readDateTime();
}

bool clockcontroliic::readDateTime()
{
    int file;
    char *filename = "/dev/i2c-1";

    if ((file = open(filename,O_RDWR)) < 0)
    {
        qDebug() << "Error opening i2c bus";
        return false;
    }

    if (ioctl(file,I2C_SLAVE,I2C_ADDRESS) < 0)
    {
        qDebug() << "Error opening i2c device";
        return false;
    }

    char buf[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    int numBytes = read(file,buf,16);

    if (numBytes < 16)
    {
        qDebug() << "Error reading i2c device. Only found " << numBytes << " bytes.";
        return false;
    }

    close(file);

    //Parse data

    char msb, lsb;

    //Byte 0 and 1 are status registers

    //Byte 2 is seconds (0 to 59)
    msb = buf[2] & 0x70;
    msb = msb >> 4;
    lsb = buf[2] & 0x0F;
    dateTime.seconds = 10*msb + lsb;

    //Byte 3 is minutes (0 to 59)
    msb = buf[3] & 0x70;
    msb = msb >> 4;
    lsb = buf[3] & 0x0F;
    dateTime.minutes = 10*msb + lsb;

    //Byte 4 is hours (0 to 23)
    msb = buf[4] & 0x30;
    msb = msb >> 4;
    lsb = buf[4] & 0x0F;
    dateTime.hours = 10*msb + lsb;

    //Byte 5 is date (1 to 31)
    msb = buf[5] & 0x30;
    msb = msb >> 4;
    lsb = buf[5] & 0x0F;
    dateTime.date = 10*msb + lsb;

    //Byte 7 is month (1 to 12)
    msb = buf[7] & 0x10;
    msb = msb >> 4;
    lsb = buf[7] & 0x0F;
    dateTime.month = 10*msb + lsb;

    //Byte 8 is year (0 to 99)
    msb = buf[8] & 0x10;
    msb = msb >> 4;
    lsb = buf[8] & 0x0F;
    dateTime.year = 10*msb + lsb;

    updateSystemTime();
    return true;

}

bool clockcontroliic::setDateTime(struct dt_clock *dt)
{
    char buf[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int temp;
    char msb, lsb;


    //Build the buffer
    //Write the start address
    buf[0] = 0x00;

    //seconds
    temp = dt->seconds/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    lsb = dt->seconds % 10;
    buf[3] = msb ^ lsb;

    //minutes
    temp = dt->minutes/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    lsb = dt->minutes % 10;
    buf[4] = msb ^ lsb;

    //hours
    temp = dt->hours/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    msb = msb & 0x30;
    lsb = dt->hours % 10;
    buf[5] = msb ^ lsb;

    //date
    temp = dt->date/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    msb = msb & 0x30;
    lsb = dt->date % 10;
    buf[6] = msb ^ lsb;

    //months
    temp = dt->month/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    msb = msb & 0x10;
    lsb = dt->month % 10;
    buf[8] = msb ^ lsb;

    //year
    temp = dt->year/10;
    msb = temp & 0x0F;
    msb = msb << 4;
    msb = msb & 0xF0;
    lsb = dt->year % 10;
    buf[9] = msb ^ lsb;

    //Write
    int file;
    char *filename = "/dev/i2c-1";

    if ((file = open(filename,O_RDWR)) < 0)
    {
        qDebug() << "Error opening i2c port";
        return false;
    }

    if (ioctl(file,I2C_SLAVE,I2C_ADDRESS) < 0)
    {
        qDebug() << "Error opening i2c device";
        return false;
    }

    int numBytes = write(file,buf,17);

    if (numBytes != 17)
    {
        qDebug() << "Error writing to i2c bus. " << numBytes << " bytes written.";
        return false;
    }

    close(file);

    readDateTime();

    return true;

}

//Called to update system time, which determines timestamps on files, etc.
void clockcontroliic::updateSystemTime()
{
    QString command = "date --set \"";
    command.append(QString::number(dateTime.month));
    command.append("/");
    command.append(QString::number(dateTime.date));
    command.append("/");
    command.append(QString::number(dateTime.year+2000));
    command.append(" ");
    command.append(QString::number(dateTime.hours));
    command.append(":");
    command.append(QString::number(dateTime.minutes));
    command.append(":");
    command.append(QString::number(dateTime.seconds));
    command.append("\"");

    QProcess proc;
    proc.execute(command);
    proc.waitForFinished(1000);
}
