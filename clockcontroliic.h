#ifndef CLOCKCONTROLIIC_H
#define CLOCKCONTROLIIC_H

#define I2C_BUS 1
#define I2C_ADDRESS 0x51

#include <QObject>
#include <QProcess>
#include <QDebug>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <stdio.h>
#include <iostream>


struct dt_clock
{
    int month;
    int date;
    int year;
    int hours;
    int minutes;
    int seconds;
};

class clockcontroliic : public QObject
{
    Q_OBJECT

public:
    clockcontroliic();

    dt_clock dateTime;

public slots:
    bool readDateTime();
    bool setDateTime(struct dt_clock *dt);

private:
    void updateSystemTime();

};

#endif // CLOCKCONTROLIIC_H
