#include "dlgautoshutoff.h"
#include "ui_dlgautoshutoff.h"

dlgAutoShutoff::dlgAutoShutoff(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgAutoShutoff)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame


    ui->setupUi(this);


    QColor bgc(254,111,95,255);
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),bgc);
    this->setPalette(pal);

    pal = ui->label->palette();
    pal.setColor(ui->label->backgroundRole(),bgc);
    ui->label->setPalette(pal);

    pal = ui->label_2->palette();
    pal.setColor(ui->label_2->backgroundRole(),bgc);
    ui->label_2->setPalette(pal);
    dlgAutoShutoff::setVisible(false);
}

dlgAutoShutoff::~dlgAutoShutoff()
{
    delete ui;
}

void dlgAutoShutoff::mousePressEvent(QMouseEvent * event)
{
    emit txUserAckShutoff();
}

void dlgAutoShutoff::displayAutoShutoffWin(bool show)
{
    dlgAutoShutoff::setVisible(show);
}
