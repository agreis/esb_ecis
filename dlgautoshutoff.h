#ifndef DLGAUTOSHUTOFF_H
#define DLGAUTOSHUTOFF_H

#include <QDialog>

namespace Ui {
class dlgAutoShutoff;
}

class dlgAutoShutoff : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgAutoShutoff(QWidget *parent = 0);
    virtual void mousePressEvent(QMouseEvent * event);
    ~dlgAutoShutoff();

signals:
    void txUserAckShutoff();

public slots:
    void displayAutoShutoffWin(bool show);


private:
    Ui::dlgAutoShutoff *ui;
};

#endif // DLGAUTOSHUTOFF_H
