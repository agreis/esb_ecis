#include "dlgcontrolsettings.h"
#include "ui_dlgcontrolsettings.h"
#include <QStandardItemModel>
#include <QTextStream>
#include <QDir>

dlgControlSettings::dlgControlSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgControlSettings)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame

    ui->setupUi(this);

    //Initialize models
    usbModel = new QStandardItemModel();
    localModel = new QStandardItemModel();

    populateLocalFiles();

    //Set focus properties
    ui->bttn_delete->setFocusPolicy(Qt::NoFocus);
    ui->bttn_exit->setFocusPolicy(Qt::NoFocus);
    ui->bttn_select->setFocusPolicy(Qt::NoFocus);
    ui->bttn_transfer->setFocusPolicy(Qt::NoFocus);

    //Set tab stylesheet
    ui->tabWidget->setStyleSheet("QTabBar::tab { height: 45px; width: 500px; }");

    usbCheckTimer = new QTimer(this);
    connect(usbCheckTimer,SIGNAL(timeout()),this,SLOT(usbTimerUpdate()));
    usbCheckTimer->start(1000);



    //Request USB drive status
    emit requestUsbUpdate();
}


dlgControlSettings::~dlgControlSettings()
{
    delete ui;
}

void dlgControlSettings::populateLocalFiles()
{

    QStandardItemModel *tempModel = new QStandardItemModel();
    ui->listView_local->setModel(tempModel);

    //Scan through files in resources directory and populate local files list box
    QDir localDir("/home/root/Resources/");
    QStringList filters;
    filters << "*.cfg";
    localDir.setNameFilters(filters);

    QStringList localFiles = localDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst);

    int i;
    localModel = new QStandardItemModel();
    ui->listView_local->setModel(localModel);


    QStandardItem *item;
    for (i = 0 ; i < localFiles.size(); i++)
    {
        item = new QStandardItem();
        item->setData(localFiles.at(i),Qt::DisplayRole);
        item->setEditable(false);
        localModel->appendRow(item);
    }

}

void dlgControlSettings::on_bttn_exit_clicked()
{
    emit txInteraction();
    usbCheckTimer->stop();
    dlgControlSettings::close();
}

//If user clicks "select"
void dlgControlSettings::on_bttn_select_clicked()
{
    emit txInteraction();

    QModelIndex index = ui->listView_local->currentIndex();
    int i = index.row();

    if (i == -1)
    {
        //error saving file
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Selection Error");
        msgBox->setInfo("No control configuration file has been selected.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }
    else
    {
        //User specified a file, make sure all associated data files are present

        //build local path name
        QVariant item = localModel->data(index);
        QString localName = "/home/root/Resources/" + item.toString();


        QVector<QVector<double> > data;
        QVector<QString> controlFiles;
        QFile file(localName);
        int i,j;

        //file check
        if (file.exists() == false)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Selection Error");
            msgBox->setInfo("Error opening files.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }


        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream fileStream(&file);

        QString rxStr = fileStream.readLine();

        while (QString::compare("*START*",rxStr)!=0)
            rxStr = fileStream.readLine();

        //Once start sequence is found, store file names in controlFiles until end is found
        i=0;
        rxStr = fileStream.readLine();
        while (QString::compare("*END*",rxStr)!=0)
        {
             controlFiles.resize(i+1);
             controlFiles[i] = "/home/root/Resources/" + rxStr;
             rxStr = fileStream.readLine();
             i++;
        }

         //Ensure that at least one control file was loaded
         if(controlFiles.size() == 0)
         {
             dlgMessageBox *msgBox= new dlgMessageBox(this);
             msgBox->setTitle("Control Configuration File Error");
             msgBox->setInfo("Configuration file lists no files.");
             msgBox->setOKButton(true,"Ok");
             msgBox->setCancelButton(false,"Cancel");
             msgBox->exec();
             return;
         }


         //Check for the presence of each control file
         for (i=0;i < controlFiles.size(); i++)
         {
             QFile tempFile(controlFiles[i]);

             if (tempFile.exists() == false)
             {
                 dlgMessageBox *msgBox= new dlgMessageBox(this);
                 msgBox->setTitle("Control Set Selection Error");
                 msgBox->setInfo("Data references from control configuration file are missing.");
                 msgBox->setOKButton(true,"Ok");
                 msgBox->setCancelButton(false,"Cancel");
                 msgBox->exec();
                 return;
             }
         }


         file.close();

         ui->txt_current->setText(item.toString());

         emit txControlName(item.toString());
    }
}

void dlgControlSettings::rxControlName(QString control)
{
    ui->txt_current->setText(control);
}

//Received USB Status
void dlgControlSettings::rxUsbUpdate(bool mounted)
{
    if (mounted)
    {
        //If USB drive is present

        //Populate usb data file list
        usbModel = new QStandardItemModel();
        usbModel->clear();
        ui->listView_usb->setModel(usbModel);

        QStringList filters;
        filters << "*.cfg";
        QDir usbDir("/home/root/usb/");
        usbDir.setNameFilters(filters);

        QStringList usbFiles = usbDir.entryList(QDir::NoDotAndDotDot |  QDir::Files, QDir::DirsFirst);
        usbModel->clear();

        int i;
        QStandardItem *item;
        for (i = 0 ; i < usbFiles.size(); i++)
        {
            item = new QStandardItem();
            item->setData(usbFiles.at(i),Qt::DisplayRole);
            item->setEditable(false);
            usbModel->appendRow(item);
        }

        ui->bttn_transfer->setEnabled(true);
    }
    else
    {
        usbModel->clear();
        ui->bttn_transfer->setEnabled(false);
    }

}

//Slot - When user selects to delete the currently selected file
void dlgControlSettings::on_bttn_delete_clicked()
{

    emit txInteraction();

    QModelIndex index = ui->listView_local->currentIndex();
    int i = index.row();

    if (i == -1)
    {
        //No file has been selected for copying
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Deletion Error");
        msgBox->setInfo("No control configuration file has been selected.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    //Confirm
    dlgMessageBox *msgBox= new dlgMessageBox(this);
    msgBox->setTitle("Confirm Control Dataset Deletion");
    msgBox->setInfo("Please confirm deletion of this control set.");
    msgBox->setOKButton(true,"Confirm");
    msgBox->setCancelButton(true,"Cancel");
    if(msgBox->exec() == false)
        return;

    //build local path name
    QVariant item = localModel->data(index);
    QString localName = "/home/root/Resources/" + item.toString();

    //if currently selected data set is in use
    if(item.toString().compare(ui->txt_current->toPlainText())==0)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Deletion Error");
        msgBox->setInfo("Cannot delete the currently selection control dataset.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    QVector<QVector<double> > data;
    QVector<QString> controlFiles;
    QFile file(localName);

    //file check
    if (file.exists() == false)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Deletion Error");
        msgBox->setInfo("Error parsing data files.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }


    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream fileStream(&file);

    QString rxStr = fileStream.readLine();

    while (QString::compare("*START*",rxStr)!=0)
        rxStr = fileStream.readLine();

    //Once start sequence is found, delete data files until end is found
    i=0;

    rxStr = fileStream.readLine();
    while (QString::compare("*END*",rxStr)!=0)
    {


        QFile localFile("/home/root/Resources/" + rxStr );

        localFile.remove();
        rxStr = fileStream.readLine();
        i++;
    }



    file.close();

    file.remove();

    //repopulate list
    populateLocalFiles();

}

//Slot - called whenever the current tab is changed
void dlgControlSettings::on_tabWidget_currentChanged(int index)
{

    emit txInteraction();
    /*

    //If the current tab is not the transfer tab
    if (index != 1)
        return;

    //If the current tab is the transfer tab, load list from USB

    //Check to see if USB drive is attached. Make default save file folder if not present
    //Check for local USB folder
    if(QDir("/home/root/usb/").exists() == false)
        QDir().mkdir("/home/root/usb/");

    //Check to make sure a device is attached
    if ((QFile("/dev/sda").exists()==false) && (QFile("/dev/sdb").exists()==false) && (QFile("/dev/sdc").exists()==false))
    {
        //No file has been selected for copying
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("USB Error");
        msgBox->setInfo("No USB storage device has been detected.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }
    else
    {
        //If USB drive is present

        if(QDir("/home/root/usb/").exists())
        {
            //Populate usb data file list
            usbModel = new QStandardItemModel();
            usbModel->clear();
            ui->listView_usb->setModel(usbModel);


            QStringList filters;
            filters << "*.cfg";
            QDir usbDir("/home/root/usb/");
            usbDir.setNameFilters(filters);

            QStringList usbFiles = usbDir.entryList(QDir::NoDotAndDotDot |  QDir::Files, QDir::DirsFirst);

            int i;
            QStandardItem *item;
            for (i = 0 ; i < usbFiles.size(); i++)
            {
                item = new QStandardItem();
                item->setData(usbFiles.at(i),Qt::DisplayRole);
                item->setEditable(false);
                usbModel->appendRow(item);
            }
        }
    }
    */

}

//Slot - when user chooses to copy selected USB control set to the instrument
void dlgControlSettings::on_bttn_transfer_clicked()
{
    emit txInteraction();

    QModelIndex index = ui->listView_usb->currentIndex();
    int i = index.row();

    if (i == -1)
    {
        //No file has been selected for copying
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Transfer Error");
        msgBox->setInfo("No control configuration file has been selected.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    //Read selected file and make sure it follows the correct format and has references to data files
    //build usb path name
    QVariant item = usbModel->data(index);
    QString usbName = "/home/root/usb/" + item.toString();

    QVector<QString> controlFiles;
    QFile file(usbName);

    //file check
    if (file.exists() == false)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Transfer Error");
        msgBox->setInfo("Error parsing data files.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }


    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream fileStream(&file);

    QString rxStr = fileStream.readLine();

    while (QString::compare("*START*",rxStr)!=0)
    {
        rxStr = fileStream.readLine();

        if (fileStream.atEnd() == true)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Transfer Error");
            msgBox->setInfo("Configuration file does not follow the correct format.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }
    }

    //Once start sequence is found, read data file names until end is reached
    i=0;


    rxStr = fileStream.readLine();
    while (QString::compare("*END*",rxStr)!=0)
    {

        //No *END* found
        if (fileStream.atEnd() == true)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Transfer Error");
            msgBox->setInfo("Configuration file does not follow the correct format.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }

        controlFiles.resize(i+1);
        controlFiles[i] = rxStr;
        rxStr = fileStream.readLine();
        i++;

    }

    file.close();

    //Make sure that at least one data file existed
    if (i < 1)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Transfer Error");
        msgBox->setInfo("Configuration file does not reference any data files.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    //Verify that all control data referenced by configuration file exists
    //Verify that none of the files exist locally
    int j;
    for(j=0;j<i;j++)
    {
        QFile usbFile("/home/root/usb/" + controlFiles[j] );
        QFile localFile("/home/root/Resources/" + controlFiles[j] );

        if (usbFile.exists() == false)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Transfer Error");
            msgBox->setInfo("Data files referenced in the configuration file are missing.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }
        if (localFile.exists() == true)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Transfer Error");
            msgBox->setInfo("Control data files referenced in the dataset already exist locally.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }
    }


    file.close();

    //Make sure control file does not already exist
    QString localName = "/home/root/Resources/" + item.toString();
    QFile localCfgFile(localName);

    if (localCfgFile.exists())
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Transfer Error");
        msgBox->setInfo("Control configuration file already exists locally.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }
    else
    {
        //copy over config file
        if(QFile::copy(usbName,localName) == false)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Control Set Transfer Error");
            msgBox->setInfo("File transfer error.");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
            return;
        }
    }

    bool err = false;
    //Copy over data files
    for(j=0;j<i;j++)
    {
        usbName = "/home/root/usb/" + controlFiles[j];
        localName = "/home/root/Resources/" + controlFiles[j];

        if(QFile::copy(usbName, localName) == false)
            err = true;

    }

    //If an error occured, remove any copied files and throw error
    if (err)
    {
        for(j=0;j<i;j++)
        {
            QFile localFile("/home/root/Resources/" + controlFiles[j]);

            if(localFile.exists())
            {
                localFile.remove();
            }
        }

        /*QFile usbControl("/home/root/usb/" + item.toString());

        if(usbControl.exists())
        {
            usbControl.remove();
        }
        */

        //error saving file
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Control Set Transfer Error");
        msgBox->setInfo("File transfer error.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    //If succesful, repopulate local file list
    populateLocalFiles();

    dlgMessageBox *msgBox= new dlgMessageBox(this);
    msgBox->setTitle("Control Set Transfer");
    msgBox->setInfo("Control dataset successfully transferred.");
    msgBox->setOKButton(true,"Ok");
    msgBox->setCancelButton(false,"Cancel");
    msgBox->exec();
}

void dlgControlSettings::mousePressEvent(QMouseEvent * event)
{
    emit txInteraction();
}

//Request an update on USB drive status
void dlgControlSettings::usbTimerUpdate()
{
    emit requestUsbUpdate();
}
