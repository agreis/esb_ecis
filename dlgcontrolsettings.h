#ifndef DLGCONTROLSETTINGS_H
#define DLGCONTROLSETTINGS_H

#include <QDialog>
#include <QTimer>
#include <QDir>
#include <QStandardItemModel>
#include "dlgmessagebox.h"

namespace Ui {
class dlgControlSettings;
}

class dlgControlSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgControlSettings(QWidget *parent = 0);
    ~dlgControlSettings();
    virtual void mousePressEvent(QMouseEvent * event);

public slots:
    void rxControlName(QString control);
    void rxUsbUpdate(bool mounted);
    void usbTimerUpdate();

signals:
    void txControlName(QString control);
    void txInteraction();
    void requestUsbUpdate();
    
private slots:
    void on_bttn_exit_clicked();
    void on_bttn_select_clicked();
    void on_bttn_delete_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_bttn_transfer_clicked();

private:
    void populateLocalFiles();
    Ui::dlgControlSettings *ui;
    QStandardItemModel *localModel;
    QStandardItemModel *usbModel;

    QTimer *usbCheckTimer;
};

#endif // DLGCONTROLSETTINGS_H
