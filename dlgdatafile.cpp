#include "dlgdatafile.h"
#include "ui_dlgdatafile.h"
#include <QStandardItemModel>
#include <QListView>
#include <QScrollBar>
#include <QDebug>

dlgDataFile::dlgDataFile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgDataFile)
{

    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame

    ui->setupUi(this);

    //Set scroll bar styles
    ui->lst_localFiles->verticalScrollBar()->setStyleSheet("QScrollBar::vertical { border: 1px; width: 50 px; margin: 42px 0 42px 0; } "
                                                           "QScrollBar::handle::vertical { min-height: 60 px; } "
                                                           "QScrollBar::sub-line:vertical { height: 40; subcontrol-position: top; subcontrol-origin: margin; } "
                                                           "QScrollBar::add-line:vertical { height: 40; subcontrol-position: bottom; subcontrol-origin: margin; }");

    ui->lst_usbFiles->verticalScrollBar()->setStyleSheet("QScrollBar::vertical { border: 1px; width: 50 px; margin: 42px 0 42px 0; } "
                                                         "QScrollBar::handle::vertical { min-height: 60 px; } "
                                                         "QScrollBar::sub-line:vertical { height: 40; subcontrol-position: top; subcontrol-origin: margin; } "
                                                         "QScrollBar::add-line:vertical { height: 40; subcontrol-position: bottom; subcontrol-origin: margin; }");

    //Set focus policy
    ui->bttn_Close->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Copy->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Delete->setFocusPolicy(Qt::NoFocus);

    //Install event filter for drag and drop
    ui->lst_usbFiles->viewport()->installEventFilter(this);

    //Scan through files in save directory and populate local files list box
    QDir localDir("/home/root/data/");
    QStringList localFiles = localDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst);

    int i;
    localModel = new QStandardItemModel();
    usbModel = new QStandardItemModel();
    ui->lst_localFiles->setModel(localModel);
    ui->lst_usbFiles->setModel(usbModel);

    QStandardItem *item;
    for (i = 0 ; i < localFiles.size(); i++)
    {
        item = new QStandardItem();
        item->setData(localFiles.at(i),Qt::DisplayRole);
        qDebug() << localFiles.at(i);
        item->setEditable(false);
        localModel->appendRow(item);
    }

    usbCheckTimer = new QTimer(this);
    connect(usbCheckTimer,SIGNAL(timeout()),this,SLOT(usbTimerUpdate()));
    usbCheckTimer->start(1000);

    //Request USB drive status
    emit requestUsbUpdate();

}

dlgDataFile::~dlgDataFile()
{
    delete ui;
}

void dlgDataFile::on_bttn_Close_clicked()
{
    emit txInteraction();

    usbCheckTimer->stop();
    dlgDataFile::close();
}

//User clicks on copy
void dlgDataFile::on_bttn_Copy_clicked()
{
    emit txInteraction();

    QModelIndexList lst = ui->lst_localFiles->selectionModel()->selectedIndexes();

    //Make a results list
    QList<int> copyResults;

    //Iterate through selected files
    for (int i=0;i<lst.length();i++)
    {
        //Attempt to copy each file, storing the results
        copyResults << copyFileToUsb(lst.at(i));
    }

    //Check to see if all copy operations were successful
    bool success = true;
    int numSuccess = 0;
    for(int i=0;i<copyResults.length();i++)
        if (copyResults[i] != ERR_NONE)
            success = false;
        else
           numSuccess++;

    if (success)
    {
        //error saving file
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Transfer Success");
        msgBox->setInfo("All " + QString::number(lst.length()) + " files were transferred successfully.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    else
    {
        //error saving file

        //Build error string
        QString errmssg = QString::number(copyResults.length()-numSuccess) + " / " + QString::number(copyResults.length()) + " file transfer failures: \n\n";
        for(int i=0;i<copyResults.length();i++)
            if (copyResults[i] != 0)
            {
                QVariant item = localModel->data(lst.at(i));
                errmssg.append(item.toString());

                switch(copyResults[i])
                {
                    case ERR_COPY:
                        errmssg.append("             \tError Copying File to USB.\n");
                        break;
                    case ERR_DIRECTORY:
                        errmssg.append("             \tError Accessing Data Directory.\n");
                        break;
                    case ERR_NO_FILE_DEFINED:
                        errmssg.append("             \tFile not found or defined.\n");
                        break;
                    case ERR_FILE_EXISTS:
                        errmssg.append("             \tFile already exists.\n");
                        break;
                }
            }

        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Transfer Error");
        msgBox->setInfo(errmssg);
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }

    //Repopulate USB file list

    QDir usbDir("/home/root/usb/ESB_DATA/");
    QStringList usbFiles = usbDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst);

    usbModel->clear();
    QStandardItem *newItem;

    for (int i = 0 ; i < usbFiles.size(); i++)
    {
        newItem = new QStandardItem();
        newItem->setData(usbFiles.at(i),Qt::DisplayRole);
        newItem->setEditable(false);
        usbModel->appendRow(newItem);
    }

    ui->lst_localFiles->clearSelection();

}

//If user selected delete local data file
void dlgDataFile::on_bttn_Delete_clicked()
{
    emit txInteraction();

    QModelIndexList lst = ui->lst_localFiles->selectionModel()->selectedIndexes();

    //Make a results list
    QList<int> deleteResults;

    //Iterate through selected files
    for (int i=0;i<lst.length();i++)
    {
        //Attempt to copy each file, storing the results
        deleteResults << deleteLocalFile(lst.at(i));
    }

    //Check to see if all copy operations were successful
    bool success = true;
    int numSuccess = 0;
    for(int i=0;i<deleteResults.length();i++)
        if (deleteResults[i] != ERR_NONE)
            success = false;
        else
           numSuccess++;

    if (success)
    {
        //error saving file
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("File Deletion Success");
        msgBox->setInfo("All " + QString::number(lst.length()) + " files were deleted successfully.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    else
    {
        //error deleting file

        //Build error string
        QString errmssg = QString::number(deleteResults.length()-numSuccess) + " / " + QString::number(deleteResults.length()) + " file deletion failures: \n\n";
        for(int i=0;i<deleteResults.length();i++)
            if (deleteResults[i] != 0)
            {
                QVariant item = localModel->data(lst.at(i));
                errmssg.append(item.toString());

                switch(deleteResults[i])
                {
                    case ERR_DELETE:
                        errmssg.append("             \tError Deleting File.\n");
                        break;
                }
            }

        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Deletion Error");
        msgBox->setInfo(errmssg);
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }

    //Repopulate local file list
    QDir localDir("/home/root/data/");
    QStringList localFiles = localDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst);

    localModel->clear();
    QStandardItem *newItem;

    for (int i = 0 ; i < localFiles.size(); i++)
    {
        newItem = new QStandardItem();
        newItem->setData(localFiles.at(i),Qt::DisplayRole);
        newItem->setEditable(false);
        localModel->appendRow(newItem);
    }

    ui->lst_localFiles->clearSelection();

}

void dlgDataFile::mousePressEvent(QMouseEvent * event)
{
    emit txInteraction();
}

//Receive the usb drive status
void dlgDataFile::rxUsbUpdate(bool mounted)
{
    if (mounted)
    {
        //If USB drive is present

        if(QDir("/home/root/usb/ESB_DATA/").exists())
        {
            //Populate usb data file list
            usbModel->clear();
            QDir usbDir("/home/root/usb/ESB_DATA/");
            QStringList usbFiles = usbDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
            QStandardItem *item;

            for (int i = 0 ; i < usbFiles.size(); i++)
            {
                item = new QStandardItem();
                item->setData(usbFiles.at(i),Qt::DisplayRole);
                item->setEditable(false);
                usbModel->appendRow(item);
            }
        }
        else
        {
            //Make directory
            QDir().mkdir("/home/root/usb/ESB_DATA/");
        }


        ui->bttn_Copy->setEnabled(true);
    }
    else
    {
        usbModel->clear();
        ui->bttn_Copy->setEnabled(false);
    }

}

//Request an update on USB drive status
void dlgDataFile::usbTimerUpdate()
{
    emit requestUsbUpdate();
}

//If user clicks on local file list adjust the button enable status based upon if a file is selected or not
void dlgDataFile::on_lst_localFiles_clicked(const QModelIndex &index)
{

    if (ui->lst_localFiles->selectionModel()->selectedIndexes().length() != 0)
    {
        ui->bttn_Copy->setEnabled(true);
        ui->bttn_Delete->setEnabled(true);
    }
    else
    {
        ui->bttn_Copy->setEnabled(false);
        ui->bttn_Delete->setEnabled(false);
    }

}

//Helper function - copies a file to the usb drive
int dlgDataFile::copyFileToUsb(const QModelIndex &index)
{
    int i = index.row();

    if (i == -1)
    {
        //No file has been selected for copying
        return ERR_NO_FILE_DEFINED;
    }
    else
    {
        //User specified a file to copy

        //build local path name
        QVariant item = localModel->data(index);
        QString localName = "/home/root/data/" + item.toString();
        QString usbName = "/home/root/usb/ESB_DATA/" + item.toString();


        //If directory does not exist, create
        if(QDir("/home/root/usb/ESB_DATA/").exists() == false)
        {
            if(!QDir().mkdir("/home/root/usb/ESB_DATA/"))
                return ERR_DIRECTORY;
        }

        //Check to see if file already exists
        if(QFile(usbName).exists())
                return ERR_FILE_EXISTS;

        if(QFile::copy(localName,usbName) == false)
            return ERR_COPY;
        else
            return ERR_NONE;


    }
}

int dlgDataFile::deleteLocalFile(const QModelIndex &index)
{

    //User confirmed delete
    QVariant item = localModel->data(index);
    QString localName = "/home/root/data/" + item.toString();
    QFile localFile(localName);

    if(localFile.remove() == false)
        return ERR_DELETE;
    else
        return ERR_NONE;

}
