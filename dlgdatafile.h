#ifndef DLGDATAFILE_H
#define DLGDATAFILE_H

#include <QDialog>
#include <QDir>
#include <QStandardItemModel>
#include <QTimer>
#include "dlgmessagebox.h"

#define ERR_NONE 0
#define ERR_COPY 1
#define ERR_FILE_EXISTS 2
#define ERR_NO_FILE_DEFINED 3
#define ERR_DIRECTORY 4
#define ERR_DELETE 5

namespace Ui {
class dlgDataFile;
}

class dlgDataFile : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgDataFile(QWidget *parent = 0);
    ~dlgDataFile();
    virtual void mousePressEvent(QMouseEvent * event);

public slots:
    void rxUsbUpdate(bool mounted);
    void usbTimerUpdate();

signals:
    void txInteraction();
    void requestUsbUpdate();
    
private slots:

    void on_bttn_Close_clicked();

    void on_bttn_Copy_clicked();

    void on_bttn_Delete_clicked();

    void on_lst_localFiles_clicked(const QModelIndex &index);

private:

    int copyFileToUsb(const QModelIndex &index);
    int deleteLocalFile(const QModelIndex &index);

    Ui::dlgDataFile *ui;
    QStandardItemModel *localModel;
    QStandardItemModel *usbModel;

    QTimer *usbCheckTimer;
};

#endif // DLGDATAFILE_H
