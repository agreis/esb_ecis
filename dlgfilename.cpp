#include "dlgfilename.h"
#include "ui_dlgfilename.h"
#include "clockcontroliic.h"
#include <QString>


dlgFileName::dlgFileName(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dlgFileName)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Read date and time for default file name

    clockCtrl = new clockcontroliic();
    clockCtrl->readDateTime();
    struct dt_clock dt = clockCtrl->dateTime;

    //Build filename string

    QString dtStr = QString::number(dt.date)+"-"+QString::number(dt.month)+
            "-"+QString::number(dt.year+2000) + " "+QString::number(dt.hours)+
            "_"+QString::number(dt.minutes)+" ESB";
    ui->line_sampleInfo->setText(dtStr);

    //Set Focus Policy
    ui->bttn_Accept->setFocusPolicy(Qt::NoFocus);

}

dlgFileName::~dlgFileName()
{
    delete ui;
}

void dlgFileName::on_bttn_Accept_clicked()
{

    if (QString::compare("",ui->line_sampleInfo->text())==0)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("File name Error");
        msgBox->setInfo("No file name was specified. Please specify a file name.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    else
        emit txSelectedName(ui->line_sampleInfo->text());
}

//Slot that receives bool indicating if datactrl was able to create specified save files
void dlgFileName::rxFileValidStatus(bool isValid)
{
    if (isValid)
    {
        //If file name was valid, close dialogue
        ui->bttn_Accept->setFocus();
        dlgFileName::close();
    }
    else
    {
        //If file name was invalid, reprompt for valid file name

        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("File Name Error");
        msgBox->setInfo("Invalid file name. Please specify another file name.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->line_sampleInfo->setText("");
    }
}

void dlgFileName::screenCap()
{
    QPixmap pm = QPixmap::grabWidget(this);
    pm.save("/home/root/picture.JPG","JPG");
}
