#ifndef DLGFILENAME_H
#define DLGFILENAME_H

#include <QWidget>
#include "clockcontroliic.h"
#include "dlgmessagebox.h"

namespace Ui {
class dlgFileName;
}

class dlgFileName : public QWidget
{
    Q_OBJECT
    
public:
    explicit dlgFileName(QWidget *parent = 0);
    ~dlgFileName();

signals:
    void txSelectedName(QString fName);

public slots:
    void rxFileValidStatus(bool isValid);
    
private slots:
    void on_bttn_Accept_clicked();

private:
    Ui::dlgFileName *ui;
    clockcontroliic *clockCtrl;
    void screenCap();
};

#endif // DLGFILENAME_H
