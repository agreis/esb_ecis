#include "dlggraphoptions.h"
#include "ui_dlggraphoptions.h"

dlgGraphOptions::dlgGraphOptions(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgGraphOptions)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    normEnabled = false;
}

dlgGraphOptions::~dlgGraphOptions()
{
    delete ui;
}

void dlgGraphOptions::on_bttn_Cancel_clicked()
{
    dlgGraphOptions::close();
}

void dlgGraphOptions::setParams(bool autoS, bool norm, int max, int min, bool normEn)
{
    normEnabled = normEn;
    ui->chk_Autoscale->setChecked(autoS);

    if (normEnabled == false)
    {
        ui->chk_Norm->setChecked(false);
        ui->chk_Norm->setEnabled(false);
    }
    else
    {
        ui->chk_Norm->setChecked(norm);
        ui->chk_Norm->setEnabled(true);

        if(norm)
        {
            ui->chk_Autoscale->setEnabled(true);
            ui->chk_Autoscale->setChecked(true);
        }
        else
        {
            ui->chk_Autoscale->setEnabled(true);
            ui->chk_Autoscale->setChecked(autoS);
        }
    }

    ui->spin_YMax->setValue(max);
    ui->spin_YMin->setValue(min);


}

void dlgGraphOptions::on_bttn_Save_clicked()
{
    bool autoScale = ui->chk_Autoscale->isChecked();
    bool norm = ui->chk_Norm->isChecked();
    int max = ui->spin_YMax->value();
    int min = ui->spin_YMin->value();

    emit sendParams(autoScale, norm, max, min);

    dlgGraphOptions::close();

}

void dlgGraphOptions::on_chk_Norm_clicked()
{
    //If normalized graph is selected, force autoscale

    if (ui->chk_Norm->isChecked())
        ui->chk_Autoscale->setChecked(true);
}

void dlgGraphOptions::on_chk_Autoscale_clicked()
{
    bool chked =ui->chk_Norm->isChecked();

    if (chked)
        ui->chk_Autoscale->setChecked(true);
}
