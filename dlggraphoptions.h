#ifndef DLGGRAPHOPTIONS_H
#define DLGGRAPHOPTIONS_H

#include <QDialog>

namespace Ui {
class dlgGraphOptions;
}

class dlgGraphOptions : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgGraphOptions(QWidget *parent = 0);
    ~dlgGraphOptions();

    void setParams(bool autoS, bool norm, int max, int min, bool normEn);

signals:

    void sendParams(bool autoS, bool norm, int max, int min);
    
private slots:

    void on_bttn_Cancel_clicked();

    void on_bttn_Save_clicked();

    void on_chk_Norm_clicked();

    void on_chk_Autoscale_clicked();

private:
    Ui::dlgGraphOptions *ui;
    bool normEnabled;   //locks gui option for normalization until control exposure complete
};

#endif // DLGGRAPHOPTIONS_H
