#include "dlgmessagebox.h"
#include "ui_dlgmessagebox.h"

dlgMessageBox::dlgMessageBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgMessageBox)
{
    //this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    //this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    ui->setupUi(this);
    this->setWindowIcon(QIcon("/home/root/noicon"));

    ui->btnn_Ok->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Cancel->setFocusPolicy(Qt::NoFocus);
}

dlgMessageBox::~dlgMessageBox()
{
    delete ui;
}

void dlgMessageBox::setTitle(QString title)
{
    ui->lblbTitle->setText(title);
}

void dlgMessageBox::setInfo(QString info)
{
    ui->lblInformation->setText(info);
}

void dlgMessageBox::setOKButton(bool en, QString text)
{
    ui->btnn_Ok->setText(text);
    ui->btnn_Ok->setVisible(en);
}

void dlgMessageBox::setCancelButton(bool en, QString text)
{
    ui->bttn_Cancel->setText(text);
    ui->bttn_Cancel->setVisible(en);
}

void dlgMessageBox::on_btnn_Ok_clicked()
{
    this->accept();
}

void dlgMessageBox::on_bttn_Cancel_clicked()
{
    this->reject();
}

void dlgMessageBox::setBackgroundColor(QColor bgc)
{
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),bgc);
    this->setPalette(pal);

    pal = ui->lblbTitle->palette();
    pal.setColor(ui->lblbTitle->backgroundRole(),bgc);
    ui->lblbTitle->setPalette(pal);
    ui->lblInformation->setPalette(pal);

}
