#ifndef DLGMESSAGEBOX_H
#define DLGMESSAGEBOX_H

#include <QDialog>
#include <QDebug>
#include <QColor>

namespace Ui {
class dlgMessageBox;
}

class dlgMessageBox : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgMessageBox(QWidget *parent = 0);
    ~dlgMessageBox();
    void setTitle(QString title);
    void setInfo(QString info);
    void setOKButton(bool en, QString text);
    void setCancelButton(bool en, QString text);
    void setBackgroundColor(QColor bgc);
    
private slots:
    void on_btnn_Ok_clicked();
    void on_bttn_Cancel_clicked();

signals:
    void clickedVal(int val);

private:
    Ui::dlgMessageBox *ui;
};

#endif // DLGMESSAGEBOX_H
