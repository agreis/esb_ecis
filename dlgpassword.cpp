#include "dlgpassword.h"
#include "ui_dlgpassword.h"
#include <QInputContext>

dlgPassword::dlgPassword(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dlgPassword)
{
    nextWin = 0;
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    ui->line_password->grabKeyboard();
    ui->line_password->grabKeyboard();

    //Set focus properties
    ui->bttn_Accept->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Cancel->setFocusPolicy(Qt::NoFocus);
}
dlgPassword::~dlgPassword()
{
    delete ui;
}

void dlgPassword::on_bttn_Cancel_clicked()
{
    ui->bttn_Accept->setFocus();
    emit passwordEntered(false,nextWin);
    dlgPassword::close();
}

//If user clicks accept, compare entered value with apssword
void dlgPassword::on_bttn_Accept_clicked()
{

    ui->bttn_Accept->setFocus();
    bool valid = false;

    //if (QString::compare(ui->line_password->text(),"") == 0)
    if (QString::compare(ui->line_password->text(),"ECIS") == 0)
        valid = true;

    emit passwordEntered(valid,nextWin);
    dlgPassword::close(); 
}


void dlgPassword::setNextDataFile()
{
    nextWin = 2;
}

void dlgPassword::setNextTestSettings()
{
    nextWin = 1;
}

void dlgPassword::setNextSoftwareUpdate()
{
    nextWin = 3;
}

void dlgPassword::setNextControlFile()
{
    nextWin = 4;
}
