#ifndef DLGPASSWORD_H
#define DLGPASSWORD_H

#include <QWidget>
#include <QString>

namespace Ui {
class dlgPassword;
}

class dlgPassword : public QWidget
{
    Q_OBJECT
    
public:
    explicit dlgPassword(QWidget *parent = 0);
    ~dlgPassword();
    void setNextDataFile();
    void setNextTestSettings();
    void setNextSoftwareUpdate();
    void setNextControlFile();

signals:
    void passwordEntered(bool valid, int window);
    
private slots:
    void on_bttn_Cancel_clicked();

    void on_bttn_Accept_clicked();

private:
    Ui::dlgPassword *ui;
    int nextWin;    //Used to determine next window, 0 = undef, 1 = test settings, 2 = data file management
};

#endif // DLGPASSWORD_H
