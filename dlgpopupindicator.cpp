#include "dlgpopupindicator.h"
#include "ui_dlgpopupindicator.h"

dlgPopupIndicator::dlgPopupIndicator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dlgPopupIndicator)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    showMssg = true;
    blinks = -1;

    tmrBlink = new QTimer(this);
    connect(tmrBlink,SIGNAL(timeout()),this,SLOT(onBlinkTimeout()));
    tmrBlink->start(2000);


    QPoint panelPos = QPoint(182, 205);
    this->move(panelPos);
}

dlgPopupIndicator::~dlgPopupIndicator()
{
    delete ui;
}

void dlgPopupIndicator::setBlinking(bool blink)
{
    isBlinking = blink;

    if (isBlinking == false)
        dlgPopupIndicator::setVisible(false);
}

//Slot called every 2 seconds to blink message
void dlgPopupIndicator::onBlinkTimeout()
{
    if (isBlinking == true)
    {
        if (showMssg == true)
        {
            showMssg = false;
            dlgPopupIndicator::setVisible(false);
        }
        else
        {
            showMssg = true;
            dlgPopupIndicator::setVisible(true);
        }

        //If counting the number of blinks
        if (blinks > 0)
            blinks = blinks -1;


        if (blinks == 0)
        {
            blinks = -1;
            dlgPopupIndicator::setVisible(false);
            isBlinking = false;

        }
    }
}

void dlgPopupIndicator::setMessage(QString mssg)
{
    ui->lbl_Text->setText(mssg);
}

void dlgPopupIndicator::setBackgroundColor(QColor bgc)
{
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),bgc);
    this->setPalette(pal);

    pal = ui->lbl_Text->palette();
    pal.setColor(ui->lbl_Text->backgroundRole(),bgc);
    ui->lbl_Text->setPalette(pal);

}
//Blinks the message specified number of times
void dlgPopupIndicator::setBlinkingNum(int bNum)
{
    blinks = bNum;
    isBlinking = true;
}

void dlgPopupIndicator::screenCap()
{
    QPixmap pm = QPixmap::grabWidget(this);
    pm.save("/home/root/picture.JPG","JPG");
}
