#ifndef DLGPOPUPINDICATOR_H
#define DLGPOPUPINDICATOR_H

#include <QWidget>
#include <QTimer>
#include <QColor>

namespace Ui {
class dlgPopupIndicator;
}

class dlgPopupIndicator : public QWidget
{
    Q_OBJECT
    
public:
    explicit dlgPopupIndicator(QWidget *parent = 0);
    ~dlgPopupIndicator();
    void setMessage(QString mssg);
    void setBackgroundColor(QColor bgc);
    void setBlinking(bool blink);
    void setBlinkingNum(int bNum);
    
private:
    Ui::dlgPopupIndicator *ui;
    QTimer* tmrBlink;
    bool showMssg;
    bool isBlinking;
    int blinks;
    void screenCap();

private slots:
    void onBlinkTimeout();
};

#endif // DLGPOPUPINDICATOR_H
