#include "dlgprecheck.h"
#include "ui_dlgprecheck.h"

dlgPreCheck::dlgPreCheck(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dlgPreCheck)
{

    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    ui->bttn_Abort->setVisible(false);

    dataPassed = false;
    limitsPassed = false;
    passedTest = false;

    //Set Focus Policy
    ui->bttn_Abort->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Next->setFocusPolicy(Qt::NoFocus);
}

dlgPreCheck::~dlgPreCheck()
{
    delete ui;
}

//Function used to pass data from main application
void dlgPreCheck::passData(int (&data)[8])
{
    int i;
    dataPassed = true;

    //Load data values to GUI
    ui->lbl_CE1->setText(QString::number(data[0]));
    ui->lbl_CE2->setText(QString::number(data[1]));
    ui->lbl_CE3->setText(QString::number(data[2]));
    ui->lbl_CE4->setText(QString::number(data[3]));
    ui->lbl_SE1->setText(QString::number(data[4]));
    ui->lbl_SE2->setText(QString::number(data[5]));
    ui->lbl_SE3->setText(QString::number(data[6]));
    ui->lbl_SE4->setText(QString::number(data[7]));

    //copy data to local member
    for (i=0; i <8; i++)
        dataArr[i] = data[i];

    //If limits have already been passed, perform analysis
    if (limitsPassed)
        performAnalysis();;

}

void dlgPreCheck::passLimits(int max, int min)
{
    limitsPassed = true;

    //Copy limits to local member
    maxImp = max;
    minImp = min;

    //If data has already been passed, perform analysis
    if (dataPassed)
        performAnalysis();
}

//Perform pre-check analysis and update GUI
void dlgPreCheck::performAnalysis()
{
    QString tempStr;
    passedTest = true;

    //CE1
    if ((dataArr[0] > maxImp) || (dataArr[0] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[0]) + "</font>";
        ui->lbl_CE1->setText(tempStr);
    }

    //CE2
    if ((dataArr[1] > maxImp) || (dataArr[1] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[1]) + "</font>";
        ui->lbl_CE2->setText(tempStr);
    }

    //CE3
    if ((dataArr[2] > maxImp) || (dataArr[2] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[2]) + "</font>";
        ui->lbl_CE3->setText(tempStr);
    }

    //CE4
    if ((dataArr[3] > maxImp) || (dataArr[3] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[3]) + "</font>";
        ui->lbl_CE4->setText(tempStr);
    }

    //SE1
    if ((dataArr[4] > maxImp) || (dataArr[4] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[4]) + "</font>";
        ui->lbl_SE1->setText(tempStr);
    }

    //SE2
    if ((dataArr[5] > maxImp) || (dataArr[5] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[5]) + "</font>";
        ui->lbl_SE2->setText(tempStr);
    }

    //SE3
    if ((dataArr[6] > maxImp) || (dataArr[6] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[6]) + "</font>";
        ui->lbl_SE3->setText(tempStr);
    }

    //SE4
    if ((dataArr[7] > maxImp) || (dataArr[7] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[7]) + "</font>";
        ui->lbl_SE4->setText(tempStr);
    }

    //If failed flag was triggered, update text
    if (passedTest == true)
    {
        tempStr = "<font color='Green'>CARTRIDGE PASSED</font>";
        ui->lbl_Result->setText(tempStr);
        ui->lbl_Instructions->setText("Impedance check passed, press ""Next "" to continue.");
        ui->bttn_Abort->setVisible(false);
        ui->bttn_Next->setText("Next");
        ui->bttn_Next->setVisible(true);
    }
    else
    {
        tempStr = "<font color='Red'>CARTRIDGE FAILED</font>";
        ui->lbl_Result->setText(tempStr);
        ui->lbl_Instructions->setText("Re-seat the cartridge and select ""Verify"", or select ""Abort"" and remove the cartridge to restart test.");
        ui->bttn_Abort->setVisible(true);
        ui->bttn_Next->setText("Verify");
        ui->bttn_Next->setVisible(true);
    }

}

//If user clicks the Next / Verify Button
void dlgPreCheck::on_bttn_Next_clicked()
{

    if (passedTest == true)
    {
        //ECIS check successful
        emit preCheckComplete(true);
        dlgPreCheck::close();
    }
    else
    {
        //ECIS check failed, perform recheck
        emit requestEcisReCheck();

        ui->lbl_CE1->setText("<font color='Black'>--------</font>");
        ui->lbl_CE2->setText("<font color='Black'>--------</font>");
        ui->lbl_CE3->setText("<font color='Black'>--------</font>");
        ui->lbl_CE4->setText("<font color='Black'>--------</font>");
        ui->lbl_SE1->setText("<font color='Black'>--------</font>");
        ui->lbl_SE2->setText("<font color='Black'>--------</font>");
        ui->lbl_SE3->setText("<font color='Black'>--------</font>");
        ui->lbl_SE4->setText("<font color='Black'>--------</font>");
        ui->lbl_Instructions->setText("Impedance measurement in progress...");
        ui->lbl_Result->setText("");

        ui->bttn_Abort->setVisible(false);
        ui->bttn_Next->setVisible(false);


    }
}

void dlgPreCheck::rxEcisReCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4)
{
    int dat[8] = {c1, c2, c3, c4, s1, s2, s3, s4};
    passData(dat);
}

void dlgPreCheck::on_bttn_Abort_clicked()
{
    //ECIS check failed
    emit preCheckComplete(false);
    dlgPreCheck::close();
}

//Capture screenshot
void dlgPreCheck::screenCap()
{
    QPixmap pm = QPixmap::grabWidget(this);
    pm.save("/home/root/picture.JPG","JPG");

}
