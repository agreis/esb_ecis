#ifndef DLGPRECHECK_H
#define DLGPRECHECK_H

#include <QWidget>

namespace Ui {
class dlgPreCheck;
}

class dlgPreCheck : public QWidget
{
    Q_OBJECT
    
public:
    explicit dlgPreCheck(QWidget *parent = 0);
    ~dlgPreCheck();
    void passData(int (&data)[8]);
    void passLimits(int max, int min);

signals:
    void requestEcisReCheck();
    void preCheckComplete(bool pass);

public slots:
    void rxEcisReCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    
private slots:
    void on_bttn_Next_clicked();
    void on_bttn_Abort_clicked();

private:
    Ui::dlgPreCheck *ui;
    bool dataPassed, limitsPassed;
    int maxImp, minImp;
    int dataArr[8];
    bool passedTest;

    void screenCap();
    void performAnalysis();
};

#endif // DLGPRECHECK_H
