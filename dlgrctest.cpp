#include "dlgrctest.h"
#include "ui_dlgrctest.h"

dlgRCTest::dlgRCTest(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgRCTest)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Initialize GUI elements
    ui->lbl_instructions->setText("Insert the RC Test Chip and select ""Test"" to initiate an electrical systems test.");
    ui->label_1->setVisible(false);
    ui->lbl_Result->setVisible(false);
    ui->lbl_CE1->setText("  --------");
    ui->lbl_CE2->setText("  --------");
    ui->lbl_CE3->setText("  --------");
    ui->lbl_CE4->setText("  --------");
    ui->lbl_SE1->setText("  --------");
    ui->lbl_SE2->setText("  --------");
    ui->lbl_SE3->setText("  --------");
    ui->lbl_SE4->setText("  --------");

    //Set acceptable range for RC chip (2000 Ohms, +/- 10%)
    maxImp = 2200;
    minImp = 1800;

    ui->bttn_Abort->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Test->setFocusPolicy(Qt::NoFocus);
}

//Function used to pass data from main application
void dlgRCTest::passData(int (&data)[8])
{
    int i;

    //Load data values to GUI
    ui->lbl_CE1->setText(QString::number(data[0]));
    ui->lbl_CE2->setText(QString::number(data[1]));
    ui->lbl_CE3->setText(QString::number(data[2]));
    ui->lbl_CE4->setText(QString::number(data[3]));
    ui->lbl_SE1->setText(QString::number(data[4]));
    ui->lbl_SE2->setText(QString::number(data[5]));
    ui->lbl_SE3->setText(QString::number(data[6]));
    ui->lbl_SE4->setText(QString::number(data[7]));

    //copy data to local member
    for (i=0; i <8; i++)
        dataArr[i] = data[i];

    //If limits have already been passed, perform analysis
    performAnalysis();

}

dlgRCTest::~dlgRCTest()
{
    delete ui;
}

void dlgRCTest::rxEcisRCCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4)
{
    int dat[8] = {c1, c2, c3, c4, s1, s2, s3, s4};
    passData(dat);
}

//Perform pre-check analysis and update GUI
void dlgRCTest::performAnalysis()
{
    QString tempStr;
    bool passedTest = true;
    int targetVal = 1870;

    //Calculate max and min based off tolerance and 2000 ohm nominal value
    maxImp = targetVal+targetVal*(tolerance/100);
    minImp = targetVal-targetVal*(tolerance/100);

    //CE1
    if ((dataArr[0] > maxImp) || (dataArr[0] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[0]) + "</font>";
        ui->lbl_CE1->setText(tempStr);
    }

    //CE2
    if ((dataArr[1] > maxImp) || (dataArr[1] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[1]) + "</font>";
        ui->lbl_CE2->setText(tempStr);
    }

    //CE3
    if ((dataArr[2] > maxImp) || (dataArr[2] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[2]) + "</font>";
        ui->lbl_CE3->setText(tempStr);
    }

    //CE4
    if ((dataArr[3] > maxImp) || (dataArr[3] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[3]) + "</font>";
        ui->lbl_CE4->setText(tempStr);
    }

    //SE1
    if ((dataArr[4] > maxImp) || (dataArr[4] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[4]) + "</font>";
        ui->lbl_SE1->setText(tempStr);
    }

    //SE2
    if ((dataArr[5] > maxImp) || (dataArr[5] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[5]) + "</font>";
        ui->lbl_SE2->setText(tempStr);
    }

    //SE3
    if ((dataArr[6] > maxImp) || (dataArr[6] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[6]) + "</font>";
        ui->lbl_SE3->setText(tempStr);
    }

    //SE4
    if ((dataArr[7] > maxImp) || (dataArr[7] < minImp))
    {
        passedTest = false;
        tempStr = "<font color='Red'>" + QString::number(dataArr[7]) + "</font>";
        ui->lbl_SE4->setText(tempStr);
    }

    //If failed flag was triggered, update text
    if (passedTest == true)
    {
        tempStr = "<font color='Green'>RC CARTRIDGE PASSED</font>";
        ui->lbl_Result->setText(tempStr);
        ui->lbl_instructions->setText("Test successful. Select ""Test"" to initiate an electrical systems test.");
        ui->bttn_Abort->setVisible(true);
        ui->bttn_Test->setVisible(true);
        ui->lbl_Result->setVisible(true);
        ui->label_1->setVisible(true);
    }
    else
    {
        tempStr = "<font color='Red'>RC CARTRIDGE FAILED</font>";
        ui->lbl_Result->setText(tempStr);
        ui->lbl_instructions->setText("Test failed. Select ""Test"" to initiate an electrical systems test.");
        ui->bttn_Abort->setVisible(true);
        ui->bttn_Test->setVisible(true);
        ui->lbl_Result->setVisible(true);
        ui->label_1->setVisible(true);
    }

}



void dlgRCTest::on_bttn_Abort_clicked()
{
    emit ecisRCCheckDone();
    dlgRCTest::close();
}

void dlgRCTest::on_bttn_Test_clicked()
{

    //Request ECIS measurement
    emit requestEcisRCCheck();

    //Update GUI
    ui->lbl_instructions->setText("Performing Measurements...");
    ui->bttn_Abort->setVisible(false);
    ui->bttn_Test->setVisible(false);
    ui->lbl_Result->setVisible(false);
    ui->label_1->setVisible(false);

}

void dlgRCTest::rxRCTolerance(double tol)
{
    tolerance = tol;
}

void dlgRCTest::screenCap()
{
    QPixmap pm = QPixmap::grabWidget(this);
    pm.save("/home/root/picture.JPG","JPG");
}
