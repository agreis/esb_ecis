#ifndef DLGRCTEST_H
#define DLGRCTEST_H

#include <QDialog>

namespace Ui {
class dlgRCTest;
}

class dlgRCTest : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgRCTest(QWidget *parent = 0);
    ~dlgRCTest();
    void passData(int (&data)[8]);
    
signals:
    void requestEcisRCCheck();
    void ecisRCCheckDone();

public slots:
    void rxEcisRCCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    void rxRCTolerance(double tol);

private slots:
    void on_bttn_Abort_clicked();
    void on_bttn_Test_clicked();

private:
    Ui::dlgRCTest *ui;
    int maxImp, minImp;
    int dataArr[8];
    void performAnalysis();
    double tolerance;

    void screenCap();


};

#endif // DLGRCTEST_H
