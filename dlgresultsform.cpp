#include "dlgresultsform.h"
#include "ui_dlgresultsform.h"
#include <QFile>
#include <QTextStream>

dlgResultsForm::dlgResultsForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgResultsForm)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Set focus policy
    ui->bttn_Accept->setFocusPolicy(Qt::NoFocus);
}

dlgResultsForm::~dlgResultsForm()
{
    delete ui;
}

void dlgResultsForm::loadFile(QString resultFilePath)
{
    QFile file(resultFilePath);

    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Result File Error");
        msgBox->setInfo("File failed to load with error: " + file.errorString());
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }

    QTextStream readSt(&file);

    ui->txt_View->setText(readSt.readAll());
    file.close();
}

void dlgResultsForm::on_bttn_Accept_clicked()
{
    dlgResultsForm::close();
}
