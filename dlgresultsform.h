#ifndef DLGRESULTSFORM_H
#define DLGRESULTSFORM_H

#include <QDialog>
#include "dlgmessagebox.h"

namespace Ui {
class dlgResultsForm;
}

class dlgResultsForm : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgResultsForm(QWidget *parent = 0);
    void loadFile(QString resultFilePath);
    ~dlgResultsForm();
    
private slots:
    void on_bttn_Accept_clicked();

private:
    Ui::dlgResultsForm *ui;
};

#endif // DLGRESULTSFORM_H
