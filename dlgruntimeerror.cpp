#include "dlgruntimeerror.h"
#include "ui_dlgruntimeerror.h"

dlgRunTimeError::dlgRunTimeError(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgRunTimeError)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame


    ui->setupUi(this);

    QColor bgc(254,111,95,255);
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),bgc);
    this->setPalette(pal);

    pal = ui->label->palette();
    pal.setColor(ui->label->backgroundRole(),bgc);
    ui->label->setPalette(pal);

    pal = ui->label_2->palette();
    pal.setColor(ui->label_2->backgroundRole(),bgc);
    ui->label_2->setPalette(pal);

    //Set focus policies
    ui->bttn_Abort->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Dismiss->setFocusPolicy(Qt::NoFocus);
}

dlgRunTimeError::~dlgRunTimeError()
{
    delete ui;
}

void dlgRunTimeError::on_bttn_Abort_clicked()
{
    emit txUserAck(true);
    dlgRunTimeError::setVisible(false);
}

void dlgRunTimeError::on_bttn_Dismiss_clicked()
{
    emit txUserAck(false);
    dlgRunTimeError::setVisible(false);
}

void dlgRunTimeError::displayErrorWin(bool show)
{
    dlgRunTimeError::setVisible(show);
}
