#ifndef DLGRUNTIMEERROR_H
#define DLGRUNTIMEERROR_H

#include <QDialog>

namespace Ui {
class dlgRunTimeError;
}

class dlgRunTimeError : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgRunTimeError(QWidget *parent = 0);
    ~dlgRunTimeError();

signals:
    void txUserAck(bool abort);
    
private slots:
    void displayErrorWin(bool show);
    void on_bttn_Abort_clicked();
    void on_bttn_Dismiss_clicked();

private:
    Ui::dlgRunTimeError *ui;
};

#endif // DLGRUNTIMEERROR_H
