#include "dlgsoftwareinfo.h"
#include "ui_dlgsoftwareinfo.h"

DlgSoftwareInfo::DlgSoftwareInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgSoftwareInfo)
{

    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Set focus properties
    ui->bttn_Close->setFocusPolicy(Qt::NoFocus);

    //Set graphics
    QPixmap pm("/home/root/Graphics/Nanohmics_about.bmp");
    ui->lbl_logo->setPixmap(pm);

    ui->lbl_version->setText("Version ---      --/--/----");

}

void DlgSoftwareInfo::updateVersionInfo(QString ver, QString dat)
{

    //Set version text
    QString verStr = "Version ";
    verStr.append(ver);
    verStr.append("       ");
    verStr.append(dat);
    ui->lbl_version->setText(verStr);
}

DlgSoftwareInfo::~DlgSoftwareInfo()
{
    delete ui;
}

void DlgSoftwareInfo::on_bttn_Close_clicked()
{
    DlgSoftwareInfo::close();
}
