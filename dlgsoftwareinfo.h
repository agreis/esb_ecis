#ifndef DLGSOFTWAREINFO_H
#define DLGSOFTWAREINFO_H

#include <QDialog>

namespace Ui {
class DlgSoftwareInfo;
}

class DlgSoftwareInfo : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgSoftwareInfo(QWidget *parent = 0);
    void updateVersionInfo(QString ver, QString dat);
    ~DlgSoftwareInfo();
    
private slots:
    void on_bttn_Close_clicked();

private:
    Ui::DlgSoftwareInfo *ui;
};

#endif // DLGSOFTWAREINFO_H
