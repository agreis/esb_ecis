#include "dlgsoftwareupdate.h"
#include "ui_dlgsoftwareupdate.h"
#include <stdlib.h>
#include <dlgmessagebox.h>
#include <QFile>



dlgSoftwareUpdate::dlgSoftwareUpdate(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgSoftwareUpdate)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Set focus properties
    ui->bttn_Cancel->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Update->setFocusPolicy(Qt::NoFocus);

    usbCheckTimer = new QTimer(this);
    connect(usbCheckTimer,SIGNAL(timeout()),this,SLOT(usbTimerUpdate()));
    usbCheckTimer->start(1000);

    //Request USB drive status
    emit requestUsbUpdate();

}

dlgSoftwareUpdate::~dlgSoftwareUpdate()
{
    delete ui;
}

//If user starts the software update
void dlgSoftwareUpdate::on_bttn_Update_clicked()
{
    emit txInteraction();

    ui->bttn_Update->setEnabled(false);

    //Check to see if "ESB_ECIS_NEW" is present within root folder of USB drive ~~~~~~~~~~

    if(QFile("/home/root/usb/ESB_ECIS_NEW").exists() == false)
    {
        //If no file present
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Error");
        msgBox->setInfo("No file named 'ESB_ECIS_NEW' found in the root directory of the attached USB drive.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        return;
    }

    //Copy file to local directory~~~~~~~

    //build local path name
    QString localName = "/home/root/ESB_ECIS_NEW";
    QString usbName = "/home/root/usb/ESB_ECIS_NEW";


    if(QFile::copy(usbName,localName) == false)
    {
        //error saving file
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Error");
        msgBox->setInfo("Failed to copy new executable to local storage. If this problem persists, restart the instrument.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->bttn_Update->setEnabled(true);
        return;
    }

    //Check to see if "ESB_ECIS_NEW" is present within root folder of local storage ~~~~~~~~~~
    if(QFile("/home/root/ESB_ECIS_NEW").exists() == false)
    {
        //If no devices attached   
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Error");
        msgBox->setInfo("Failed to copy new executable to local storage. If this problem persists, restart the instrument.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->bttn_Update->setEnabled(true);
        return;
    }

    //Success
    dlgMessageBox *msgBox= new dlgMessageBox(this);
    msgBox->setTitle("Success");
    msgBox->setInfo("New software version successfully copied to disk. The instrument will now restart.");
    msgBox->setOKButton(true,"Ok");
    msgBox->setCancelButton(false,"Cancel");
    msgBox->exec();

    //Run update ~~~~~~~
    system("reboot");
}

//If user cancels the software update
void dlgSoftwareUpdate::on_bttn_Cancel_clicked()
{
    usbCheckTimer->stop();
    dlgSoftwareUpdate::close();
    emit txInteraction();
}

void dlgSoftwareUpdate::mousePressEvent(QMouseEvent * event)
{
    emit txInteraction();
}

void dlgSoftwareUpdate::rxUsbUpdate(bool mounted)
{
    if (mounted)
    {
        ui->bttn_Update->setEnabled(true);
    }
    else
    {
        ui->bttn_Update->setEnabled(false);
    }
}


void dlgSoftwareUpdate::usbTimerUpdate()
{
    emit requestUsbUpdate();
}
