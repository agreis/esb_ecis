#ifndef DLGSOFTWAREUPDATE_H
#define DLGSOFTWAREUPDATE_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class dlgSoftwareUpdate;
}

class dlgSoftwareUpdate : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgSoftwareUpdate(QWidget *parent = 0);
    ~dlgSoftwareUpdate();
    virtual void mousePressEvent(QMouseEvent * event);

public slots:
    void rxUsbUpdate(bool mounted);
    void usbTimerUpdate();

signals:
    void txInteraction();
    void requestUsbUpdate();
    
private slots:
    void on_bttn_Update_clicked();

    void on_bttn_Cancel_clicked();

private:
    Ui::dlgSoftwareUpdate *ui;

    QTimer *usbCheckTimer;
};

#endif // DLGSOFTWAREUPDATE_H
