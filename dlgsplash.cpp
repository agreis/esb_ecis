#include "dlgsplash.h"
#include "ui_dlgsplash.h"

dlgSplash::dlgSplash(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgSplash)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame

    ui->setupUi(this);
}

dlgSplash::~dlgSplash()
{
    delete ui;
}
