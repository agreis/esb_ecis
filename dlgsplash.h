#ifndef DLGSPLASH_H
#define DLGSPLASH_H

#include <QDialog>

namespace Ui {
class dlgSplash;
}

class dlgSplash : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgSplash(QWidget *parent = 0);
    ~dlgSplash();
    
private:
    Ui::dlgSplash *ui;
};

#endif // DLGSPLASH_H
