#include "dlgtemperror.h"
#include "ui_dlgtemperror.h"

dlgTempError::dlgTempError(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgTempError)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame


    ui->setupUi(this);

    QColor bgc(254,111,95,255);
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),bgc);
    this->setPalette(pal);

    pal = ui->label->palette();
    pal.setColor(ui->label->backgroundRole(),bgc);
    ui->label->setPalette(pal);

    pal = ui->label_2->palette();
    pal.setColor(ui->label_2->backgroundRole(),bgc);
    ui->label_2->setPalette(pal);
}

dlgTempError::~dlgTempError()
{
    delete ui;
}

void dlgTempError::on_bttn_Accept_clicked()
{
    emit txUserAck(true);
    dlgTempError::close();
}

void dlgTempError::on_bttn_Dismiss_clicked()
{
    emit txUserAck(false);
    dlgTempError::close();
}
