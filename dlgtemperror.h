#ifndef DLGTEMPERROR_H
#define DLGTEMPERROR_H

#include <QDialog>

namespace Ui {
class dlgTempError;
}

class dlgTempError : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgTempError(QWidget *parent = 0);
    ~dlgTempError();

signals:
    void txUserAck(bool abort);

private slots:
    void on_bttn_Accept_clicked();

    void on_bttn_Dismiss_clicked();

private:
    Ui::dlgTempError *ui;
};

#endif // DLGTEMPERROR_H
