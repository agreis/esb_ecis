#include "dlgtestrecords.h"
#include "ui_dlgtestrecords.h"
#include <QFile>
#include <QTextStream>

dlgTestRecords::dlgTestRecords(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgTestRecords)
{

    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    //Set focus properties
    ui->bttn_Close->setFocusPolicy(Qt::NoFocus);
}

dlgTestRecords::~dlgTestRecords()
{
    delete ui;
}

void dlgTestRecords::loadFile(QString path)
{
    QFile file(path);

    if(file.exists() == false)
    {
        ui->txt_View->setText("No test records found.");
    }
    else
    {
        file.open(QFile::ReadOnly | QFile::Text);

        if (file.isOpen() == false)
        {

        }

        QTextStream in(&file);

        //ui->txt_View->setText(readSt.readAll());

        while (!in.atEnd())
        {
            QString line = in.readLine();
            ui->txt_View->append(line);
        }
        file.close();
    }
}

void dlgTestRecords::on_bttn_Close_clicked()
{
    dlgTestRecords::close();
}
