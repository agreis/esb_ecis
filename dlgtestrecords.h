#ifndef DLGTESTRECORDS_H
#define DLGTESTRECORDS_H

#include <QDialog>

namespace Ui {
class dlgTestRecords;
}

class dlgTestRecords : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgTestRecords(QWidget *parent = 0);
    void loadFile(QString path);
    ~dlgTestRecords();

    
private slots:
    void on_bttn_Close_clicked();

private:
    Ui::dlgTestRecords *ui;
};

#endif // DLGTESTRECORDS_H
