#include "dlgtestsettings.h"
#include "ui_dlgtestsettings.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <QDateTime>
#include <QTime>
#include <QDate>
#include <QCalendarWidget>




dlgTestSettings::dlgTestSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgTestSettings)
{

    clockCtrl = new clockcontroliic();
    clockCtrl->readDateTime();


    struct dt_clock dt = clockCtrl->dateTime;

    QDate *date = new QDate();
    QDateTime *dTime = new QDateTime();
    QTime *time = new QTime();

    date->setDate(2000+dt.year,dt.month,dt.date);
    time->setHMS(dt.hours,dt.minutes,dt.seconds);
    dTime->setTime(*time);


    ui->dateEdit->setDate(*date);
    ui->timeEdit->setDateTime(*dTime);


    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);


    timeChanged = false;


}

dlgTestSettings::dlgTestSettings(testData *dPtr) : ui(new Ui::dlgTestSettings)
{
    dataPtr = dPtr;

    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

    ui->spin_PosCtrlDuration->setValue(dataPtr->posCtrlDuration);
    ui->spin_PreExposeDuration->setValue(dataPtr->controlDuration);
    ui->spin_TestDuration->setValue(dataPtr->testDuration);
    ui->spin_MovWinConfidence->setValue(dataPtr->movWinConfidence);
    ui->spin_TecSetpoint->setValue(dataPtr->tempSetpoint);
    ui->chk_TecEnable->setChecked(dataPtr->tecEn);
    ui->spin_MinImpedance->setValue(dataPtr->minImpedance);
    ui->spin_MaxImpedance->setValue(dataPtr->maxImpedance);
    ui->spin_TimeDelay->setValue(dataPtr->timeDelay);
    ui->spin_ValidImp->setValue(dataPtr->validImpedanceRange);
    ui->lineEdit_serial->setText(dataPtr->instSerial);
    ui->spin_maxImpRun->setValue(dataPtr->maxImpRun);
    ui->spin_minImpRun->setValue(dataPtr->minImpRun);
    ui->spin_ShutoffTime->setValue(dataPtr->shutoffTime);
    ui->chk_ShutoffEn->setChecked(dataPtr->enableAutoShutoff);
    ui->spin_RCTol->setValue(dataPtr->rcTol);



    //Get current date and time
    clockCtrl = new clockcontroliic();
    clockCtrl->readDateTime();
    struct dt_clock dt = clockCtrl->dateTime;

    if (dt.seconds == -1)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Clock Error");
        msgBox->setInfo("Error reading from clock circuit");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }


    QDate *date = new QDate();
    QDateTime *dTime = new QDateTime();
    QTime *time = new QTime();

    date->setDate(dt.year+2000,dt.month,dt.date);
    time->setHMS(dt.hours,dt.minutes,dt.seconds);
    dTime->setDate(*date);
    dTime->setTime(*time);


    ui->dateEdit->setDate(*date);
    ui->timeEdit->setDateTime(*dTime);

    timeChanged = false;

    //Set tab stylesheet
    ui->tabWidget->setStyleSheet("QTabBar::tab { height: 45px; width: 230px; }");

    //Set spinbox stylesheets
    QString styleStr = "QSpinBox { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
                       "QSpinBox::up-button { subcontrol-position: right; right: 48px; width: 48px; height: 48px; }"
                       "QSpinBox::down-button { subcontrol-position: right; width: 48px; height: 48px; }";

    ui->spin_MaxImpedance->setStyleSheet(styleStr);
    ui->spin_maxImpRun->setStyleSheet(styleStr);
    ui->spin_MinImpedance->setStyleSheet(styleStr);
    ui->spin_minImpRun->setStyleSheet(styleStr);
    ui->spin_PosCtrlDuration->setStyleSheet(styleStr);
    ui->spin_PreExposeDuration->setStyleSheet(styleStr);
    ui->spin_ShutoffTime->setStyleSheet(styleStr);
    ui->spin_TecSetpoint->setStyleSheet(styleStr);
    ui->spin_TestDuration->setStyleSheet(styleStr);
    ui->spin_TimeDelay->setStyleSheet(styleStr);
    ui->spin_ValidImp->setStyleSheet(styleStr);

    //Set double spinbox stylesheets
    styleStr = "QDoubleSpinBox { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
                           "QDoubleSpinBox::up-button { subcontrol-position: right; right: 48px; width: 48px; height: 48px; }"
                           "QDoubleSpinBox::down-button { subcontrol-position: right; width: 48px; height: 48px; }";

    ui->spin_RCTol->setStyleSheet(styleStr);
    ui->spin_MovWinConfidence->setStyleSheet(styleStr);

    //Set calendar stylesheet
    ui->dateEdit->setStyleSheet("QDateEdit { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
            "QDateEdit::drop-down { subcontrol-position: right; width: 48px; height: 48px; }");
    ui->dateEdit->calendarWidget()->setMinimumHeight(400);
    ui->dateEdit->calendarWidget()->setMinimumWidth(400);
    QFont font = ui->dateEdit->calendarWidget()->font();
    font.setPointSize(18);
    ui->dateEdit->calendarWidget()->setFont(font);
    //Set time stylesheet
    ui->timeEdit->setStyleSheet("QDateTimeEdit { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
                                "QDateTimeEdit::up-button { subcontrol-position: right; right: 48px; width: 48px; height: 48px; }"
                                "QDateTimeEdit::down-button { subcontrol-position: right; width: 48px; height: 48px; }");

    //Set checkbox stylesheet
    ui->chk_ShutoffEn->setStyleSheet("QCheckBox::indicator { width: 36px; height: 36px; }");
    ui->chk_TecEnable->setStyleSheet("QCheckBox::indicator { width: 36px; height: 36px; }");

    //Set Focus Constraints
    ui->chk_ShutoffEn->setFocusPolicy(Qt::NoFocus);
    ui->chk_TecEnable->setFocusPolicy(Qt::NoFocus);

}

dlgTestSettings::~dlgTestSettings()
{
    delete ui;
}

void dlgTestSettings::on_bttn_Cancel_clicked()
{
    //Set focus to enforce keyboard hide
    ui->bttn_Cancel->setFocus();

    emit txInteraction();
    dlgTestSettings::close();
}

//If user clicks save, update global vars and save to configuration file
void dlgTestSettings::on_bttn_Save_clicked()
{
    //Set focus to enforce keyboard hide
    ui->bttn_Save->setFocus();

    emit txInteraction();

    bool tecEnable = dataPtr->tecEn;
    int tecSp = dataPtr->tempSetpoint;

    dataPtr->posCtrlDuration = ui->spin_PosCtrlDuration->value();
    dataPtr->controlDuration = ui->spin_PreExposeDuration->value();
    dataPtr->testDuration = ui->spin_TestDuration->value();
    dataPtr->movWinConfidence = ui->spin_MovWinConfidence->value();
    dataPtr->tempSetpoint = ui->spin_TecSetpoint->value();
    dataPtr->tecEn = ui->chk_TecEnable->isChecked();
    dataPtr->maxImpedance = ui->spin_MaxImpedance->value();
    dataPtr->minImpedance = ui->spin_MinImpedance->value();
    dataPtr->timeDelay = ui->spin_TimeDelay->value();
    dataPtr->validImpedanceRange = ui->spin_ValidImp->value();
    dataPtr->instSerial = ui->lineEdit_serial->text();
    dataPtr->maxImpRun = ui->spin_maxImpRun->value();
    dataPtr->minImpRun = ui->spin_minImpRun->value();
    dataPtr->shutoffTime =  ui->spin_ShutoffTime->value();
    dataPtr->enableAutoShutoff = ui->chk_ShutoffEn->isChecked();
    dataPtr->rcTol = ui->spin_RCTol->value();

    dataPtr->writeConfigFile();

    //Emit signal if TEC settings have changed
    if ((tecEnable != dataPtr->tecEn) || (tecSp != dataPtr->tempSetpoint))
        emit tecSettingChanged();

    //Set system date/time

    if (timeChanged == true)
    {
        QDateTime dateTime = ui->timeEdit->dateTime();
        QDate date = ui->dateEdit->date();
        QTime time = dateTime.time();



        dt_clock rt;

        rt.seconds = 0;
        rt.minutes = time.minute();
        rt.hours = time.hour();
        rt.date = date.day();
        rt.month = date.month();
        rt.year = date.year() % 100;

        if (clockCtrl->setDateTime(&rt) == false)
        {
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Clock Error");
            msgBox->setInfo("Error writing to the clock circuit");
            msgBox->setOKButton(true,"Ok");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
        }



    }






    dlgTestSettings::close();
}

void dlgTestSettings::on_timeEdit_timeChanged(const QTime &date)
{
    timeChanged = true;
    emit txInteraction();
}

void dlgTestSettings::on_dateEdit_dateChanged(const QDate &date)
{
    timeChanged = true;
    emit txInteraction();
}

void dlgTestSettings::on_tabWidget_currentChanged(int index)
{
    emit txInteraction();
}

//If user clicks on dialogue panel empty space, reset idle time.
void dlgTestSettings::mousePressEvent(QMouseEvent * event)
{
    emit txInteraction();

}
