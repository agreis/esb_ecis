#ifndef DLGTESTSETTINGS_H
#define DLGTESTSETTINGS_H

#include <QDialog>
#include <testdata.h>
#include "clockcontroliic.h"
#include "dlgmessagebox.h"

namespace Ui {
class dlgTestSettings;
}

class dlgTestSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgTestSettings(QWidget *parent = 0);
    dlgTestSettings(testData* dPtr);
    ~dlgTestSettings();
    virtual void mousePressEvent(QMouseEvent * event);
    
signals:
    void tecSettingChanged();
    void txInteraction();

private slots:
    void on_bttn_Cancel_clicked();

    void on_bttn_Save_clicked();

    void on_timeEdit_timeChanged(const QTime &date);

    void on_dateEdit_dateChanged(const QDate &date);

    void on_tabWidget_currentChanged(int index);

private:
    Ui::dlgTestSettings *ui;
    testData *dataPtr;
    clockcontroliic *clockCtrl;
    bool timeChanged;
};

#endif // DLGTESTSETTINGS_H
