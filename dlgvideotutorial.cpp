#include "dlgvideotutorial.h"
#include "ui_dlgvideotutorial.h"

dlgVideoTutorial::dlgVideoTutorial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgVideoTutorial)
{
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);

//    Phonon::MediaSource src("/home/root/Graphics/training.mp4");
 //   ui->videoPlayer->play(src);

}

dlgVideoTutorial::~dlgVideoTutorial()
{
    delete ui;
}
