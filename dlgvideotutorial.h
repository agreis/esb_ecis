#ifndef DLGVIDEOTUTORIAL_H
#define DLGVIDEOTUTORIAL_H

#include <QDialog>
//#include <phonon/VideoPlayer>
//#include <phonon/MediaSource>

namespace Ui {
class dlgVideoTutorial;
}

class dlgVideoTutorial : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgVideoTutorial(QWidget *parent = 0);
    ~dlgVideoTutorial();
    
private:
    Ui::dlgVideoTutorial *ui;
};

#endif // DLGVIDEOTUTORIAL_H
