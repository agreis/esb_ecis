#include "eciscontrol.h"
#include "Simple_GPIO.h"
#include "dlgmessagebox.h"
#include <QThread>
#include <QDebug>

//Serial Includes
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

ecisControl::ecisControl()
{
    int retVal;
    connected = false;

    struct termios uart4_attributes;

    //Initialize global vars
    serialPtrECIS = -1;

    //Set up UART 4 (ECIS) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //Attempt to Open UART4
    //if((serialPtrECIS = open("/dev/ttyO4", O_RDWR | O_NOCTTY )) < 0)
    if((serialPtrECIS = open("/dev/ttyO4", O_RDWR | O_NONBLOCK )) < 0)
    {
        //Error opening UART
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("ECIS Error");
        msgBox->setInfo("Error opening UART connection to the ECIS circuit");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        connected = false;
    }

    //Set Port attributes
    tcgetattr(serialPtrECIS, &uart4_attributes);   //Load current attributes into uart2_attributes object
    uart4_attributes.c_iflag = 0;
    uart4_attributes.c_oflag = 0;
    uart4_attributes.c_lflag = 0;
    uart4_attributes.c_cc[VMIN] = 0;    //Minimum number of bytes to return from read
    uart4_attributes.c_cc[VTIME] = 100; //Read Timeout
    cfsetospeed(&uart4_attributes, B57600);     //Baudrates
    cfsetispeed(&uart4_attributes, B57600);     //Baudrates

    retVal = tcsetattr(serialPtrECIS, TCSANOW, &uart4_attributes);

    //Set new attributes
    if (retVal != 0)
    {
        //If setup returned error
        int errsv = errno;
        const char *str = strerror(errsv);
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("ECIS Error");
        msgBox->setInfo("Error configuring ECIS measurement system.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        serialPtrECIS = -1;
    }

}

void ecisControl::ecisAckStart()
{
    //If ECIS subsystem is not initialized
    if (serialPtrECIS == -1)
    {
        emit ecisAckResult(false);
        return;
    }
    else //Else, perform ack exchange
    {
        char txBuff[5], rxBuff[19];

        //Flush the serial port buffers
        tcflush(serialPtrECIS,TCIOFLUSH);

        //Build command buffer
        txBuff[0] = '$';
        txBuff[1] = 'A';
        txBuff[2] = 'C';
        txBuff[3] = 'K';
        txBuff[4] = '!';

        //Write data to UART once - disregard
        write(serialPtrECIS,txBuff,6);
        sleep(1);
        read(serialPtrECIS,rxBuff,19);

        //Flush the serial port buffers
        tcflush(serialPtrECIS,TCIOFLUSH);

        //write data to UART
        write(serialPtrECIS, txBuff, 6);
        sleep(1);
        //read(serialPtrECIS,rxBuff,19);
        QString rxMssg = readData(19);

        //If no bytes were read, error
        if (rxMssg.length() == 0)
        {
            //Error opening ECIS channel
            connected = false;
            emit ecisAckResult(false);
            return;
        }

        //Verify ACK message
        if (QString::compare(rxMssg,"ACKmicroECISv1.5Ok.") == 0)
        {
            connected = true;
            emit ecisAckResult(true);
            qDebug() << "ECIS ACK";
        }
        else
        {
            //Error opening ECIS channel
            connected = false;
            emit ecisAckResult(false);
        }

    }

}


//Read data from serial port, expecting length of message
QString ecisControl::readData(int len)
{
    QString rxMssg = "";
    char c;

    QTime timer;
    timer.start();

    while ((rxMssg.length() < len) && (timer.elapsed() < ECIS_READ_TIMEOUT))
    {
        if(read(serialPtrECIS,&c,1) > 0)
        {
            rxMssg.append(c);
        }
    }

    return rxMssg;

}

//Slot that runs a single 8-electrode measurement and triggers signal to pass data to other thread
void ecisControl::ecisMeasurementStart()
{

    //If ECIS subsystem is not initialized
    if (!connected)
    {
        return;
    }
    else //Else, perform measurement
    {
        char txBuff[6], rxBuff[119];
        int measureArr[8];
        QString rxStr;

        //Initialize rxBuff
        for (int i =0;i<119;i++)
            rxBuff[i] = 0;

        //Flush the serial port buffers
        tcflush(serialPtrECIS,TCIOFLUSH);

        //initialize data array
        for (int i=0;i<6;i++)
            measureArr[i] = 0;

        //Build command buffer
        txBuff[0] = '$';
        txBuff[1] = 'S';
        txBuff[2] = 'M';
        txBuff[3] = 'P';
        txBuff[4] = '3';
        txBuff[5] = '!';

        //Write data to UART
        write(serialPtrECIS,txBuff,6);
        sleep(6);

        //Read
        QString rxMessage = readData(119);

        //Check ACK
        QString ackMssg = rxMessage.mid(0,7);

        if (QString::compare(ackMssg,"SMP3Ok.")!= 0)
        {
            //Error
            emit ecisMeasurementError();
            return;
        }

        QString readMssg;
        //Iterate through data, storing parsed values
        for(int i=0;i<8;i++)
        {
            readMssg = rxMessage.mid(i*14+7,14);

            //Make sure the first three characters are RDP
            if (QString::compare(readMssg.mid(0,3),"RDP")!= 0)
            {
                //Error
                emit ecisMeasurementError();
                return;
            }

            //Make sure the last three characters are Ok.
            if (QString::compare(readMssg.mid(11,3),"Ok.")!= 0)
            {
                //Error
                emit ecisMeasurementError();
                return;
            }

            //Characters 3-4 are the channel number (do nothing for now, since they come in order)

            //Characters 5-11 are the impedance
            measureArr[i] = readMssg.mid(5,6).toInt()/100;

        }


        emit ecisMeasurementComplete(measureArr[4],measureArr[5],measureArr[6],measureArr[7],measureArr[0],measureArr[1],measureArr[2],measureArr[3]);
    }

}
