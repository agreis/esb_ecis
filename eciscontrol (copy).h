#ifndef ECISCONTROL_H
#define ECISCONTROL_H

#include <QObject>
#include <QTime>


#define ECIS_READ_TIMEOUT 50 //units of ms

class ecisControl : public QObject
{
    Q_OBJECT

public:
    ecisControl();
    bool isConnected() { return connected; }

signals:
    void ecisMeasurementComplete(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    void ecisAckResult(bool found);
    void ecisMeasurementError();

public slots:
    void ecisMeasurementStart();
    void ecisAckStart();

private:

    bool connected;
    int serialPtrECIS;
    QString readData(int len);

};

#endif // ECISCONTROL_H
