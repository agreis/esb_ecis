//Description:Class Implementation for Input Panel GUI
//Author: Alex Greis
//Date: 7/2/2013

//References: qt-project.org/doc/qt-4.8/tools-inputpanel.html


#include "inputpanel.h"
#include "ui_inputpanel.h"
#include <QtCore>

inputPanel::inputPanel(QWidget *parent) : QWidget(parent), ui(new Ui::inputPanel)
{
    //Set global caps var to false
    caps = false;

    //setup GUI
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame
    ui->setupUi(this);




    //connect application focus event to get focus updates
    connect(qApp,SIGNAL(focusChanged(QWidget*,QWidget*)),this,SLOT(saveFocusWidget(QWidget*,QWidget*)));

    //set signal mapper to emit mapped signals
    signalMapper.setMapping(ui->bttn_A,ui->bttn_A);
	signalMapper.setMapping(ui->bttn_B,ui->bttn_B);
	signalMapper.setMapping(ui->bttn_C,ui->bttn_C);
	signalMapper.setMapping(ui->bttn_D,ui->bttn_D);
	signalMapper.setMapping(ui->bttn_E,ui->bttn_E);
	signalMapper.setMapping(ui->bttn_F,ui->bttn_F);
	signalMapper.setMapping(ui->bttn_G,ui->bttn_G);
	signalMapper.setMapping(ui->bttn_H,ui->bttn_H);
	signalMapper.setMapping(ui->bttn_I,ui->bttn_I);
	signalMapper.setMapping(ui->bttn_J,ui->bttn_J);
	signalMapper.setMapping(ui->bttn_K,ui->bttn_K);
	signalMapper.setMapping(ui->bttn_L,ui->bttn_L);
	signalMapper.setMapping(ui->bttn_M,ui->bttn_M);
	signalMapper.setMapping(ui->bttn_N,ui->bttn_N);
	signalMapper.setMapping(ui->bttn_O,ui->bttn_O);
	signalMapper.setMapping(ui->bttn_P,ui->bttn_P);
	signalMapper.setMapping(ui->bttn_Q,ui->bttn_Q);
	signalMapper.setMapping(ui->bttn_R,ui->bttn_R);
	signalMapper.setMapping(ui->bttn_S,ui->bttn_S);
	signalMapper.setMapping(ui->bttn_T,ui->bttn_T);
	signalMapper.setMapping(ui->bttn_U,ui->bttn_U);
	signalMapper.setMapping(ui->bttn_V,ui->bttn_V);
	signalMapper.setMapping(ui->bttn_W,ui->bttn_W);
	signalMapper.setMapping(ui->bttn_X,ui->bttn_X);
	signalMapper.setMapping(ui->bttn_Y,ui->bttn_Y);
	signalMapper.setMapping(ui->bttn_Z,ui->bttn_Z);
	signalMapper.setMapping(ui->bttn_0,ui->bttn_0);
	signalMapper.setMapping(ui->bttn_1,ui->bttn_1);
	signalMapper.setMapping(ui->bttn_2,ui->bttn_2);
	signalMapper.setMapping(ui->bttn_3,ui->bttn_3);
	signalMapper.setMapping(ui->bttn_4,ui->bttn_4);
	signalMapper.setMapping(ui->bttn_5,ui->bttn_5);
	signalMapper.setMapping(ui->bttn_6,ui->bttn_6);
	signalMapper.setMapping(ui->bttn_7,ui->bttn_7);
	signalMapper.setMapping(ui->bttn_8,ui->bttn_8);
	signalMapper.setMapping(ui->bttn_9,ui->bttn_9);
    signalMapper.setMapping(ui->bttn_Caps, ui->bttn_Caps);
    signalMapper.setMapping(ui->bttn_Space,ui->bttn_Space);
    signalMapper.setMapping(ui->bttn_comma,ui->bttn_comma);
    signalMapper.setMapping(ui->bttn_period,ui->bttn_period);
    signalMapper.setMapping(ui->bttn_close,ui->bttn_close);
    signalMapper.setMapping(ui->bttn_equals,ui->bttn_equals);
    signalMapper.setMapping(ui->bttn_minus,ui->bttn_minus);
    signalMapper.setMapping(ui->bttn_bkspc,ui->bttn_bkspc);


    //connect button signals to signal mapper
    connect(ui->bttn_A,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_B,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_C,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_D,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_E,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_F,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_G,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_H,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_I,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_J,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_K,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_L,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_M,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_N,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_O,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_P,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_Q,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_R,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_S,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_T,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_U,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_V,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_W,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_X,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_Y,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_Z,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_0,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_1,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_2,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_3,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_4,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_5,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_6,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_7,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_8,SIGNAL(clicked()),&signalMapper,SLOT(map()));
	connect(ui->bttn_9,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_Caps,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_Space,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_comma,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_period,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_close,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_minus,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_equals,SIGNAL(clicked()),&signalMapper,SLOT(map()));
    connect(ui->bttn_bkspc,SIGNAL(clicked()),&signalMapper,SLOT(map()));

    connect(&signalMapper, SIGNAL(mapped(QWidget*)),this,SLOT(buttonClicked(QWidget*)));
}

inputPanel::~
inputPanel()
{
    delete ui;
}

void inputPanel::buttonClicked(QWidget *w)
{

    QChar chr = qvariant_cast<QChar>(w->property("buttonValue"));

    //if button was Caps Lock, call function, otherwise emit signal
    if (chr == '~')
    {
        capsLockHandler();
    }
    else if (chr == '`')
    {
        closePanel();
    }
    else
        emit characterGenerated(chr);
}

void inputPanel::saveFocusWidget(QWidget *oldFocus, QWidget *newFocus)
{
    if (newFocus != 0 && !this->isAncestorOf(newFocus))
    {
        lastFocusedWidget = newFocus;
    }
}

bool inputPanel::event(QEvent *e)
{
    switch (e->type())
    {
    case QEvent::WindowActivate:
        if (lastFocusedWidget)
            lastFocusedWidget->activateWindow();
        break;
    default:
        break;
    }

    return QWidget::event(e);
}

//Called if user clicks "CLOSE"
void inputPanel::closePanel()
{
    this->hide();
}

//Called if user clicks "CAPS"
void inputPanel::capsLockHandler()
{
    if (caps)
    {
        ui->bttn_A->setText("a");
        ui->bttn_A->setProperty("buttonValue",'a');

        ui->bttn_B->setText("b");
        ui->bttn_B->setProperty("buttonValue",'b');

        ui->bttn_C->setText("c");
        ui->bttn_C->setProperty("buttonValue",'c');

        ui->bttn_D->setText("d");
        ui->bttn_D->setProperty("buttonValue",'d');

        ui->bttn_E->setText("e");
        ui->bttn_E->setProperty("buttonValue",'e');

		ui->bttn_F->setText("f");
        ui->bttn_F->setProperty("buttonValue",'f');

		ui->bttn_G->setText("g");
        ui->bttn_G->setProperty("buttonValue",'g');

		ui->bttn_H->setText("h");
        ui->bttn_H->setProperty("buttonValue",'h');

		ui->bttn_I->setText("i");
        ui->bttn_I->setProperty("buttonValue",'i');

		ui->bttn_J->setText("j");
        ui->bttn_J->setProperty("buttonValue",'j');

		ui->bttn_K->setText("k");
        ui->bttn_K->setProperty("buttonValue",'k');

		ui->bttn_L->setText("l");
        ui->bttn_L->setProperty("buttonValue",'l');

		ui->bttn_M->setText("m");
        ui->bttn_M->setProperty("buttonValue",'m');

		ui->bttn_N->setText("n");
        ui->bttn_N->setProperty("buttonValue",'n');

		ui->bttn_O->setText("o");
        ui->bttn_O->setProperty("buttonValue",'o');

		ui->bttn_P->setText("p");
        ui->bttn_P->setProperty("buttonValue",'p');

		ui->bttn_Q->setText("q");
        ui->bttn_Q->setProperty("buttonValue",'q');

		ui->bttn_R->setText("r");
        ui->bttn_R->setProperty("buttonValue",'r');

		ui->bttn_S->setText("s");
        ui->bttn_S->setProperty("buttonValue",'s');

		ui->bttn_T->setText("t");
        ui->bttn_T->setProperty("buttonValue",'t');

		ui->bttn_U->setText("u");
        ui->bttn_U->setProperty("buttonValue",'u');

		ui->bttn_V->setText("v");
        ui->bttn_V->setProperty("buttonValue",'v');

		ui->bttn_W->setText("w");
        ui->bttn_W->setProperty("buttonValue",'w');

		ui->bttn_X->setText("x");
        ui->bttn_X->setProperty("buttonValue",'x');

		ui->bttn_Y->setText("y");
        ui->bttn_Y->setProperty("buttonValue",'y');

		ui->bttn_Z->setText("z");
        ui->bttn_Z->setProperty("buttonValue",'z');

		ui->bttn_0->setText("0");
        ui->bttn_0->setProperty("buttonValue",'0');

		ui->bttn_1->setText("1");
        ui->bttn_1->setProperty("buttonValue",'1');

		ui->bttn_2->setText("2");
        ui->bttn_2->setProperty("buttonValue",'2');

		ui->bttn_3->setText("3");
		ui->bttn_3->setProperty("buttonValue",'3');

		ui->bttn_4->setText("4");
        ui->bttn_4->setProperty("buttonValue",'4');

		ui->bttn_5->setText("5");
        ui->bttn_5->setProperty("buttonValue",'5');

		ui->bttn_6->setText("6");
        ui->bttn_6->setProperty("buttonValue",'6');

		ui->bttn_7->setText("7");
        ui->bttn_7->setProperty("buttonValue",'7');

		ui->bttn_8->setText("8");
        ui->bttn_8->setProperty("buttonValue",'8');

		ui->bttn_9->setText("9");
        ui->bttn_9->setProperty("buttonValue",'9');

        ui->bttn_comma->setText(",");
        ui->bttn_comma->setProperty("buttonValue",',');

        ui->bttn_period->setText(".");
        ui->bttn_period->setProperty("buttonValue",'.');

        ui->bttn_equals->setText("=");
        ui->bttn_equals->setProperty("buttonValue",'=');

        ui->bttn_minus->setText("-");
        ui->bttn_minus->setProperty("buttonValue",'-');

        caps = false;
    }
    else
    {
        ui->bttn_A->setText("A");
        ui->bttn_A->setProperty("buttonValue",'A');

        ui->bttn_B->setText("B");
        ui->bttn_B->setProperty("buttonValue",'B');

        ui->bttn_C->setText("C");
        ui->bttn_C->setProperty("buttonValue",'C');

        ui->bttn_D->setText("D");
        ui->bttn_D->setProperty("buttonValue",'D');

        ui->bttn_E->setText("E");
        ui->bttn_E->setProperty("buttonValue",'E');

		ui->bttn_F->setText("F");
        ui->bttn_F->setProperty("buttonValue",'F');

		ui->bttn_G->setText("G");
        ui->bttn_G->setProperty("buttonValue",'G');

		ui->bttn_H->setText("H");
        ui->bttn_H->setProperty("buttonValue",'H');

		ui->bttn_I->setText("I");
        ui->bttn_I->setProperty("buttonValue",'I');

		ui->bttn_J->setText("J");
        ui->bttn_J->setProperty("buttonValue",'J');

		ui->bttn_K->setText("K");
        ui->bttn_K->setProperty("buttonValue",'K');

		ui->bttn_L->setText("L");
        ui->bttn_L->setProperty("buttonValue",'L');

		ui->bttn_M->setText("M");
        ui->bttn_M->setProperty("buttonValue",'M');

		ui->bttn_N->setText("N");
        ui->bttn_N->setProperty("buttonValue",'N');

		ui->bttn_O->setText("O");
        ui->bttn_O->setProperty("buttonValue",'O');

		ui->bttn_P->setText("P");
        ui->bttn_P->setProperty("buttonValue",'P');

		ui->bttn_Q->setText("Q");
        ui->bttn_Q->setProperty("buttonValue",'Q');

		ui->bttn_R->setText("R");
        ui->bttn_R->setProperty("buttonValue",'R');

		ui->bttn_S->setText("S");
        ui->bttn_S->setProperty("buttonValue",'S');

		ui->bttn_T->setText("T");
        ui->bttn_T->setProperty("buttonValue",'T');

		ui->bttn_U->setText("U");
        ui->bttn_U->setProperty("buttonValue",'U');

		ui->bttn_V->setText("V");
        ui->bttn_V->setProperty("buttonValue",'V');

		ui->bttn_W->setText("W");
        ui->bttn_W->setProperty("buttonValue",'W');

		ui->bttn_X->setText("X");
        ui->bttn_X->setProperty("buttonValue",'X');

		ui->bttn_Y->setText("Y");
        ui->bttn_Y->setProperty("buttonValue",'Y');

		ui->bttn_Z->setText("Z");
        ui->bttn_Z->setProperty("buttonValue",'Z');

		ui->bttn_0->setText(")");
        ui->bttn_0->setProperty("buttonValue",')');

		ui->bttn_1->setText("!");
        ui->bttn_1->setProperty("buttonValue",'!');

		ui->bttn_2->setText("@");
        ui->bttn_2->setProperty("buttonValue",'@');

		ui->bttn_3->setText("#");
		ui->bttn_3->setProperty("buttonValue",'#');

		ui->bttn_4->setText("$");
        ui->bttn_4->setProperty("buttonValue",'$');

		ui->bttn_5->setText("%");
        ui->bttn_5->setProperty("buttonValue",'%');

		ui->bttn_6->setText("^");
        ui->bttn_6->setProperty("buttonValue",'^');

        ui->bttn_7->setText("&&");
        ui->bttn_7->setProperty("buttonValue",'&');

		ui->bttn_8->setText("*");
        ui->bttn_8->setProperty("buttonValue",'*');

		ui->bttn_9->setText("(");
        ui->bttn_9->setProperty("buttonValue",'(');

        ui->bttn_comma->setText("<");
        ui->bttn_comma->setProperty("buttonValue",'<');

        ui->bttn_period->setText(">");
        ui->bttn_period->setProperty("buttonValue",'>');

        ui->bttn_equals->setText("+");
        ui->bttn_equals->setProperty("buttonValue",'+');

        ui->bttn_minus->setText(":");
        ui->bttn_minus->setProperty("buttonValue",':');

        caps = true;
    }
}


