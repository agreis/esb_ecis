//Description:Class Definition for Input Panel GUI
//Author: Alex Greis
//Date: 7/2/2013

//References: qt-project.org/doc/qt-4.8/tools-inputpanel.html


#ifndef INPUTPANEL_H
#define INPUTPANEL_H

#include <QWidget>
#include <QSignalMapper>

namespace Ui {
class inputPanel;
}

class inputPanel : public QWidget
{
    Q_OBJECT
    
public:
    explicit inputPanel(QWidget *parent = 0);
    ~inputPanel();

signals:
    void characterGenerated(QChar character);

protected:
    bool event(QEvent *e);

private slots:
    void saveFocusWidget(QWidget *oldFocus, QWidget *newFocus);
    void buttonClicked(QWidget *w);


private:
    void capsLockHandler();
    void closePanel();

    bool caps;
    Ui::inputPanel *ui;
    QWidget *lastFocusedWidget;
    QSignalMapper signalMapper;
};

#endif // INPUTPANEL_H
