//Description:Class Implementation for input panel context
//Author: Alex Greis
//Date: 7/2/2013

//References: qt-project.org/doc/qt-4.8/tools-inputpanel.html


#include "inputpanelcontext.h"
//#include "inputpanel.h"
#include <QtCore>
#include <QApplication>


/*inputPanelContext::inputPanelContext(QObject *parent) :
    QInputContext(parent)
{
    ip = new inputPanel();
    connect(ip, SIGNAL(characterGenerated(QChar)),SLOT(sendCharacter(QChar)));

}*/

inputPanelContext::inputPanelContext()
{
    ip = new inputPanel;
    connect(ip, SIGNAL(characterGenerated(QChar)),SLOT(sendCharacter(QChar)));
}

inputPanelContext::~inputPanelContext()
{
    delete ip;

}

bool inputPanelContext::isComposing() const
{
    return false;
}

QString inputPanelContext::identifierName()
{
    return "inputPanelContext";
}

QString inputPanelContext::language()
{
    return "en_US";
}


void inputPanelContext::reset()
{

}

bool inputPanelContext::filterEvent(const QEvent *event)
{
    if (event->type() == QEvent::RequestSoftwareInputPanel)
    {
        updatePosition();
        ip->show();
        return true;
    }
    else if (event->type() == QEvent::CloseSoftwareInputPanel)
    {
        ip->hide();
        return true;
    }
    return false;
}

void inputPanelContext::sendCharacter(QChar character)
{
    QPointer<QWidget> w = focusWidget();

    if(!w)
        return;

    //Special case for backspace
    if (character == '}')
    {
        QKeyEvent keyPress(QEvent::KeyPress, Qt::Key_Backspace, Qt::NoModifier, QString(character));
        QApplication::sendEvent(w, &keyPress);
    }
    else
    {
        QKeyEvent keyPress(QEvent::KeyPress, character.unicode(), Qt::NoModifier, QString(character));
        QApplication::sendEvent(w, &keyPress);
    }



    if(!w)
        return;

    QKeyEvent keyRelease(QEvent::KeyPress,character.unicode(),Qt::NoModifier,QString());
    QApplication::sendEvent(w, &keyRelease);

}

void inputPanelContext::updatePosition()
{
    QWidget *widget = focusWidget();

    if (!widget)
        return;

    QRect widgetRect = widget->rect();
    QPoint panelPos = QPoint(0, 600-255);   //QPoint(widgetRect.left(), widgetRect.bottom() +2);
    //panelPos = widget->mapToGlobal(panelPos);
    ip->move(panelPos);
}

