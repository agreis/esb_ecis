//Description:Class Definition for Input Panel Context
//  Responsible for recognizing global requests for input, hiding/displaying the virtual keyboard, and passing key press events to widgets
//Author: Alex Greis
//Date: 7/2/2013

//References: qt-project.org/doc/qt-4.8/tools-inputpanel.html


#ifndef INPUTPANELCONTEXT_H
#define INPUTPANELCONTEXT_H

#include <QInputContext>
#include "inputpanel.h"



class inputPanel;

class inputPanelContext : public QInputContext
{
    Q_OBJECT
public:
   // explicit inputPanelContext(QObject *parent = 0);
    inputPanelContext();
    ~inputPanelContext();

    bool filterEvent(const QEvent *event);

    QString identifierName();
    QString language();

    bool isComposing() const;

    void reset();
    
private slots:
    void sendCharacter(QChar character);

private:
    void updatePosition();

private:
    inputPanel *ip;
    
};

#endif // INPUTPANELCONTEXT_H
