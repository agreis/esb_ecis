/*
 *Interrupt monitor
 *
 *Provides control of GPIO ports that trigger interrupts. Also has a few
 *misc gpio controls (fan speed, power trigger)
 *
 *
 */


#include "intmonitor.h"
#include "Simple_GPIO.h"
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

IntMonitor::IntMonitor(QObject *parent) :
    QObject(parent)
{
    int retVal;

    //Configure MUX Pins~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //ECIS INT 0
    //retVal = gpio_omap_mux_setup("eCAP0_in_PWM0_out","37");       //GPIO0_7 mode 7, input with pulldown
    gpio_export(7);
    retVal = gpio_set_dir(7,INPUT_PIN);
    retVal = gpio_set_edge(7, "both");

    //ECIS INT 1
    //retVal = gpio_omap_mux_setup("gpmc_be1n","37");       //GPIO1_28 mode 7, input with pulldown
    gpio_export(60);
    retVal = gpio_set_dir(60,INPUT_PIN);
    retVal = gpio_set_edge(60, "both");

    //PWR_INT (power button press)
    //retVal = gpio_omap_mux_setup("gpmc_a0","37");     //GPIO1_16, input with pulldown
    gpio_export(48);
    retVal = gpio_set_dir(48,INPUT_PIN);
    retVal = gpio_set_edge(48,"both");

    //CHGEN INT (battery charging status)
    gpio_export(68);
    retVal = gpio_set_dir(68,INPUT_PIN);
    retVal = gpio_set_edge(68,"both");

}


IntMonitor::~IntMonitor()
{

}

int IntMonitor::readLines()
{
    int err;
    unsigned int value1, value2;
    err = gpio_get_value(60,&value1);
    err = gpio_get_value(7, &value2);

    unsigned int retVal = value1 | value2;
}

void IntMonitor::monitor()
{
    QString rxStr;


    //----------------

    int fd[4];

    fd[0] = open("/sys/class/gpio/gpio7/value", O_RDONLY);
    fd[1] = open("/sys/class/gpio/gpio60/value", O_RDONLY);
    fd[2] = open("/sys/class/gpio/gpio48/value", O_RDONLY);
    fd[3] = open("/sys/class/gpio/gpio68/value", O_RDONLY);

    struct pollfd pfd[4];

    pfd[0].fd = fd[0];
    pfd[0].events = POLLPRI;
    pfd[0].revents = 0;

    pfd[1].fd = fd[1];
    pfd[1].events = POLLPRI;
    pfd[1].revents = 0;

    pfd[2].fd = fd[2];
    pfd[2].events = POLLPRI;
    pfd[2].revents = 0;

    pfd[3].fd = fd[3];
    pfd[3].events = POLLPRI;
    pfd[3].revents = 0;


    while(1)
    {
       poll(pfd,4,-1);

       if (pfd[0].revents != 0)
       {
           //ECIS INT 0
           int val = getPinStatus(fd[0]);
           emit triggered(0,val);
       }
       if (pfd[1].revents != 0)
       {
           //ECIS INT 1
           int val = getPinStatus(fd[1]);
           emit triggered(1,val);
       }
       if (pfd[2].revents != 0)
       {
           //Power button
           int val = getPinStatus(fd[2]);
           if (val == 0)
            emit powerDownTrig();
       }
       if (pfd[3].revents != 0)
       {
           //Charge status
           int val = getPinStatus(fd[3]);
           if (val == 0)
               emit chargeStateChanged(true);
           else
               emit chargeStateChanged(false);
       }


       sleep(1);
   }

}

int IntMonitor::getPinStatus(int fd)
{
    int value;
    lseek(fd,0,0);

    char buff[1024];
    int size = read(fd,buff,sizeof(buff));
    if (size != -1)
    {
        buff[size] = NULL;
        value = atoi(buff);
    }
    else
    {
        value = -1;
    }
    return value;
}
