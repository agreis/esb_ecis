#ifndef INTMONITOR_H
#define INTMONITOR_H

#include <QObject>

class IntMonitor : public QObject
{
    Q_OBJECT
public:
    explicit IntMonitor(QObject *parent = 0);
    ~IntMonitor();
    int connectedToDc;
    int readLines();
signals:
    void triggered(int id, int value);
    void powerDownTrig();
    void chargeStateChanged(bool charging);
public slots:
    void monitor();
private:
    int getPinStatus(int fd);
};

#endif // INTMONITOR_H
