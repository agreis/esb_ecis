#include "mainwindow.h"
#include "inputpanelcontext.h"
#include <QApplication>
#include <QWSServer>
#include <QProxyStyle>
#include <QDebug>


//Custom proxy style class to launch SIP in single clicks
class CustomProxyStyle:public QProxyStyle
{
public:
    //Sty
    int styleHint(StyleHint hint, const QStyleOption *option = 0, const QWidget *widget = 0, QStyleHintReturn *returnData = 0) const
    {
        if (hint == QStyle::SH_RequestSoftwareInputPanel)
        {
            return QStyle::RSIP_OnMouseClick;
        }

        return QProxyStyle::styleHint(hint,option,widget,returnData);
    }

    virtual int pixelMetric(PixelMetric metric, const QStyleOption * option = 0, const QWidget * widget = 0) const
    {
        switch (metric)
        {
            case QStyle::PM_SmallIconSize:
                return 40;
            case QStyle::PM_LargeIconSize:
                return 40;
            case QStyle::PM_ButtonIconSize:
                return 40;
            case QStyle::PM_ToolBarIconSize:
                return 40;
            case QStyle::PM_TabBarIconSize:
                return 40;
            default:
                return QProxyStyle::pixelMetric(metric, option, widget);
        }

    }

};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setStyle(new CustomProxyStyle);
    QWSServer::setCursorVisible(false);

    inputPanelContext *ic = new inputPanelContext;
    a.setInputContext(ic);

    MainWindow w;
    w.show();
    
    return a.exec();
}
