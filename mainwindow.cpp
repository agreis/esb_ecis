#include "mainwindow.h"



MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{

    //set up main window
    this->setWindowFlags(Qt::CustomizeWindowHint);  //set window with no title bar
    this->setWindowFlags(Qt::FramelessWindowHint);  //set window with no frame

    ui->setupUi(this);
    ui->lbl_Alert->setVisible(false);
    ui->bttn_Results->setVisible(false);
    ui->bttn_posControl->setVisible(false);
    ui->actionAbort_Test->setEnabled(false);

    //Battery menu bar
    battBar = new QMenuBar(ui->menuBar);
    battIcon = new QMenu("",battBar);
    battIcon->setIcon(QIcon("/home/root/Graphics/batt_empty.png"));
    battReadout = new QMenu("Initializing...");
    battBar->addMenu(battReadout);
    battBar->addMenu(battIcon);
    ui->menuBar->setCornerWidget(battBar);
    battAlertIssued = false;

    battCharging = false;
    battCharge = -1;

    //Size
    QFont font = ui->menuFile->font();
    qDebug() << font.pointSize();


    //Prevent focus
    ui->bttn_autoscale->setFocusPolicy(Qt::NoFocus);
    ui->bttn_normalize->setFocusPolicy(Qt::NoFocus);
    battIcon->setFocusPolicy(Qt::NoFocus);
    battReadout->setFocusPolicy(Qt::NoFocus);

    //Initialize graph option buttons
    ui->bttn_normalize->setEnabled(false);
    palUnselected = ui->bttn_Next->palette();
    palSelected = ui->bttn_Next->palette();
    palSelected.setColor(QPalette::Button,QColor(165,220,245));
    ui->bttn_autoscale->setAutoFillBackground(true);
    ui->bttn_normalize->setAutoFillBackground(true);
    ui->bttn_autoscale->setPalette(palSelected);
    ui->bttn_autoscale->update();

    //set up graph
    ui->plot_Main->xAxis->setLabel("Time (min)");
    ui->plot_Main->yAxis->setLabel("Impedance (Ohms)");
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();
    ui->plot_Main->addGraph();

    QColor color;

    //CE1
    color = QColor(0,0,255,255);
    //color.setHsl(160,240,120);
    ui->plot_Main->graph(0)->setPen(color);

    //CE2
    color = QColor(70,70,255,255);
    //color.setHsl(160,240,153);
    ui->plot_Main->graph(1)->setPen(color);

    //CE3
    color = QColor(111,111,255,255);
    //color.setHsl(160,240,172);
    ui->plot_Main->graph(2)->setPen(color);

    //CE4
    color = QColor(164,164,255,255);
    //color.setHsl(160,240,197);
    ui->plot_Main->graph(3)->setPen(color);

    //SE1
    color = QColor(200,0,0,255);
    //color.setHsv(0,240,255);
    ui->plot_Main->graph(4)->setPen(color);

    //SE2
    color = QColor(255,0,0,255);
    //color.setHsv(0,240,255);
    ui->plot_Main->graph(5)->setPen(color);

    //SE3
    color = QColor(255,60,60,255);
    //color.setHsv(0,240,255);
    ui->plot_Main->graph(6)->setPen(color);

    //SE4
    color = QColor(255,136,136,255);
    //color.setHsv(0,240,255);
    ui->plot_Main->graph(7)->setPen(color);

    //VLINE1
    color = QColor(0,0,0,255);
    //color.setHsv(40,255,255);
    ui->plot_Main->graph(8)->setPen(color);
    QPen vPen;
    vPen.setColor(color);
    vPen.setWidthF(1);
    ui->plot_Main->graph(8)->setPen(vPen);

    //VLINE2
    color = QColor(0,0,0,255);
    //color.setHsv(40,255,255);
    ui->plot_Main->graph(9)->setPen(color);
    ui->plot_Main->graph(9)->setPen(vPen);


    audio_al_st = false;
    autoShutdownTrig= false;

    //Set focus
    ui->bttn_autoscale->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Next->setFocusPolicy(Qt::NoFocus);
    ui->bttn_normalize->setFocusPolicy(Qt::NoFocus);
    ui->bttn_posControl->setFocusPolicy(Qt::NoFocus);
    ui->bttn_Results->setFocusPolicy(Qt::NoFocus);

    //Set stylesheet of spinboxes
    ui->spin_maxGraph->setStyleSheet("QSpinBox { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
                                     "QSpinBox::up-button { subcontrol-position: right; right: 48px; width: 48px; height: 48px; }"
                                     "QSpinBox::down-button { subcontrol-position: right; width: 48px; height: 48px; }");

    ui->spin_minGraph->setStyleSheet("QSpinBox { border: 1px solid; padding-right: 60px; padding-left: 20px; }"
                                     "QSpinBox::up-button { subcontrol-position: right; right: 48px; width: 48px; height: 48px; }"
                                     "QSpinBox::down-button { subcontrol-position: right; width: 48px; height: 48px; }");


    //Launch initialization function after 1 second
    QTimer::singleShot(300,this,SLOT(init()));



}

MainWindow::~MainWindow()
{
    delete ui;
}

//Slot used to initialize the application GUI
void MainWindow::init()
{

    //Initialize Timers
    measurementTimer = new QTimer(this);
    connect(measurementTimer,SIGNAL(timeout()),this,SLOT(onMeasurementTimerInterval()));

    guiTimer = new QTimer(this);    //Gui timer used to refresh GUI during runtime
    connect(guiTimer,SIGNAL(timeout()),this,SLOT(onGuiTimerInterval()));
    guiTimer->start(1000);

    battTimer = new QTimer(this);   //Check the battery status
    connect(battTimer,SIGNAL(timeout()),this,SLOT(onBattTimerInterval()));
    battTimer->start(500);

    tempTimer = new QTimer(this);   //Every five seconds, ensure that the heatsink temperature is not >= 75 C
    connect(tempTimer,SIGNAL(timeout()),this,SLOT(onTempTimerInterval()));
    tempTimer->start(5000);

    initTempTimer = new QTimer(this);   //Every five seconds, check the TEC temperature during the init stage
    connect(initTempTimer,SIGNAL(timeout()),this,SLOT(onTempMeasureInitInterval()));
    initTempTimer->start(5000);

    idleTimer = new QTimer(this);       //every minute, increment idle timer (if idle)
    connect(idleTimer,SIGNAL(timeout()),this,SLOT(onIdleTimerInterval()));
    idleTimer->start(1000);


    //Init IO Control
    miscIoThread = new QThread(this);
    ioControl = new miscIO();

    ioControl->moveToThread(miscIoThread);
    connect(this,SIGNAL(txRequestBuzz(int)),ioControl,SLOT(pulseBuzzer(int)));
    miscIoThread->start();


    //Load config file
    dataCtrl.readConfigFile();

    //Set up ECIS thread /slots~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ecisThread = new QThread(this);
    ecisCtrl = new ecisControl();
    ecisCtrl->moveToThread(ecisThread);
    connect(ecisCtrl,SIGNAL(ecisMeasurementComplete(int,int,int,int,int,int,int,int)),this,SLOT(rxEcisData(int,int,int,int,int,int,int,int)));
    connect(ecisCtrl,SIGNAL(ecisAckResult(bool)),this,SLOT(rxEcisAck(bool)));
    connect(this,SIGNAL(requestEcisMeasurement()),ecisCtrl,SLOT(ecisMeasurementStart()));
    connect(this,SIGNAL(requestEcisAck()),ecisCtrl,SLOT(ecisAckStart()));
    ecisThread->start();

    //check for ECIS measurement circuit presence
    if (dataCtrl.DEBUG_sbcOnly == false)
        emit requestEcisAck();

    //Set up TEC thread /slots~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    tecThread = new QThread(this);
    tecCtrl = new tecControl();
    tecCtrl->moveToThread(tecThread);
    connect(tecCtrl,SIGNAL(tecTempMeasureComplete(double)),this,SLOT(rxTecTempMeasure(double)));
    connect(tecCtrl,SIGNAL(tecTempSetpointComplete(double)),this,SLOT(rxTecSetpoint(double)));
    connect(tecCtrl,SIGNAL(tecAckComplete(bool)),this,SLOT(rxTecAck(bool)));
    connect(this,SIGNAL(requestTecMeasurement()),tecCtrl,SLOT(tecTempMeasureStart()));
    connect(this,SIGNAL(requestTecAck()),tecCtrl,SLOT(tecAckStart()));
    connect(this,SIGNAL(requestTecDisable()),tecCtrl,SLOT(tecDisableStart()));
    connect(this,SIGNAL(requestTecEnable()),tecCtrl,SLOT(tecEnableStart()));
    connect(this,SIGNAL(setTecSetpoint(double)),tecCtrl,SLOT(tecSetpointSet(double)));

    tecThread->start();

    emit requestTecAck();

    //Set up interrupt monitor thread

    intControl = new IntMonitor();
    interruptThread = new QThread(this);
    intControl->moveToThread(interruptThread);
    connect(interruptThread,SIGNAL(started()),intControl,SLOT(monitor()));
    connect(intControl,SIGNAL(triggered(int,int)),this,SLOT(onIntRx(int,int)));
    connect(intControl,SIGNAL(powerDownTrig()),this,SLOT(rxPowerBttnClick()));
    connect(intControl, SIGNAL(chargeStateChanged(bool)),this,SLOT(rxBatteryChargeStatus(bool)));
    interruptThread->start();

    //Set up moving window analysis thread
    movWin = new movingWindow();
    movWinThread = new QThread(this);
    movWin->moveToThread(movWinThread);
    connect(movWin,SIGNAL(txAnalysisComplete(bool)),this,SLOT(rxAnalysisResults(bool)));
    connect(this,SIGNAL(requestAnalysis(QString,QTextStream*,QString,double)),movWin,SLOT(rxAnalysisRequest(QString,QTextStream*,QString,double)));
    movWinThread->start();


    //initialize graph data containers
    timeStamp = 0;

    //Initialize TEC
    if ((dataCtrl.tecEn == true) && (dataCtrl.DEBUG_sbcOnly == false))
    {
        emit setTecSetpoint(dataCtrl.tempSetpoint);
        emit requestTecEnable();
        emit requestTecMeasurement();
    }

    //Initialize flashing popup (hidden until used later)
    popUp = new dlgPopupIndicator();
    //popUp->setMessage("Test");
    //popUp->setBackgroundColor(Qt::blue);
    popUp->setBlinking(false);
    popUp->hide();


    //Initialize error window popup
    errorWin = new dlgRunTimeError();
    connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
    connect(this,SIGNAL(txErrorWinShow(bool)),errorWin,SLOT(displayErrorWin(bool)));
    //errorWin->show();

    //Initialize Auto-shutoff window popup
    autoShutoffWin = new dlgAutoShutoff();
    connect(this,SIGNAL(txAutoShutoffWinShow(bool)),autoShutoffWin,SLOT(displayAutoShutoffWin(bool)));
    connect(autoShutoffWin,SIGNAL(txUserAckShutoff()),this,SLOT(rxAutoShutoffAck()));

    //Global gui vars
    nextFilter = false;

    //Interrupt delay time
    numSecsElapsed =0;
    dataCtrl.currentState = testData::INIT;

    //Initialize Idle Timer and setup relevant idle monitoring
    idleTimeCount = 0;
    installEventFilter(this);
    installEventFilter(this->menuBar());
    installEventFilter(this->ui->plot_Main);
    connect(ui->plot_Main,SIGNAL(mousePress(QMouseEvent*)),this,SLOT(onPlotClick(QMouseEvent*)));
    //installEventFilter(ui->bttn_Next);
    //installEventFilter(ui->bttn_posControl);
    //installEventFilter(ui->bttn_Results);


    //Set audio enabled state
    audio_en = true;

    //Init power control
    pwrControl = new powerControl();
    pwrThread = new QThread(this);
    pwrControl->moveToThread(pwrThread);
    connect(this,SIGNAL(requestBatteryStatus()),pwrControl,SLOT(requestBattery()));
    connect(pwrControl,SIGNAL(txBatteryCharge(int)),this,SLOT(rxBatteryStatus(int)));
    connect(this,SIGNAL(txShutdownRequest()),pwrControl,SLOT(powerDown()));
    pwrThread->start();

    //Debug, enable level translator
    ioControl->setTimer6(true);

    //Initialize the USB Drive Control
    //Load config file
    dataCtrl.readConfigFile();

    //Set up ECIS thread /slots~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    usbThread = new QThread(this);
    usbDriveCtrl = new usbMounter();
    usbDriveCtrl->moveToThread(usbThread);
    usbThread->start();

    //Alert user if software was updated
    if (dataCtrl.updated)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        QString message = "The software was successfully updated to version ";
        message.append(SOFTWARE_VER);
        msgBox->setTitle("Software Update");
        msgBox->setInfo(message);
        msgBox->setOKButton(true,"Accept");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }

}

void MainWindow::rxBatteryStatus(int batVal)
{

    //For now, ignore errors
    if (batVal < 0)
    {
        emit requestBatteryStatus();
        return;
    }

    battCharge = batVal;


    //Set the text battery status
    QString mssg =QString::number(batVal) + "%";
    battReadout->setTitle(mssg);
    battReadout->setObjectName("ColorThis");

    QPalette pal_norm = battReadout->palette();
    QPalette pal_red = battReadout->palette();
    pal_norm.setBrush(QPalette::WindowText,*(new QBrush(Qt::black)));
    pal_red.setBrush(QPalette::WindowText,*(new QBrush(Qt::red)));

    //Based on the battery level, set the icon


    if(batVal > 75)
    {
        battAlertIssued = false;
        battBar->setPalette(pal_norm);

        if(battCharging)
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_4_bar_charge.png"));
        else
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_4_bar.png"));
    }
    else if (batVal >50)
    {
        battAlertIssued = false;
        battBar->setPalette(pal_norm);

        if(battCharging)
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_3_bar_charge.png"));
        else
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_3_bar.png"));
    }
    else if (batVal > 25)
    {
        battAlertIssued = false;
        battBar->setPalette(pal_norm);

        if(battCharging)
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_2_bar_charge.png"));
        else
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_2_bar.png"));
    }
    else
    {

        if(battCharging)
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_1_bar_charge.png"));
        else
            battIcon->setIcon(QIcon("/home/root/Graphics/batt_1_bar.png"));

        //Low battery alerts
        if (batVal < 10)
        {
            //If less than 10, set font to be red
            battBar->setPalette(pal_red);
        }

        if (batVal >=5)
            battAlertIssued = false;

        //If less than 5 and not charging, give shut down alert
        if ((batVal < 5) && (battAlertIssued == false) && (!battCharging))
        {
            battAlertIssued = true;
            emit txRequestBuzz(1);
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Battery Warning");
            msgBox->setInfo("The battery charge is critically low. System will auto-shutdown at 2%. Please connect charger immediately.");
            msgBox->setOKButton(true,"Accept");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->exec();
        }

        if ((batVal < 2) && (!battCharging))
        {
            emit txRequestBuzz(1);
            dlgMessageBox *msgBox= new dlgMessageBox(this);
            msgBox->setTitle("Shutting Down");
            msgBox->setInfo("The battery is depleted. The instrument is powering down.");
            msgBox->setOKButton(false,"Accept");
            msgBox->setCancelButton(false,"Cancel");
            msgBox->show();
            sleep(5);
            msgBox->hide();

            emit txShutdownRequest();
        }

    }

    ui->menuBar->setCornerWidget(battBar);
}

//Callback for clicking on the main plot for idle monitoring
void MainWindow::onPlotClick(QMouseEvent* eve)
{
    idleTimeCount = 0;
    emit txAutoShutoffWinShow(false);
}

//Event filter to monitor idle time (mouse events)
bool MainWindow::eventFilter(QObject *object, QEvent *event)
{

    int x = 0;

    switch(event->type())
    {
        case QEvent::MouseMove:
            x = 1;
            break;
        case QEvent::MouseButtonPress:
            x = 1;
            break;
        case QEvent::HoverEnter:
            x = 1;
            break;
        case QEvent::HoverMove:
            x = 1;
            break;
        case QEvent::HoverLeave:
            x = 1;
            break;
        case QEvent::Enter:
            x = 1;
            break;
        case QEvent::Leave:
            x = 1;
            break;
        case QEvent::NonClientAreaMouseMove:
            x = 1;
            break;
        case QEvent::NonClientAreaMouseButtonPress:
            x = 1;
            break;
        case QEvent::TouchBegin:
            x = 1;
            break;
        case QEvent::TouchEnd:
            x = 1;
            break;
        case QEvent::TouchUpdate:
            x = 1;
            break;
        default:
            break;

    }

    //If any of the events were user input, reset idle count timer
    if (x ==1)
    {
        idleTimeCount = 0;
        emit txAutoShutoffWinShow(false);
    }


    return false;
}

//If user clicks on dialogue panel empty space, reset idle time.
void MainWindow::mousePressEvent(QMouseEvent * event)
{
    idleTimeCount = 0;
    emit txAutoShutoffWinShow(false);

}

//Slot for Menu bar selection - Abort Test
void MainWindow::On_menuAbortTest()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgMessageBox *msgBox= new dlgMessageBox(this);
    msgBox->setTitle("Please Confirm");
    msgBox->setInfo("Do you wish to abort the current test?");
    msgBox->setOKButton(true,"Confirm");
    msgBox->setCancelButton(true,"Cancel");
    int retVal = msgBox->exec();

    if (retVal == 1)
    {
        //User has selected to  abort test. Return application to the initial state

        popUp->setBlinking(false);
        dataCtrl.currentState = testData::INIT;
        unlockGui();
        dataCtrl.reInit();
        ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
        ui->bttn_Results->setVisible(false);
        ui->lbl_Alert->setVisible(false);
        ui->bttn_Next->setText("Next");
        ui->bttn_Next->setVisible(true);
        clearPlot();
        dataCtrl.warningDisplayed = false;
    }
}

//Slot for Menu bar selection - Data File Management
void MainWindow::On_menuDataFile()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgPassword *passWin = new dlgPassword();
    connect(passWin,SIGNAL(passwordEntered(bool,int)),this,SLOT(passwordComplete(bool,int)));
    passWin->setNextDataFile();
    passWin->show();



}

//Slot for Menu bar selection - Software Information
void MainWindow::On_menuSoftwareInfo()
{
    DlgSoftwareInfo *infoWin = new DlgSoftwareInfo();
    infoWin->updateVersionInfo(SOFTWARE_VER,SOFTWARE_DATE);

    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    infoWin->show();
}

//Slot for menu bar selection - Control File Management
void MainWindow::On_menuControlFile()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgPassword *passWin = new dlgPassword();
    connect(passWin,SIGNAL(passwordEntered(bool,int)),this,SLOT(passwordComplete(bool,int)));
    passWin->setNextControlFile();
    passWin->show();
}

//Slot for Menu bar selection - Software Update
void MainWindow::On_menuSoftwareUpdate()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgPassword *passWin = new dlgPassword();
    connect(passWin,SIGNAL(passwordEntered(bool,int)),this,SLOT(passwordComplete(bool,int)));
    passWin->setNextSoftwareUpdate();
    passWin->show();
}
//Slot for Menu bar selection - Test Records
void MainWindow::On_menuTestRecords()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgTestRecords *recordWin = new dlgTestRecords();
    recordWin->loadFile(dataCtrl.testRecordPath);

    recordWin->show();
}

//Slot for Menu bar selection - Test Settings
void MainWindow::On_menuTestSettings()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    dlgPassword *passWin = new dlgPassword();
    connect(passWin,SIGNAL(passwordEntered(bool,int)),this,SLOT(passwordComplete(bool,int)));
    passWin->setNextTestSettings();
    passWin->show();

}

//Slot for Menu bar selection - RC Test
void MainWindow::On_menuRCTest()
{

    //Make sure that the ECIS hardware is connected
    if(!ecisCtrl->isConnected())
    {
        //If control file has not been specified, raise an error
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("ECIS Hardware Error");
        msgBox->setInfo("An issue exists with the ECIS hardware. Please restart if this issue persists.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        return;
    }

    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    //Launch RC Test Window
    dlgRCTest *rctWin = new dlgRCTest();
    connect(this,SIGNAL(txEcisDataRCCheck(int,int,int,int,int,int,int,int)),rctWin, SLOT(rxEcisRCCheck(int,int,int,int,int,int,int,int)));
    connect(rctWin,SIGNAL(requestEcisRCCheck()),this,SLOT(rxRCCheckEcisRequest()));
    connect(rctWin,SIGNAL(ecisRCCheckDone()),this,SLOT(rCCheckComplete()));
    connect(this,SIGNAL(txRcTol(double)),rctWin,SLOT(rxRCTolerance(double)));

    //Send tolerance to class
    emit(txRcTol(dataCtrl.rcTol));

    dataCtrl.currentState = testData::RC_TEST;
    rctWin->show();
}

//Slot for Menu bar selection - Power off
void MainWindow::On_menuPowerOff()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);
    int retVal=0;

    if (autoShutdownTrig == false)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Power Down Requested");
        msgBox->setInfo("Are you sure you want to power down? The current test will be aborted.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(true,"Cancel");
        retVal = msgBox->exec();
    }
    else
    {
        retVal = 1;
    }

    if (retVal == 1)
        emit txShutdownRequest();
}

void MainWindow::On_menuTrainingVideo()
{

}

//Slot for Menu bar selection - Audio toggle
void MainWindow::On_menuAudioToggle()
{
    idleTimer = 0;
    emit txAutoShutoffWinShow(false);

    if (audio_en == true)
    {
        //Update text
        ui->actionDisable_Audio->setText("Enable Audio");

        //Update state var
        audio_en = false;
    }
    else
    {
        //Update text
        ui->actionDisable_Audio->setText("Disable Audio");

        //Update state var
        audio_en = true;

        //Play sound
        playAlert();
    }
}

//Slot called when test settings indicate a change in TEC settings
void MainWindow::tecSettingsChanged()
{

    //update temperature setpoint
    emit setTecSetpoint((double)(dataCtrl.tempSetpoint));
    sleep(1);

    if (dataCtrl.tecEn == true)
    {
        emit requestTecEnable();
    }
    else
    {
        emit requestTecDisable();;
    }
}

//Slot called when precheck is complete
void MainWindow::preCheckComplete(bool passed)
{
    if (passed == true)
    {
        dataCtrl.currentState = testData::SAMPLE_NAME_INPUT;
        dlgFileName *fileWin = new dlgFileName();
        connect(this,SIGNAL(txFileNameValid(bool)),fileWin,SLOT(rxFileValidStatus(bool)));
        connect(fileWin,SIGNAL(txSelectedName(QString)),this,SLOT(rxFileName(QString)));
        fileWin->show();
    }
    else
    {
        dataCtrl.currentState = testData::INIT;
        unlockGui();
        ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test.");
        ui->bttn_Next->setVisible(true);
    }
}

//Slot that receives candidate file name from file naming window. Attempts to create files and returns success or failure to file window via signal.
void MainWindow::rxFileName(QString fname)
{
    bool success = dataCtrl.initSaveFiles(fname);
    emit txFileNameValid(success);

    //If save was succesful, file window will close from slot/signal - advance program state
    if (success == true)
    {
           ui->lbl_Alert->setVisible(false);
           dataCtrl.currentState = testData::PRE_EXPOSURE;
           numSecsElapsed =0;
           dataCtrl.recentTemp = 0;
           emit requestEcisMeasurement();
           emit requestTecMeasurement();
           measurementTimer->start(60000);  //start 1 minute timer for measurement interval
           guiTimer->start(1000);

           dataCtrl.measLeft = dataCtrl.controlDuration +1;
           dataCtrl.secondsLeft = dataCtrl.controlDuration*60;

           dataCtrl.chipDisconnect = false; //clear any previously registered errors before starting test
           updateGuiTime();

    }

}

//Slot for receiving graphing parameters from graph option window
void MainWindow::rxGraphParams(bool autoS, bool norm, int max, int min)
{
    dataCtrl.yMax = max;
    dataCtrl.yMin = min;
    dataCtrl.autoScale = autoS;
    dataCtrl.normalized= norm;

    updatePlot();
}

//Slot for recieving ecis measurement data from ecis thread
void MainWindow::rxEcisData(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4)
{
    //Swap controls and sample channels
    int c1t = c1;
    int c2t = c2;
    int c3t = c3;
    int c4t = c4;
    c4 = s4;
    c3 = s3;
    c2 = s2;
    c1 = s1;
    s1 = c1t;
    s2 = c2t;
    s3 = c3t;
    s4 = c4t;

   /* if (dataCtrl.analysisRunning)
    {
        int x = 2;
    }
    */

    double dataArr[8] = {c1,c2,c3,c4,s1,s2,s3,s4};
    int iDataArr[8] = {c1,c2,c3,c4,s1,s2,s3,s4};

    //max reading of 8000 ohms (open)
    int i;

    for (i=0; i<8; i++)
    {
        if (dataArr[i] > 8000)
        {
            dataArr[i] = 8000;
            iDataArr[i] = 8000;

            switch(i)
            {
                case 0:
                {
                   c1 = 8000;
                   break;
                }
                case 1:
                {
                   c2 = 8000;
                   break;
                }
                case 2:
                {
                   c3 = 8000;
                   break;
                }
                case 3:
                {
                   c4 = 8000;
                   break;
                }
                case 4:
                {
                   s1 = 8000;
                   break;
                }
                case 5:
                {
                   s2 = 8000;
                   break;
                }
                case 6:
                {
                   s3 = 8000;
                   break;
                }
                case 7:
                {
                   s4 = 8000;
                   break;
                }

            }
        }
    }

    //copy measurement to global
    dataCtrl.recentDataArr[0] = dataArr[0];
    dataCtrl.recentDataArr[1] = dataArr[1];
    dataCtrl.recentDataArr[2] = dataArr[2];
    dataCtrl.recentDataArr[3] = dataArr[3];
    dataCtrl.recentDataArr[4] = dataArr[4];
    dataCtrl.recentDataArr[5] = dataArr[5];
    dataCtrl.recentDataArr[6] = dataArr[6];
    dataCtrl.recentDataArr[7] = dataArr[7];


    //Action depends on current application state
    switch(dataCtrl.currentState)
    {
        //User is performing RC Testing
        case testData::RC_TEST:
        {
            emit txEcisDataRCCheck(c1,c2,c3,c4,s1,s2,s3,s4);
            break;
        }
        case testData::IMP_CHECK_IN_PROGRESS:
        {
            //Impedance pre-check complete
            dlgPreCheck *pChkWin = new dlgPreCheck();
            pChkWin->passData(iDataArr);
            pChkWin->passLimits(dataCtrl.maxImpedance,dataCtrl.minImpedance);
            connect(this,SIGNAL(txEcisDataReCheck(int,int,int,int,int,int,int,int)),pChkWin, SLOT(rxEcisReCheck(int,int,int,int,int,int,int,int)));
            connect(pChkWin,SIGNAL(requestEcisReCheck()),this,SLOT(rxImpPreCheckEcisRequest()));
            connect(pChkWin,SIGNAL(preCheckComplete(bool)),this,SLOT(preCheckComplete(bool)));

            pChkWin->show();

            maxMinVal(dataArr);

            break;
        }

        case testData::IMP_CHECK_FAIL:
        {
            emit txEcisDataReCheck(c1,c2,c3,c4,s1,s2,s3,s4);
            break;
        }

        case testData::PRE_EXPOSURE:
        {

            maxMinVal(dataArr);
            this->plotData(dataArr);
            dataCtrl.addSaveFileEntry(dataArr);


            //Perform run-time error checking
            if ((errCheck(dataArr)) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);

            }

            //If pre-exposure period is complete, show indicator, reveal time, move to next state
            if (dataCtrl.measLeft <= 0)
            {

                popUp->setMessage("Click Next to  initiate test");
                popUp->setBackgroundColor(QColor(254,111,95,164));
                popUp->setBlinking(true);

                playAlert();

                ui->lbl_Alert->setText("Pre-exposure complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->bttn_Next->setVisible(true);

                dataCtrl.currentState = testData::PRE_EXPOSURE_COMPLETE;


            }

            break;
        }
        case testData::PRE_EXPOSURE_COMPLETE:
        {

            maxMinVal(dataArr);
            this->plotData(dataArr);
            dataCtrl.addSaveFileEntry(dataArr);
            dataCtrl.addRawFileEntry(dataArr);

            break;
        }
        case testData::TEST_EXPOSURE:
        {

            maxMinVal(dataArr);
            dataCtrl.numMeas = dataCtrl.numMeas +1;
            this->plotData(dataArr);
            dataCtrl.addSaveFileEntry(dataArr);
            dataCtrl.addRawFileEntry(dataArr);

            //Perform run-time error checking after specified delay
            if ((errCheck(dataArr)) && (dataCtrl.numMeas > dataCtrl.timeDelay) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);
            }

            //If measurement period has expired
            if (dataCtrl.measLeft <=0)
            {
                dataCtrl.analysisRunning = true;
                dataCtrl.currentState = testData::TEST_EXPOSURE_COMPLETE;
                ui->lbl_userInstruct->setText("Performing Analysis...");

                //Set Alert as temp indicator

                ui->lbl_Alert->setText("Temp: " + QString::number(dataCtrl.recentTemp/1) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setVisible(true);

                QPalette pal = ui->lbl_Alert->palette();
                pal.setColor(ui->lbl_Alert->foregroundRole(),Qt::black);
                ui->lbl_Alert->setPalette(pal);

                emit requestAnalysis(dataCtrl.controlCfgPath,&dataCtrl.rawStream,dataCtrl.resultsPath,dataCtrl.movWinConfidence/100);

            }
            //Perform run-time moving window analysis every 5 minutes (after the initial 10 minutes)
            else if ((dataCtrl.numMeas >=10) && (dataCtrl.numMeas%5 == 0))
            {
                dataCtrl.analysisRunning = true;
                emit requestAnalysis(dataCtrl.controlCfgPath,&dataCtrl.rawStream,dataCtrl.resultsPath,dataCtrl.movWinConfidence/100);
            }

            break;
        }
        case testData::TEST_EXPOSURE_COMPLETE:
        {
            maxMinVal(dataArr);
            //dataCtrl.numMeas = dataCtrl.numMeas +1;
            this->plotData(dataArr);
            dataCtrl.addSaveFileEntry(dataArr);
            dataCtrl.addRawFileEntry(dataArr);
            break;
        }
        case testData::POSITIVE_CONTROL_EXPOSURE:
        {
            maxMinVal(dataArr);
            dataCtrl.numMeas = dataCtrl.numMeas +1;
            this->plotData(dataArr);
            dataCtrl.addSaveFileEntry(dataArr);
            dataCtrl.addRawFileEntry(dataArr);

            //If measurement period has expired
            if (dataCtrl.measLeft <=0)
            {
                dataCtrl.analysisRunning = true;
                dataCtrl.currentState = testData::POSITIVE_CONTROL_COMPLETE;
                ui->lbl_userInstruct->setText("Performing Analysis...");

                //Set Alert as temp indicator
                ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
                ui->lbl_Alert->setText("Temp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setVisible(true);

                QPalette pal = ui->lbl_Alert->palette();
                pal.setColor(ui->lbl_Alert->foregroundRole(),Qt::black);
                ui->lbl_Alert->setPalette(pal);

                emit requestAnalysis(dataCtrl.controlCfgPath,&dataCtrl.rawStream,dataCtrl.posResultsPath,dataCtrl.movWinConfidence/100);

            }
            //Perform run-time moving window analysis every 5 minutes (after the initial 10 minutes)
            else if ((dataCtrl.numMeas >=10) && (dataCtrl.numMeas%5 == 0))
            {
                dataCtrl.analysisRunning = true;
                emit requestAnalysis(dataCtrl.controlCfgPath,&dataCtrl.rawStream,dataCtrl.posResultsPath,dataCtrl.movWinConfidence/100);
            }

            break;
        }
        default:
    {
        break;
    }



    }

}

void MainWindow::rxEcisAck(bool ack)
{
    //Error opening UART

    if (ack == false)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("ECIS Hardware Error");
        msgBox->setInfo("Error connecting to measurement subsystem.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
}

//Function: clearPlot
//Description: Used to reinitialize plot for new test. Updates plot widget to initial state
void MainWindow::clearPlot()
{
    c1Arr.clear();
    c2Arr.clear();
    c3Arr.clear();
    c4Arr.clear();
    s1Arr.clear();
    s2Arr.clear();
    s3Arr.clear();
    s4Arr.clear();
    c1NormArr.clear();
    c2NormArr.clear();
    c3NormArr.clear();
    c4NormArr.clear();
    s1NormArr.clear();
    s2NormArr.clear();
    s3NormArr.clear();
    s4NormArr.clear();
    vLine1Arr.clear();
    vLine1Tarr.clear();
    vLine2Arr.clear();
    vLine2Tarr.clear();

    normTimeArr.clear();
    timeArr.clear();
    timeStamp = 0;
    dataCtrl.maxReading = 0;
    dataCtrl.minReading = 0;
    dataCtrl.maxNormReading = 1;
    dataCtrl.minNormReading = 1;

    c1Norm = 0;
    c2Norm = 0;
    c3Norm = 0;
    c4Norm = 0;
    s1Norm = 0;
    s2Norm = 0;
    s3Norm = 0;
    s4Norm = 0;

    //Point graph plots to data
    ui->plot_Main->graph(0)->setData(timeArr,c1Arr);
    ui->plot_Main->graph(1)->setData(timeArr,c2Arr);
    ui->plot_Main->graph(2)->setData(timeArr,c3Arr);
    ui->plot_Main->graph(3)->setData(timeArr,c4Arr);
    ui->plot_Main->graph(4)->setData(timeArr,s1Arr);
    ui->plot_Main->graph(5)->setData(timeArr,s2Arr);
    ui->plot_Main->graph(6)->setData(timeArr,s3Arr);
    ui->plot_Main->graph(7)->setData(timeArr,s4Arr);
    ui->plot_Main->graph(8)->setData(vLine1Tarr,vLine1Arr);
    ui->plot_Main->graph(9)->setData(vLine2Tarr,vLine2Arr);

    //redraw graph
    //ui->plot_Main->yAxis->setRange(0,200000);
    ui->plot_Main->xAxis->setRange(0,timeStamp+5);
    ui->plot_Main->replot();

    //update data fields
    ui->lbl_CE1->setText("------");
    ui->lbl_CE2->setText("------");
    ui->lbl_CE3->setText("------");
    ui->lbl_CE4->setText("------");
    ui->lbl_SE1->setText("------");
    ui->lbl_SE2->setText("------");
    ui->lbl_SE3->setText("------");
    ui->lbl_SE4->setText("------");

    //Global graphing vars
    dataCtrl.enNormalized = false;
    dataCtrl.normalized = false;

    //Reset graph flags and buttons
    ui->bttn_normalize->setPalette(palUnselected);
    ui->bttn_normalize->setEnabled(false);
    ui->bttn_normalize->update();

}

//Function: updatePlot
//Description: Updates graph with applied graph settings and current data
void MainWindow::updatePlot()
{
    //Point graph plots to data, based off graph options
    if (dataCtrl.normalized == false)
    {

        ui->plot_Main->yAxis->setLabel("Impedance (Ohms)");

        //Absolute impedance
        ui->plot_Main->graph(0)->setData(timeArr,c1Arr);
        ui->plot_Main->graph(1)->setData(timeArr,c2Arr);
        ui->plot_Main->graph(2)->setData(timeArr,c3Arr);
        ui->plot_Main->graph(3)->setData(timeArr,c4Arr);
        ui->plot_Main->graph(4)->setData(timeArr,s1Arr);
        ui->plot_Main->graph(5)->setData(timeArr,s2Arr);
        ui->plot_Main->graph(6)->setData(timeArr,s3Arr);
        ui->plot_Main->graph(7)->setData(timeArr,s4Arr);

        //redraw graph
        if (dataCtrl.autoScale)
            ui->plot_Main->yAxis->setRange(0,dataCtrl.maxReading + dataCtrl.maxReading*.1);
        else
            ui->plot_Main->yAxis->setRange(dataCtrl.yMin,dataCtrl.yMax);

        //Set x-axis
        ui->plot_Main->xAxis->setRange(0,timeStamp+10);
        ui->plot_Main->replot();
    }
    else
    {
        ui->plot_Main->yAxis->setLabel("Normalized Impedance");

        //Normalized data
        ui->plot_Main->graph(0)->setData(normTimeArr,c1NormArr);
        ui->plot_Main->graph(1)->setData(normTimeArr,c2NormArr);
        ui->plot_Main->graph(2)->setData(normTimeArr,c3NormArr);
        ui->plot_Main->graph(3)->setData(normTimeArr,c4NormArr);
        ui->plot_Main->graph(4)->setData(normTimeArr,s1NormArr);
        ui->plot_Main->graph(5)->setData(normTimeArr,s2NormArr);
        ui->plot_Main->graph(6)->setData(normTimeArr,s3NormArr);
        ui->plot_Main->graph(7)->setData(normTimeArr,s4NormArr);

        //redraw graph - normalized graph is always autoscaled
         ui->plot_Main->yAxis->setRange(dataCtrl.minNormReading - dataCtrl.minNormReading*.1,dataCtrl.maxNormReading + dataCtrl.maxNormReading*.1);

         //Set x-axis
         ui->plot_Main->xAxis->setRange(normTimeArr[0],timeStamp+10);
         ui->plot_Main->replot();
    }
}

//Function: PlotData
//Description: updates current plot with new data point, appended at to existing data. Graph will be updated to settings set by the graph options tab.
void MainWindow::plotData(double (&data)[8])
{
    //Always add data to global QVectors
    c1Arr.append(data[0]);
    c2Arr.append(data[1]);
    c3Arr.append(data[2]);
    c4Arr.append(data[3]);
    s1Arr.append(data[4]);
    s2Arr.append(data[5]);
    s3Arr.append(data[6]);
    s4Arr.append(data[7]);

    //For normalized data, don't normalize until beginning of exposure period
    if ((dataCtrl.currentState == testData::TEST_EXPOSURE) || (dataCtrl.currentState == testData::TEST_EXPOSURE_COMPLETE) || (dataCtrl.currentState == testData::POSITIVE_CONTROL_EXPOSURE))
    {
        c1NormArr.append(data[0]/c1Norm);
        c2NormArr.append(data[1]/c2Norm);
        c3NormArr.append(data[2]/c3Norm);
        c4NormArr.append(data[3]/c4Norm);
        s1NormArr.append(data[4]/s1Norm);
        s2NormArr.append(data[5]/s2Norm);
        s3NormArr.append(data[6]/s3Norm);
        s4NormArr.append(data[7]/s4Norm);

        normTimeArr.append(timeStamp);

    }




    timeArr.append(timeStamp);
    timeStamp = timeStamp+1;

    //Point graph plots to data, based off graph options
    if (dataCtrl.normalized == false)
    {
        //Absolute impedance
        ui->plot_Main->graph(0)->setData(timeArr,c1Arr);
        ui->plot_Main->graph(1)->setData(timeArr,c2Arr);
        ui->plot_Main->graph(2)->setData(timeArr,c3Arr);
        ui->plot_Main->graph(3)->setData(timeArr,c4Arr);
        ui->plot_Main->graph(4)->setData(timeArr,s1Arr);
        ui->plot_Main->graph(5)->setData(timeArr,s2Arr);
        ui->plot_Main->graph(6)->setData(timeArr,s3Arr);
        ui->plot_Main->graph(7)->setData(timeArr,s4Arr);

        //redraw graph
        if (dataCtrl.autoScale)
            ui->plot_Main->yAxis->setRange(0,dataCtrl.maxReading + dataCtrl.maxReading*.1);
        else
            ui->plot_Main->yAxis->setRange(dataCtrl.yMin,dataCtrl.yMax);

        //Set x-axis
        ui->plot_Main->xAxis->setRange(0,timeStamp+10);
        ui->plot_Main->replot();
    }
    else
    {
        //Normalized data
        ui->plot_Main->graph(0)->setData(normTimeArr,c1NormArr);
        ui->plot_Main->graph(1)->setData(normTimeArr,c2NormArr);
        ui->plot_Main->graph(2)->setData(normTimeArr,c3NormArr);
        ui->plot_Main->graph(3)->setData(normTimeArr,c4NormArr);
        ui->plot_Main->graph(4)->setData(normTimeArr,s1NormArr);
        ui->plot_Main->graph(5)->setData(normTimeArr,s2NormArr);
        ui->plot_Main->graph(6)->setData(normTimeArr,s3NormArr);
        ui->plot_Main->graph(7)->setData(normTimeArr,s4NormArr);

        //redraw graph - normalized graph is always autoscaled
         ui->plot_Main->yAxis->setRange(dataCtrl.minNormReading - dataCtrl.minNormReading*.1,dataCtrl.maxNormReading + dataCtrl.maxNormReading*.1);

         //Set x-axis
         ui->plot_Main->xAxis->setRange(normTimeArr[0],timeStamp+10);
         ui->plot_Main->replot();
    }








    //update data fields
    ui->lbl_CE1->setText(QString::number(data[0]));
    ui->lbl_CE2->setText(QString::number(data[1]));
    ui->lbl_CE3->setText(QString::number(data[2]));
    ui->lbl_CE4->setText(QString::number(data[3]));
    ui->lbl_SE1->setText(QString::number(data[4]));
    ui->lbl_SE2->setText(QString::number(data[5]));
    ui->lbl_SE3->setText(QString::number(data[6]));
    ui->lbl_SE4->setText(QString::number(data[7]));
}

//Used to insert vertical line into graph for identification of normalization point
void MainWindow::insertVLinPlot()
{
    //initialize Arr
    vLine1Arr.clear();
    vLine1Tarr.clear();

    //Add two values to time and data arrays (roughly a vertical line)
    vLine1Arr.append(0);
    vLine1Arr.append(30000);

    vLine1Tarr.append(timeStamp-1);
    vLine1Tarr.append(timeStamp-1+.001);

    ui->plot_Main->graph(8)->setData(vLine1Tarr,vLine1Arr);
    ui->plot_Main->replot();
}

//Used to insert vertical line into graph for identification of positive control start interval
void MainWindow::insertVLinPlot2()
{
    //initialize Arr
    vLine2Arr.clear();
    vLine2Tarr.clear();

    //Add two values to time and data arrays (roughly a vertical line)
    vLine2Arr.append(0);
    vLine2Arr.append(30000);

    vLine2Tarr.append(timeStamp-1);
    vLine2Tarr.append(timeStamp-1+.001);

    ui->plot_Main->graph(9)->setData(vLine2Tarr,vLine2Arr);
    ui->plot_Main->replot();
}

void MainWindow::rxTecTempMeasure(double temp)
{
    if (tecCtrl->isConnected())
        dataCtrl.recentTemp = temp;
    else
        dataCtrl.recentTemp = -1;

}

void MainWindow::rxTecSetpoint(double setPt)
{
    //Receive TEC Setpoint
}

void MainWindow::rxTecAck(bool ack)
{
    if (!ack)
    {
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("TEC Error");
        msgBox->setInfo("Error connecting to TEC subsystem.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    else
        emit requestTecMeasurement();
}

//Slot called once per minute to update battery status
void MainWindow::onBattTimerInterval()
{
    //Request battery charge value
    emit requestBatteryStatus();

    //need to adjust frequency of timer after first, immediate trigger
    if (battTimer->interval() != 10000)
        battTimer->start(100000);

}

//Slot for receiving password complete signal
void MainWindow::passwordComplete(bool valid, int window)
{
    if (valid == false)
    {
        idleTimeCount = 0;
        emit txAutoShutoffWinShow(false);

        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Password Invalid");
        msgBox->setInfo("The entered password is incorrect.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    else
    {
        if (window == 1)
        {
            idleTimeCount = 0;
            emit txAutoShutoffWinShow(false);

            dlgTestSettings *settingWin = new dlgTestSettings(&dataCtrl);
            connect(settingWin,SIGNAL(tecSettingChanged()),this,SLOT(tecSettingsChanged()));
            connect(settingWin,SIGNAL(txInteraction()),this,SLOT(rxIdleInteraction()));
            settingWin->show();
        }
        else if (window == 2)
        {
            idleTimeCount = 0;
            emit txAutoShutoffWinShow(false);

            dlgDataFile *dataWin = new dlgDataFile();
            connect(dataWin,SIGNAL(txControlName(QString)),this,SLOT(rxControlFile(QString)));
            connect(dataWin,SIGNAL(txInteraction()),this,SLOT(rxIdleInteraction()));
            connect(dataWin,SIGNAL(requestUsbUpdate()),usbDriveCtrl,SLOT(updateUsbMount()));
            connect(usbDriveCtrl,SIGNAL(txUsbStatus(bool)),dataWin,SLOT(rxUsbUpdate(bool)));
            dataWin->show();
        }
        else if (window == 3)
        {
            idleTimeCount = 0;
            emit txAutoShutoffWinShow(false);

            dlgSoftwareUpdate *updateWin = new dlgSoftwareUpdate();
            connect(updateWin,SIGNAL(txControlName(QString)),this,SLOT(rxControlFile(QString)));
            connect(updateWin,SIGNAL(requestUsbUpdate()),usbDriveCtrl,SLOT(updateUsbMount()));
            connect(usbDriveCtrl,SIGNAL(txUsbStatus(bool)),updateWin,SLOT(rxUsbUpdate(bool)));
            updateWin->show();
        }
        else if (window == 4)
        {
            idleTimeCount = 0;
            emit txAutoShutoffWinShow(false);

            dlgControlSettings *controlWin = new dlgControlSettings();
            connect(this,SIGNAL(txControlName(QString)),controlWin,SLOT(rxControlName(QString)));
            connect(controlWin,SIGNAL(txControlName(QString)),this,SLOT(rxControlFile(QString)));
            connect(controlWin,SIGNAL(txInteraction()),this,SLOT(rxIdleInteraction()));
            connect(controlWin,SIGNAL(requestUsbUpdate()),usbDriveCtrl,SLOT(updateUsbMount()));
            connect(usbDriveCtrl,SIGNAL(txUsbStatus(bool)),controlWin,SLOT(rxUsbUpdate(bool)));
            emit txControlName(dataCtrl.controlName);
            controlWin->show();
        }
    }

}

//Slot for receiving control file name from control configuration window
void MainWindow::rxControlFile(QString control)
{
    dataCtrl.controlName = control;
    dataCtrl.writeConfigFile();
}

//Slot for measurement timer interval
void MainWindow::onMeasurementTimerInterval()
{
    //Action depends on current application state
    switch(dataCtrl.currentState)
    {
        case testData::PRE_EXPOSURE:
        {
            emit requestEcisMeasurement();
            emit requestTecMeasurement();

            if (dataCtrl.DEBUG_sbcOnly == true)
            {
                dataCtrl.recentTemp = dataCtrl.sampNum;
            }
            break;
        }
        case testData::PRE_EXPOSURE_COMPLETE:
        {
            emit requestEcisMeasurement();
            emit requestTecMeasurement();

            if (dataCtrl.DEBUG_sbcOnly == true)
            {
                dataCtrl.recentTemp = dataCtrl.sampNum;
            }
            break;
        }
        case testData::TEST_EXPOSURE:
        {
            emit requestEcisMeasurement();
            emit requestTecMeasurement();

            if (dataCtrl.DEBUG_sbcOnly == true)
            {
                dataCtrl.recentTemp = dataCtrl.sampNum;
            }
            break;
        }
        case testData::TEST_EXPOSURE_COMPLETE:
        {
            emit requestEcisMeasurement();
            emit requestTecMeasurement();

            if (dataCtrl.DEBUG_sbcOnly == true)
            {
                dataCtrl.recentTemp = dataCtrl.sampNum;
            }
            break;
        }
        case testData::POSITIVE_CONTROL_EXPOSURE:
        {
            emit requestEcisMeasurement();
            emit requestTecMeasurement();
            break;
        }


    }

}

//Slot called whenever user clicks on the "Next" Button
void MainWindow::on_bttn_Next_clicked()
{
    //reset idle counter
    idleTimer = 0;

    //Action depends on current application state
    switch(dataCtrl.currentState)
    {
        case testData::INIT:
        {

            //Make sure the control has been specified
            if (dataCtrl.controlName.size()==0)
            {
                //If control file has not been specified, raise an error
                dlgMessageBox *msgBox= new dlgMessageBox(this);
                msgBox->setTitle("Control File Error");
                msgBox->setInfo("There is no currently selected control file set. Please select one in the Control File Management panel.");
                msgBox->setOKButton(true,"Ok");
                msgBox->setCancelButton(false,"Cancel");
                msgBox->exec();

                return;
            }

            //Make sure the control file exists and is valid
            //Check and make sure a control file is selected
            QVector<QString> controlFiles;
            qDebug() << dataCtrl.controlCfgPath;
            QFile file(dataCtrl.controlCfgPath);

            //file check
            if (file.exists() == false)
            {
                //If control file has not been specified, raise an error
                dlgMessageBox *msgBox= new dlgMessageBox(this);
                msgBox->setTitle("Control File Error");
                msgBox->setInfo("The currently specified control file could not be opened. Please select one in the Control File Management panel.");
                msgBox->setOKButton(true,"Ok");
                msgBox->setCancelButton(false,"Cancel");
                msgBox->exec();
                return;
            }

            file.open(QIODevice::ReadOnly | QIODevice::Text);
            QTextStream fileStream(&file);

            QString rxStr = fileStream.readLine();

            while (QString::compare("*START*",rxStr)!=0)
                rxStr = fileStream.readLine();

            //Once start sequence is found, store file names in controlFiles until end is found
            int i=0;
            rxStr = fileStream.readLine();
            while ((QString::compare("*END*",rxStr)!=0)&&(fileStream.atEnd()==false))
            {
                 controlFiles.resize(i+1);
                 controlFiles[i] = "/home/root/Resources/" + rxStr;
                 rxStr = fileStream.readLine();
                 i++;
             }

             //Ensure that at least one control file is read
             if(controlFiles.size() == 0)
             {
                 //If control file has not been specified, raise an error
                 dlgMessageBox *msgBox= new dlgMessageBox(this);
                 msgBox->setTitle("Control File Error");
                 msgBox->setInfo("The currently specified control file set does not contain any data files. Please select a new control file in the Control File Management panel.");
                 msgBox->setOKButton(true,"Ok");
                 msgBox->setCancelButton(false,"Cancel");
                 msgBox->exec();
                 return;
             }


             file.close();

             //Make sure that the ECIS hardware is connected
             if(!ecisCtrl->isConnected())
             {
                 //If control file has not been specified, raise an error
                 dlgMessageBox *msgBox= new dlgMessageBox(this);
                 msgBox->setTitle("ECIS Hardware Error");
                 msgBox->setInfo("An issue exists with the ECIS hardware. Please restart if this issue persists.");
                 msgBox->setOKButton(true,"Ok");
                 msgBox->setCancelButton(false,"Cancel");
                 msgBox->exec();
                 return;
             }


            //Application init: change GUI and request a single ECIS measurement for pre-check
            dataCtrl.currentState = testData::IMP_CHECK_IN_PROGRESS;
            ui->bttn_Next->setVisible(false);
            ui->lbl_userInstruct->setText("Performing Impedance Measurements...");
            lockGui();
            emit requestEcisMeasurement();
            dataCtrl.chipDisconnect = false;
            audio_al_st = false;



            break;
        }
        case testData::PRE_EXPOSURE_COMPLETE:
        {
            //End of pre-exposure period, user acks message
            dataCtrl.currentState = testData::TEST_EXPOSURE;
            ui->bttn_Next->setVisible(false);

            dataCtrl.numMeas = 0;
            dataCtrl.insertNormPoint();
            dataCtrl.insertMarker();
            dataCtrl.enNormalized = true;


            //Set normalization point channel values
            c1Norm = dataCtrl.recentDataArr[0];
            c2Norm = dataCtrl.recentDataArr[1];
            c3Norm = dataCtrl.recentDataArr[2];
            c4Norm = dataCtrl.recentDataArr[3];
            s1Norm = dataCtrl.recentDataArr[4];
            s2Norm = dataCtrl.recentDataArr[5];
            s3Norm = dataCtrl.recentDataArr[6];
            s4Norm = dataCtrl.recentDataArr[7];

            c1NormArr.append(1);
            c2NormArr.append(1);
            c3NormArr.append(1);
            c4NormArr.append(1);
            s1NormArr.append(1);
            s2NormArr.append(1);
            s3NormArr.append(1);
            s4NormArr.append(1);
            normTimeArr.append(timeStamp);


            dataCtrl.normalized = true;
            //Enable normalization button
            ui->bttn_normalize->setPalette(palSelected);
            ui->bttn_normalize->setEnabled(true);
            ui->bttn_normalize->update();

            //Set max/min normalipowerDownTrig()zed initialization values for graphing
            ui->plot_Main->yAxis->setRange(.9,1.1);


            dataCtrl.measLeft = dataCtrl.testDuration;
            dataCtrl.secondsLeft = dataCtrl.testDuration*60;

            popUp->setMessage("Inject control and sample NOW\n\nDo NOT remove syringes until the end of the test");
            popUp->setBackgroundColor(QColor(64,219,111,163));
            popUp->setBlinkingNum(10);

            insertVLinPlot();

            ui->lbl_Alert->setVisible(false);

            updateGuiTime();
            updatePlot();

            audio_al_st = false;

            break;
        }
        case testData::TEST_EXPOSURE:
        {
            nextFilter = true;  //for some reason, clicked signal is emmitted twice. Use filter to prevent erroneous application behavior

            //User has selected end test following run-time analysis positive result
            dataCtrl.currentState = testData::TEST_EXPOSURE_COMPLETE;
            ui->lbl_userInstruct->setText("Test Complete");
            ui->bttn_Next->setText("New Test");
            ui->bttn_Results->setVisible(true);
            dataCtrl.addResultToRecords(true);

        }
        case testData::TEST_EXPOSURE_COMPLETE:
        {
            if (nextFilter == false)
            {
                //Sample exposure period has ended and user has selected "New Test"
                dataCtrl.currentState = testData::INIT;
                unlockGui();
                dataCtrl.reInit();
                ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
                ui->bttn_Results->setVisible(false);
                ui->lbl_Alert->setVisible(false);
                ui->bttn_Next->setText("Next");
                ui->bttn_posControl->setVisible(false);
                clearPlot();
            }
            else
                nextFilter = false;

            break;
        }
        case testData::POSITIVE_CONTROL_EXPOSURE:
        {
            //nextFilter = true;  //for some reason, clicked signal is emmitted twice. Use filter to prevent erroneous application behavior

            //User has selected end test following run-time analysis positive result
            dataCtrl.currentState = testData::POSITIVE_CONTROL_COMPLETE;
            ui->lbl_userInstruct->setText("Test Complete");
            ui->bttn_Next->setText("New Test");
            ui->bttn_Results->setVisible(true);
            //dataCtrl.addResultToRecords(true);
        }
        case testData::POSITIVE_CONTROL_COMPLETE:
        {
            //Positive exposure period has ended and user has selected "New Test"
            dataCtrl.currentState = testData::INIT;
            unlockGui();
            dataCtrl.reInit();
            ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
            ui->bttn_Results->setVisible(false);
            ui->lbl_Alert->setVisible(false);
            ui->bttn_Next->setText("Next");
            clearPlot();
        }


    }
}

//Slot called from pre-check window to request ecis measurement if cartridge initially failed
void MainWindow::rxImpPreCheckEcisRequest()
{
    dataCtrl.currentState =testData::IMP_CHECK_FAIL;
    emit requestEcisMeasurement();
}

//Slot called from GuiTimer timeout for GUI time updates
//Note: chip disconnect interrupts are processed on this slot as well due to the one second latency
void MainWindow::onGuiTimerInterval()
{
    intControl->readLines();

    switch(dataCtrl.currentState)
    {


        case testData::INIT:
        {
            //Set Alert as temp indicator
            ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
            if (tecCtrl->isConnected())
                if ((dataCtrl.recentTemp > 0) && (dataCtrl.recentTemp <=100))
                    ui->lbl_Alert->setText("Temperature: " + QString::number(dataCtrl.recentTemp,'f',0) + QString::fromUtf8(" °C"));
                else
                    ui->lbl_Alert->setText("Temperature: -----"  + QString::fromUtf8(" °C"));
            else
            {
                if (tecCtrl->isInitialized())
                    ui->lbl_Alert->setText("Temperature: ERROR");
                else
                    ui->lbl_Alert->setText("Temperature: -----"  + QString::fromUtf8(" °C"));
            }

            ui->lbl_Alert->setVisible(true);

            QPalette pal = ui->lbl_Alert->palette();
            pal.setColor(ui->lbl_Alert->foregroundRole(),Qt::black);
            ui->lbl_Alert->setPalette(pal);

            break;
        }
        case testData::PRE_EXPOSURE:
        {
            numSecsElapsed = numSecsElapsed +1;
            if ((dataCtrl.chipDisconnect == true) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->setAttribute(Qt::WA_ShowWithoutActivating);
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);
                dataCtrl.warningDisplayed = true;
            }

            dataCtrl.secondsLeft = dataCtrl.secondsLeft - 1;
            updateGuiTime();
            break;
        }
        case testData::PRE_EXPOSURE_COMPLETE:
        {
            if ((dataCtrl.chipDisconnect == true) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->setAttribute(Qt::WA_ShowWithoutActivating);
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);
                dataCtrl.warningDisplayed = true;
            }

            ui->lbl_userInstruct->setText("Pre-exposure complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) + QString::fromUtf8(" °C"));

            if (audio_al_st == false)
            {
                audio_al_st = true;
                playAlert();
            }

            break;
        }
        case testData::TEST_EXPOSURE:
        {
            if ((dataCtrl.chipDisconnect == true) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->setAttribute(Qt::WA_ShowWithoutActivating);
                //errorWin->show();\

                playAlert();
                emit txErrorWinShow(true);
                dataCtrl.warningDisplayed = true;
            }

            dataCtrl.secondsLeft = dataCtrl.secondsLeft - 1;
            updateGuiTime();
            break;
        }
        case testData::TEST_EXPOSURE_COMPLETE:
        {

            if (dataCtrl.analysisRunning == false)
            {
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
            }
            else
            {
                ui->lbl_userInstruct->setText("Performing Analysis...\t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
            }


            if ((dataCtrl.chipDisconnect == true) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->setAttribute(Qt::WA_ShowWithoutActivating);
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);
                dataCtrl.warningDisplayed = true;
            }

            break;
        }
        case testData::POSITIVE_CONTROL_EXPOSURE:
        {

        if ((dataCtrl.chipDisconnect == true) && (dataCtrl.warningDisplayed == false))
            {
                //Error was detected. Launch error window
                //dlgRunTimeError *errorWin = new dlgRunTimeError();
                //connect(errorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
                //errorWin->show();
                playAlert();
                emit txErrorWinShow(true);
                dataCtrl.warningDisplayed = true;
            }

            dataCtrl.secondsLeft = dataCtrl.secondsLeft - 1;
            updateGuiTime();
            break;
        }
        case testData::POSITIVE_CONTROL_COMPLETE:
        {
            if (dataCtrl.analysisRunning == false)
            {
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
            }
            else
            {
                ui->lbl_userInstruct->setText("Performing Analysis...\t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
            }

            break;
        }
        default:
            break;
    }

}

//Private function updates GUI label based on current test state
void MainWindow::updateGuiTime()
{

    QString timeStr, tempStr;
    int h, m;
    int s = dataCtrl.secondsLeft;
    h = s/3600;
    s = s%3600;
    m = s /60;
    s = s%60;

    QTime timeDur;
    timeDur.setHMS(h,m,s);
    timeStr = timeDur.toString("hh:mm:ss");

    if (timeStr.length() == 0)
        timeStr = "00:00:00";


    tempStr = "Time Remaining: " + timeStr + "\t\tTemp: " + QString::number(dataCtrl.recentTemp) +  QString::fromUtf8(" °C");

    ui->lbl_userInstruct->setText(tempStr);
}

//Slot called from tempTimer timeout for heatsink temperature monitoring
//Note: TEC overheating is detected on this slot
void MainWindow::onTempTimerInterval()
{
    double temp = dataCtrl.recentTemp;

    if (temp > 74)
    {
        //Heatsink temperature threshold exceeded
        emit requestTecDisable();
        //testData->tecEn = false;

        dlgTempError *tempErrorWin = new dlgTempError();
        connect(tempErrorWin,SIGNAL(txUserAck(bool)),this,SLOT(rxRunTimeErrorAck(bool)));
        tempErrorWin->setAttribute(Qt::WA_ShowWithoutActivating);
        tempErrorWin->show();

    }
}

//Private slot to evaluate idle shutdown sequences
void MainWindow::onIdleTimerInterval()
{
    //If idle timer is disabled, return
    if (dataCtrl.enableAutoShutoff == false)
        return;

    //If application is at the init state, or a test has finished, increment idle timer
    if ((dataCtrl.currentState == testData::INIT) || (dataCtrl.currentState == testData::POSITIVE_CONTROL_COMPLETE) || (dataCtrl.currentState == testData::TEST_EXPOSURE_COMPLETE))
    {
        idleTimeCount = idleTimeCount +1;

        //If timer expires, show warning for one minute
        if (idleTimeCount == dataCtrl.shutoffTime*60)
        {
            playAlert();
            emit txAutoShutoffWinShow(true);
        }

        //After one minute, shut down ESB Reader
        if (idleTimeCount == dataCtrl.shutoffTime*60+60)
        {
            idleTimeCount = 0;
            autoShutdownTrig = true;
            //emit txShutdownRequest();
            On_menuPowerOff();
        }
    }
    else
    {
        //If application is in any other state, reset timer to zero
        idleTimer = 0;
        emit txAutoShutoffWinShow(false);
    }
}

//Private slot to measure temperature during initial state
void MainWindow::onTempMeasureInitInterval()
{
        if (dataCtrl.currentState == testData::INIT)
        {
            emit requestTecMeasurement();
        }
}

//Private function reads the heatsink temperature from the I2C bus (TC74)
int MainWindow::checkHeatsinkTemp()
{
    return -1;
}

//Private function reads the ambient temperature from the I2C bus (LM73)
int MainWindow::checkAmbientTemp()
{
    return -1;
}

//Used to recieve interrupt events from interrupt monitor thread
void MainWindow::onIntRx(int id, int val)
{
    QString mssg;

    int num;

    //disregard interrupts from the first 10 seconds
    //if ((dataCtrl.currentState == testData::PRE_EXPOSURE) &&(numSecsElapsed < 10))
    //    id = 3;

    //disregard interrupts from application states before the test start

    if ((dataCtrl.currentState == testData::INIT) ||
            (dataCtrl.currentState == testData::IMP_CHECK_IN_PROGRESS) || (dataCtrl.currentState == testData::IMP_CHECK_FAIL) ||
            (dataCtrl.currentState == testData::SAMPLE_NAME_INPUT))
    {
        id = 3;
    }


    switch(id)
    {
        case 0: //cartridge present signal
        {
            if (val == 1)
            {
                dataCtrl.chipDisconnect = true;
                //Error opening UART
            }
            break;
        }
        case 1: //cartridge present signal
        {
            if(val == 1)
            {
                //Error opening UART
                dataCtrl.chipDisconnect = true;
            }
            break;
        }
    }

}

//Utility function for finding the largest and smallest values (used for graph auto-scale)
void MainWindow::maxMinVal(double (&data)[8])
{
    int i;
    double min, max;

    min = data[0];
    max = data[0];

    //find local max / min
    for (i = 0; i < 8; i++)
    {
        if (data[i] < min)
            min = data[i];

        if (data[i] > max)
            max = data[i];
    }

    //Always use these values during impedance pre-check since no values have been assigned
    if (dataCtrl.currentState == testData::IMP_CHECK_IN_PROGRESS)
    {
        dataCtrl.maxReading = max;
        dataCtrl.minReading = min;
    }

    //set global values if needed
    if (max > dataCtrl.maxReading)
        dataCtrl.maxReading = max;

    if (min < dataCtrl.minReading)
        dataCtrl.minReading = min;

    //For test states that allow normalized graphing
    if ((dataCtrl.currentState == testData::TEST_EXPOSURE) || (dataCtrl.currentState == testData::TEST_EXPOSURE_COMPLETE) || (dataCtrl.currentState ==  testData::POSITIVE_CONTROL_EXPOSURE))
    {
        double nData[8];

        nData[0] = data[0]/c1Norm;
        nData[1] = data[1]/c2Norm;
        nData[2] = data[2]/c3Norm;
        nData[3] = data[3]/c4Norm;
        nData[4] = data[4]/s1Norm;
        nData[5] = data[5]/s2Norm;
        nData[6] = data[6]/s3Norm;
        nData[7] = data[7]/s4Norm;

        min = 1;
        max = 1;

        //find local max / min
        for (i = 0; i < 8; i++)
        {
            if (nData[i] < min)
                min = nData[i];

            if (nData[i] > max)
                max = nData[i];
        }

        //set global values if needed
        if (max > dataCtrl.maxNormReading)
            dataCtrl.maxNormReading = max;

        if (min < dataCtrl.minNormReading)
            dataCtrl.minNormReading = min;
    }
}

//Slot for receiving moving window analysis results
void MainWindow::rxAnalysisResults(bool isSafe)
{
    //Global flag
    dataCtrl.analysisRunning = false;

    //Determine action based on application state
    switch(dataCtrl.currentState)
    {
        case testData::TEST_EXPOSURE:   //Case Test Exposure - run-time analysis
        {
            if (isSafe == false)    //If sample is contaminated, allow user to end test
            {
                ui->lbl_Alert->setText("<font color='Red'>CONTAMINATED</font>");
                ui->lbl_Alert->setVisible(true);
                ui->bttn_Next->setText("End Test");
                ui->bttn_Next->setVisible(true);
            }
            else    //If sample is safe, take no action
            {
                ui->lbl_Alert->setVisible(false);
            }
            break;
        }
        case testData::TEST_EXPOSURE_COMPLETE:  //case - end of test exposure period
        {

            playAlert();
            dataCtrl.addResultToRecords(!(isSafe));

            if (isSafe  == false)
            {

                QPalette pal = ui->lbl_Alert->palette();
                pal.setColor(ui->lbl_Alert->foregroundRole(),Qt::black);
                ui->lbl_Alert->setPalette(pal);
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setText("<font color='Red'>CONTAMINATED</font>");
                ui->lbl_Alert->setVisible(true);
            }
            else
            {
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setText("<font color='Green'>NO CONTAMINATION DETECTED</font>");
                ui->lbl_Alert->setVisible(true);
                ui->bttn_posControl->setVisible(true);  //enable positive control if test was not contaminated

            }
            ui->bttn_Next->setText("New Test");
            ui->bttn_Next->setVisible(true);
            ui->bttn_Results->setVisible(true);

            break;
        }
        case testData::POSITIVE_CONTROL_EXPOSURE:   //Case - positive control run-time
        {
            if (isSafe == false)
            {
                ui->lbl_Alert->setText("<font color='Green'>SUCCESS</font>");
                ui->lbl_Alert->setVisible(true);
                ui->bttn_Next->setText("End Test");
                ui->bttn_Next->setVisible(true);
            }
            else
            {
                ui->lbl_Alert->setVisible(false);
            }
            break;
        }
        case testData::POSITIVE_CONTROL_COMPLETE:  //case - end of positive control exposure period
        {

            //dataCtrl.addResultToRecords(!(isSafe));
            playAlert();

            if (isSafe  == false)
            {
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setText("<font color='Green'>SUCCESS</font>");
                ui->lbl_Alert->setVisible(true);
            }
            else
            {
                ui->lbl_userInstruct->setText("Test Complete \t\tTemp: " + QString::number(dataCtrl.recentTemp,'f',0) +  QString::fromUtf8(" °C"));
                ui->lbl_Alert->setText("<font color='Red'>FAILED</font>");
                ui->lbl_Alert->setVisible(true);

            }
            ui->bttn_Next->setText("New Test");
            ui->bttn_Next->setVisible(true);
            ui->bttn_Results->setVisible(true);

            break;
        }
        default:
            break;
    }
}

//Slot that receives the charging state of the system
void MainWindow::rxBatteryChargeStatus(bool charging)
{
    battCharging = charging;

    if (battCharging)
        battAlertIssued = false;

    rxBatteryStatus(battCharge);


}

//Slot called when user clicks on results button, launches results window with triangle matrix
void MainWindow::on_bttn_Results_clicked()
{
    //reset the idle time count
    idleTimer = 0;

    dlgResultsForm *resultsWin = new dlgResultsForm();

    if (dataCtrl.currentState == testData::TEST_EXPOSURE_COMPLETE)
        resultsWin->loadFile(dataCtrl.resultsPath);
    if (dataCtrl.currentState == testData::POSITIVE_CONTROL_COMPLETE)
        resultsWin->loadFile(dataCtrl.posResultsPath);
    resultsWin->show();
}

//User has selected to perfom a positive control test
void MainWindow::on_bttn_posControl_clicked()
{
    //reset the idle time count
    idleTimer = 0;

    dataCtrl.startPosControlRaw();

    ui->bttn_posControl->setVisible(false);
    ui->bttn_Next->setVisible(false);
    ui->bttn_Results->setVisible(false);
    ui->lbl_Alert->setVisible(false);

    dataCtrl.currentState = testData::POSITIVE_CONTROL_EXPOSURE;

    dataCtrl.insertMarker();    //add marker to data file to indicate start of positive control period
    insertVLinPlot2();

    dataCtrl.numMeas = 0;
    dataCtrl.measLeft = dataCtrl.posCtrlDuration;
    dataCtrl.secondsLeft = dataCtrl.posCtrlDuration*60;

    updatePlot();

    //Create a temporary blinking popup
    popUp->setMessage("Inject positive control into test port NOW");
    popUp->setBackgroundColor(QColor(64,219,111,163));
    popUp->setBlinkingNum(10);
}

//Used to detect anomalies (run-time error detection)
//Returns true if an error exists
bool MainWindow::errCheck(double (&data)[8])
{
    int i;
    double max,  min, range;

    max = data[0];
    min = data[0];

    //Check to see if any single measurement is out of range
    for(i=0;i<8;i++)
    {
        if (data[i] < dataCtrl.minImpRun)
            return true;

        if (data[i] > dataCtrl.maxImpRun)
            return true;
    }

    //find max and min of dataset for total range
    for(i=0;i<8;i++)
    {
        if (data[i] < min)
            min = data[i];

        if (data[i] > max)
            max = data[i];
    }

    range = max - min;

    if (dataCtrl.sampNum > dataCtrl.timeDelay)
    {
        if (range > dataCtrl.validImpedanceRange)
            return true;
        else
            return false;
    }

    return false;
}

//Slot for receiving user ack from error dialogue
//If user selected to abort test, perform required action
void MainWindow::rxRunTimeErrorAck(bool abort)
{
    if (abort)
    {
        //User has selected to  abort test. Return application to the initial state

        popUp->setBlinking(false);
        dataCtrl.currentState = testData::INIT;
        unlockGui();
        dataCtrl.reInit();
        ui->lbl_userInstruct->setText("Insert chip array and click Next to initialize test");
        ui->bttn_Results->setVisible(false);
        ui->lbl_Alert->setVisible(false);
        ui->bttn_Next->setText("Next");
        ui->bttn_Next->setVisible(true);
        ui->bttn_posControl->setVisible(false);
        clearPlot();
        dataCtrl.warningDisplayed = false;
    }
    else
    {
        //If user dissmissed warning, reset flag
        dataCtrl.chipDisconnect = false;
        dataCtrl.warningDisplayed = false;
    }
}

//Function: lock Gui
//Disables all gui elements that could interfere during test execution
void MainWindow::lockGui()
{
    ui->actionTest_Settings->setEnabled(false);
    ui->actionData_File_Management->setEnabled(false);
    ui->actionAbort_Test->setEnabled(true);
    ui->actionSoftware_Update->setEnabled(false);
    ui->actionControl_File_Management->setEnabled(false);
    ui->actionRC_Test->setEnabled(false);

}

//Function: unlock Gui
//Re-enables all locked gui widgets
void MainWindow::unlockGui()
{
    ui->actionTest_Settings->setEnabled(true);
    ui->actionData_File_Management->setEnabled(true);
    ui->actionAbort_Test->setEnabled(false);
    ui->actionSoftware_Update->setEnabled(true);
    ui->actionControl_File_Management->setEnabled(true);
    ui->actionRC_Test->setEnabled(true);
}

//Function: screen Capture
//Takes a screenshot
void MainWindow::screenCapture()
{
    QPixmap pm = QPixmap::grabWidget(this);
    pm.save("/home/root/picture.BMP","BMP");
}

//Slot: RC Test complete
void MainWindow::rCCheckComplete()
{
    dataCtrl.currentState = testData::INIT;
}

//Slot: RC Test requested ECIS measurement
void MainWindow::rxRCCheckEcisRequest()
{
    emit requestEcisMeasurement();
}

//Function: plays alert sound, if audio is enabled
void MainWindow::playAlert()
{

    if (audio_en)
    {
        //QSound::play("Resources/chime.wav");
        emit txRequestBuzz(1);
    }
}

//Slot: User interacted with auto-shutoff warning
void MainWindow::rxAutoShutoffAck()
{
    idleTimeCount=0;
    emit txAutoShutoffWinShow(false);
}

//Slot: User has clicked the power down button
void MainWindow::rxPowerBttnClick()
{
    dlgMessageBox *msgBox= new dlgMessageBox(this);
    msgBox->setTitle("Power Down Requested");
    msgBox->setInfo("Are you sure you want to power down? The current test will be aborted.");
    msgBox->setOKButton(true,"Ok");
    msgBox->setCancelButton(true,"Cancel");
    int retVal = msgBox->exec();


    if (retVal == 1)
        emit txShutdownRequest();

}

//Slot: rxIdleInteraction
void MainWindow::rxIdleInteraction()
{
    idleTimeCount=0;
    emit txAutoShutoffWinShow(false);

}

//Slot: on_bttn_autoscale_clicked
//User clicked on autoscale toggle button
void MainWindow::on_bttn_autoscale_clicked()
{
    if (dataCtrl.autoScale)
    {
        dataCtrl.autoScale = false;
        ui->bttn_autoscale->setPalette(palUnselected);
        ui->bttn_autoscale->update();
    }
    else
    {
        dataCtrl.autoScale = true;
        ui->bttn_autoscale->setPalette(palSelected);
        ui->bttn_autoscale->update();
    }

    //Update arrows for scale enable status
    if ((!dataCtrl.autoScale) && (!dataCtrl.normalized))
    {
        ui->spin_maxGraph->setEnabled(true);
        ui->spin_minGraph->setEnabled(true);
    }
    else
    {
        ui->spin_maxGraph->setEnabled(false);
        ui->spin_minGraph->setEnabled(false);
    }

    rxGraphParams(dataCtrl.autoScale,dataCtrl.enNormalized,ui->spin_maxGraph->value(),ui->spin_minGraph->value());
}

//Slot: on_bttn_normalize_clicked
//User clicked on normalize toggle button
void MainWindow::on_bttn_normalize_clicked()
{
    if (dataCtrl.enNormalized)
    {
        dataCtrl.enNormalized = false;
        ui->bttn_normalize->setPalette(palUnselected);
        ui->bttn_normalize->update();
    }
    else
    {
        dataCtrl.enNormalized = true;
        ui->bttn_normalize->setPalette(palSelected);
        ui->bttn_normalize->update();
    }

    //Update arrows for scale enable status
    if ((!dataCtrl.autoScale) && (!dataCtrl.normalized))
    {
        ui->spin_maxGraph->setEnabled(true);
        ui->spin_minGraph->setEnabled(true);
    }
    else
    {
        ui->spin_maxGraph->setEnabled(false);
        ui->spin_minGraph->setEnabled(false);
    }

    rxGraphParams(dataCtrl.autoScale,dataCtrl.enNormalized,ui->spin_maxGraph->value(),ui->spin_minGraph->value());
}

//User changed maximum value
void MainWindow::on_spin_maxGraph_valueChanged(int arg1)
{
    if (arg1 <= ui->spin_minGraph->value())
    {
        //Error, value must be greater than the min
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Graph Axes Limit Error");
        msgBox->setInfo("The maximum value must be greater than the minimum.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->spin_maxGraph->setValue(ui->spin_minGraph->value()+1);
    }
    else if(arg1 <=2)
    {
        //Error, value must be greater than zero
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Graph Axes Limit Error");
        msgBox->setInfo("The maximum value must be greater than the minimum.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->spin_maxGraph->setValue(ui->spin_minGraph->value()+1);
    }
    else
        rxGraphParams(dataCtrl.autoScale,dataCtrl.enNormalized,ui->spin_maxGraph->value(),ui->spin_minGraph->value());
}

void MainWindow::on_spin_minGraph_valueChanged(int arg1)
{
    if (arg1 >= ui->spin_maxGraph->value())
    {
        //Error, value must be greater than the min
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Graph Axes Limit Error");
        msgBox->setInfo("The minimum value must be less than the maximum.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->spin_minGraph->setValue(ui->spin_maxGraph->value()-1);
    }
    else if(arg1 < 0)
    {
        //Error, value must be greater than the min
        dlgMessageBox *msgBox= new dlgMessageBox(this);
        msgBox->setTitle("Graph Axes Limit Error");
        msgBox->setInfo("The minimum value cannot be less than zero.");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();

        ui->spin_minGraph->setValue(ui->spin_maxGraph->value()-1);
    }

    rxGraphParams(dataCtrl.autoScale,dataCtrl.enNormalized,ui->spin_maxGraph->value(),ui->spin_minGraph->value());
}

