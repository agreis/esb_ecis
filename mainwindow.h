#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QVector>
#include <QMenuBar>
#include <QMenu>
#include <QPalette>
#include "testdata.h"
#include "eciscontrol.h"
#include "teccontrol.h"
#include "dlgpopupindicator.h"
#include "intmonitor.h"
#include "movingwindow.h"
#include "dlgruntimeerror.h"
#include "dlgautoshutoff.h"
#include "miscio.h"
#include "dlgmessagebox.h"
#include "powercontrol.h"
#include "usbmounter.h"

#include "ui_mainwindow.h"
#include "dlggraphoptions.h"
#include "dlgdatafile.h"
#include "dlgsoftwareinfo.h"
#include "dlgtestrecords.h"
#include "dlgtestsettings.h"
#include "testdata.h"
#include "eciscontrol.h"
#include "teccontrol.h"
#include "dlgpassword.h"
#include "dlgrctest.h"
#include "dlgprecheck.h"
#include "dlgfilename.h"
#include "dlgpopupindicator.h"
#include "dlgresultsform.h"
#include "dlgruntimeerror.h"
#include "dlgtemperror.h"
#include "movingwindow.h"
#include "dlgsoftwareupdate.h"
#include "dlgcontrolsettings.h"
#include "dlgautoshutoff.h"
#include "Simple_GPIO.h"
#include "miscio.h"


#include <QSound>
#include <QTimer>
#include <QApplication>
#include <QTextStream>
#include <QPalette>
#include <QPixmap>


//I2C includes
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

//UART
#include <termios.h>
#include <errno.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    virtual void mousePressEvent(QMouseEvent * event);
    ~MainWindow();


public slots:
    void On_menuDataFile();
    void On_menuControlFile();
    void On_menuSoftwareInfo();
    void On_menuTestRecords();
    void On_menuTestSettings();
    void On_menuAbortTest();
    void On_menuSoftwareUpdate();
    void On_menuRCTest();
    void On_menuAudioToggle();
    void On_menuPowerOff();
    void On_menuTrainingVideo();


    //Misc application flow slots
    void onPlotClick(QMouseEvent* eve);
    void tecSettingsChanged();
    void passwordComplete(bool valid, int win);
    void rxImpPreCheckEcisRequest();
    void preCheckComplete(bool passed);
    void rxFileName(QString fname);
    void rxGraphParams(bool autoS, bool norm, int max, int min);
    void rxRunTimeErrorAck(bool abort);
    void rxAutoShutoffAck();
    void rxControlFile(QString control);
    void rxRCCheckEcisRequest();
    void rCCheckComplete();
    void rxIdleInteraction();   //used to acknowledge any interaction with other dialogues

    //ECIS Slots
    void rxEcisData(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    void rxEcisAck(bool ack);

    //TEC Slots
    void rxTecTempMeasure(double temp);
    void rxTecSetpoint(double setPt);
    void rxTecAck(bool ack);

    //Moving window slots
    void rxAnalysisResults(bool isSafe);

    //Battery charge state
    void rxBatteryChargeStatus(bool charging);


signals:
    //ECIS signals
    void requestEcisMeasurement();
    void requestEcisAck();

    //TEC signals
    void requestTecMeasurement();
    void requestTecAck();
    void requestTecEnable();
    void requestTecDisable();
    void setTecSetpoint(double setPoint);

    //Power signal
    void requestBatteryStatus();

    //Program flow sync between different windows
    void txEcisDataReCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    void txFileNameValid(bool valid);
    void txControlName(QString control);
    void txErrorWinShow(bool show);
    void txEcisDataRCCheck(int c1, int c2, int c3, int c4, int s1, int s2, int s3, int s4);
    void txAutoShutoffWinShow(bool show);
    void txShutdownRequest();
    void txRequestBuzz(int sec);
    void txRcTol(double tol);

    //Moving window signals
    void requestAnalysis(QString cfgF, QTextStream *testF, QString resF, double n);



    
private slots:
    void on_bttn_Next_clicked();
    void onMeasurementTimerInterval();
    void onTempMeasureInitInterval();
    void onGuiTimerInterval();
    void onBattTimerInterval();
    void onTempTimerInterval();
    void onIdleTimerInterval();
    void onIntRx(int id, int val);
    void rxPowerBttnClick();
    void init();
    void rxBatteryStatus(int batVal);

    void on_bttn_Results_clicked();

    void on_bttn_posControl_clicked();
    bool eventFilter(QObject *, QEvent *);

    void on_bttn_autoscale_clicked();

    void on_bttn_normalize_clicked();

    void on_spin_maxGraph_valueChanged(int arg1);

    void on_spin_minGraph_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    testData dataCtrl;
    ecisControl* ecisCtrl;
    tecControl* tecCtrl;
    IntMonitor* intControl;
    miscIO* ioControl;
    powerControl *pwrControl;
    usbMounter *usbDriveCtrl;

    QThread* miscIoThread;
    QThread* ecisThread;
    QThread* tecThread;
    QThread* interruptThread;
    QThread* movWinThread;
    QThread* pwrThread;
    QThread* usbThread;

    QTimer* measurementTimer;
    QTimer* guiTimer;
    QTimer* battTimer;
    QTimer* idleTimer;
    QTimer* tempTimer;
    QTimer* initTempTimer;

    dlgPopupIndicator* popUp;
    movingWindow* movWin;
    bool nextFilter;    //used to prevent accidental trigger of next button
    int numSecsElapsed; //Used to disregard first 2 seconds of interrupts
    dlgRunTimeError *errorWin;
    dlgAutoShutoff *autoShutoffWin;
    bool audio_en;      //audio enable state
    bool audio_al_st;   //audio alert state, make sure there isn't a repeating alert every timer tick
    bool autoShutdownTrig;  //skip dialogue for power down


    //graph data vars and containers~~~~~~~~~~~~~~~~~~~~~~~
    //Impedance graph vars
    int timeStamp;
    QVector<double> timeArr;
    QVector<double> c1Arr;
    QVector<double> c2Arr;
    QVector<double> c3Arr;
    QVector<double> c4Arr;
    QVector<double> s1Arr;
    QVector<double> s2Arr;
    QVector<double> s3Arr;
    QVector<double> s4Arr;
    QVector<double> vLine1Arr;
    QVector<double> vLine1Tarr;
    QVector<double> vLine2Arr;
    QVector<double> vLine2Tarr;

    //norm graph vars
    QVector<double> normTimeArr;
    QVector<double> c1NormArr;
    QVector<double> c2NormArr;
    QVector<double> c3NormArr;
    QVector<double> c4NormArr;
    QVector<double> s1NormArr;
    QVector<double> s2NormArr;
    QVector<double> s3NormArr;
    QVector<double> s4NormArr;

    double c1Norm, c2Norm, c3Norm, c4Norm, s1Norm, s2Norm, s3Norm, s4Norm;
    int idleTimeCount;

    //graph vars for buttons
    QPalette palUnselected;
    QPalette palSelected;

    //UI for battery readout
    QMenuBar *battBar;
    QMenu *battIcon;
    QMenu *battReadout;

    //Battery vars
    bool battAlertIssued;
    bool battCharging;
    int battCharge;

    //Private functions
    void plotData(double (&data)[8]);
    void updatePlot();
    void insertVLinPlot();
    void insertVLinPlot2();
    void updateGuiTime();
    void clearPlot();
    int checkHeatsinkTemp();
    int checkAmbientTemp();
    void maxMinVal(double (&data)[8]);
    bool errCheck(double (&data)[8]);
    void lockGui();
    void unlockGui();
    void screenCapture();
    void playAlert();


};

#endif // MAINWINDOW_H
