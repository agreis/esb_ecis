/*
 *  MiscIO
 *
 *  Provides Miscellaneous I/O for various subsystems
 *
 **/

#include <stdlib.h>
#include <Simple_GPIO.h>
#include "miscio.h"

//UART
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <fcntl.h>


miscIO::miscIO(QObject *parent) :
               QObject(parent)
{
    int retVal;

    //Set up buzzer
    //retVal = gpio_omap_mux_setup("gpmc_oen_ren","11");       //GPIO2_3 mode 3, output
    gpio_export(67);
    retVal = gpio_set_dir(67,OUTPUT_PIN);

    //Set up buzzer
    //retVal = gpio_omap_mux_setup("gpmc_oen_ren","11");       //GPIO2_4 mode 3, output

    //Set up debug / level translator enable (3_19)
    gpio_export(115);
    retVal = gpio_set_dir(115,OUTPUT_PIN);
    gpio_set_value(115,LOW);

}

miscIO::~miscIO()
{

}

//Slot called to sound buzzer for 1 second
void miscIO::pulseBuzzer(int sec)
{
    gpio_set_value(67,HIGH);
    sleep(sec);
    gpio_set_value(67,LOW);

}
//Slot called to set timer6 (GPIO2_4)
void miscIO::setTimer6(bool val)
{
    gpio_set_value(115,HIGH);
    gpio_set_value(68,HIGH);
}

//Slot called to enable I2C line
void miscIO::i2cToggle(bool en)
{

}


