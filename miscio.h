#ifndef MISCIO_H
#define MISCIO_H

#include <QObject>
#include <QDebug>

class miscIO : public QObject
{
    Q_OBJECT

public:
    explicit miscIO(QObject *parent = 0);
    ~miscIO();

signals:

public slots:
    void i2cToggle(bool en);
    void pulseBuzzer(int sec);
    void setTimer6(bool val);

private:
    int serialPtrUartBatt;
};

#endif // MISCIO_H
