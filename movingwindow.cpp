
//Moving Window Class Definition
//Last Update: 7/31/2013 by Alex Greis

//Ported all moving window algorithms from Windows CE / VB 2008. Added extra functionality to enable easier integration with Qt,
//      specifically to enable running cpu-intensive analysis in a separate QThread to preserve GUI responsiveness.


#include <QFile>
#include <QDir>
#include <QtCore>
#include <math.h>

#include "movingwindow.h"


movingWindow::movingWindow()
{

    //Initialize global member vars
    MINCONTAMINATED = 10;
    NumDataCols = 8;
    NumWells = 4;
    PERMS = 35;



    controlsLoaded = false;

    //i= movingWindowAnalysis(QString("/home/root/Resources/ECISControls.cfg"),QString("/home/root/Resources/exp.txt"),QString("/home/root/data/expImpResults.txt"),8);


}

//Slot: rxAnalysisRequest
//Preconditions: cfgF is the path to the file containing a list of control files
//                      testF is the path to the file containing the raw experimental data
//                      resF is the path to the desired results file (will create this during analysis)
//Postconditions: emits signal txAnalysisComplete with result of analysis. Results file will be created and filled with triangle matrix.

void movingWindow::rxAnalysisRequest(QString cfgF, QTextStream *testF, QString resF, double n)
{
     int res =movingWindowAnalysis(cfgF,testF,resF,n);
     emit txAnalysisComplete(res);
}

//Function: movingWindowAnalysis
//Preconditions: ccfgFile is the path to the file containing a list of control files
//                      testFile is the path to the file containing the raw experimental data
//Postconditions: outputs 1 if the sample is safe, 0 if contaminated, based on statistical analysis
//                      -1 if config file was not found
//                      -2 if the config file contained no control files
int movingWindow::movingWindowAnalysis(QString cfgFile, QTextStream *testFile, QString resultFile, double CI)
{
    const int NUMWELLS = 8;
    int i;
    QString rxStr;

    //Parse the config file and retrieve the paths to the control files
    //Only do this once

    if (controlsLoaded == false)
    {

        QVector<QString> controlFiles;
        QFile file(cfgFile);

        //file check
        if (file.exists() == false)
            return -1;

        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream fileStream(&file);

        rxStr = fileStream.readLine();

        while (QString::compare("*START*",rxStr)!=0)
            rxStr = fileStream.readLine();

        //Once start sequence is found, store file names in controlFiles until end is found
        i=0;
        rxStr = fileStream.readLine();
         while (QString::compare("*END*",rxStr)!=0)
         {
             controlFiles.resize(i+1);
             controlFiles[i] = "/home/root/Resources/" + rxStr;
             rxStr = fileStream.readLine();
             i++;
         }

         //Ensure that at least one control file was loaded
         if(controlFiles.size() == 0)
             return -2;


         file.close();

         //Vector controlFiles now contains the paths to all of the control files
         //Its size is the number of control files listed in configFile

         //Go through controlFiles and open each file
         //Parse the file using LoadData and store each file in controls vector
        // QVector<QVector<QVector<double> > > controls;


         for(i=0;i<controlFiles.size();i++)
         {
             controls.resize(i+1);
             controls[i] = loadControlData(controlFiles[i],NUMWELLS);
         }

         controlsLoaded = true;
    }

     //controls vector now contains all of the normalized control data

     //Open and parse the experimental data with LoadData
     QVector<QVector<double> > experiment = loadData(testFile, NUMWELLS);

     //Run get all control distributions on the control array
     QVector<QVector<double> > allControlDistributions = getAllControlDistributions(controls, experiment.size());

     //Perform the triangles analysis; data returned is testStatistics and criticalValues in a vector
     QVector<QVector<QVector<double> > > analysis = triangles(experiment, allControlDistributions, CI);


     return isSampleSafe(displayResults(analysis, resultFile));

}

//Function: isSampleSafe
//Preconditions: arr is an integer matrix containing the triangular results containing only 0, 1, or -1
//                      0-> result is not significant for time window
//                      1-> result is significant for time window
//                      -1->place holder to make matrix triangular
bool movingWindow::isSampleSafe(QVector<QVector<int> > arr)
{
    uint counter =0;
    int col = arr[0].size();
    uint i;

    //Traverse each column until either:
    //counter exceeds MINCONTAMINATED, the minimum number of significant time windows to consider the entire sample contaminated.
    //          In this case, stop analysis, and report the sample is unsafe
    //the current column being examined is 0, in which case no column contained at least MINCONTAMINATED number of significant time windows.
    //          In this case, report the sample is safe

    while((counter < MINCONTAMINATED) && (col > 0))
    {
        //Initialize the counter
        counter = 0;

        //Traverse rows
        for(i=0; i< arr.size(); i++)
        {
            if(arr[i][col-1] == 1)
            {
                counter ++;     //If time window is significant (ie. 1) then increment counter
            }
            else                //Time window was not significant (ie. 0)
            {
                //Check if counter has reached MINCONTAMINATED; if not reset counter
                //Otherwise preserve its value
                if(counter < MINCONTAMINATED)
                    counter = 0;

            }
        }
        //Decrement the current column
        col = col -1;
    }

    if(counter >= MINCONTAMINATED)
    {
        return false;
    }
    else
    {
        return true;
    }


}

//Function: DisplayResults
//Preconditions: analysis is a <vector<vector<vector<double>>>; resultsFile is a string
//Postconditions: returns a matrix containing the triangle matrix analysis of the data, also writes the same triangular matrix to file
QVector<QVector<int> > movingWindow::displayResults(QVector<QVector<QVector<double> > > analysis, QString resultsFile)
{
    const int TESTSTATISTICS =0;
    const int CRITICALVALUES = 1;
    const int WIDTH = 3;

    QVector<QVector<int> > triangleMatrix;
    triangleMatrix.resize(1);
    QVector<int> row;
    row.resize(1);

    QFile saveFile(resultsFile);

    //If file exists
    if (saveFile.exists() == true)
    {
    }


    saveFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream saveStream(&saveFile);

    QString tmpStr = "";
    QString tmpIntStr = "";
    uint i,j,k,cntr,tmpBool;
    cntr = 0;

    //Create horizontal vector for the beginning times ie. [1 2 3 4 5 6 ...]

    for(i=1; i <= analysis[0].size();i++)
    {
        tmpIntStr = QString::number(i);
        while(tmpIntStr.size()<WIDTH)
        {
            tmpIntStr.append(" ");
        }
        tmpStr.append(tmpIntStr);
    }

    saveStream << tmpStr + "\r\n";
    tmpStr = "";

    //Rows
    for (i = 0; i< analysis[0].size();i++)
    {
        for (j = 0; j < analysis[0].size(); j++)
        {
            if (i == j)
            {
                //Create end times at the beginning of each line
                tmpIntStr = QString::number(i+1);
                while(tmpIntStr.size()<WIDTH)
                {
                    tmpIntStr.append(" ");
                }
                tmpStr.append(tmpIntStr);
            }
            else if(i>j)
            {
                //Pad with spaces
                row.resize(cntr + 1);
                row[cntr] = -1;
                cntr++;
                tmpStr.append("   ");
            }
            else
            {
                //Check if result is significant, if so output 1, else output 0
                row.resize(cntr+1);
                if(analysis[TESTSTATISTICS][i][j] > analysis[CRITICALVALUES][i][j])
                    tmpBool = 1;
                else
                    tmpBool = 0;

                row[cntr] = tmpBool;
                cntr++;
                tmpStr.append(QString::number(tmpBool) + "  ");
            }
        }

        triangleMatrix.resize(i+1);
        triangleMatrix[i] = row;
        cntr = 0;
        saveStream << tmpStr + "\r\n";
        tmpStr = "";
    }

    saveFile.close();

    return triangleMatrix;





}

//Function: triangles
//Preconditions: dataSet and allControlDistributions are vector<vector<double>> containing double values
//Postconditions: performs the bulk of the statistical analysis, and returns a vector of data
QVector<QVector<QVector<double> > > movingWindow::triangles(QVector<QVector<double> > dataSet, QVector<QVector<double> > allControlDistributions, double CI)
{
    //define constants used
    const int NUMWELLS = 4;     //Number of control and unknown wells
    const int CONTROLSTART = 0; //The starting index of the control well data
    const int CONTROLEND = 3;   //The ending index of the control well data
    const int UNKNOWNSTART = 4; //The starting index of the unknown well data
    const int UNKNOWNEND = 7;   //The ending index of the unknown well data

    //Resize the dataSet to contain the same number of rows as the control sets
    int i;
    dataSet.resize(allControlDistributions[0].size());

    //Initialize testStatistics and criticalValues to be square matrices of size [dataSet.size() x dataSet.size()], type double, and filled with zeros
    QVector<QVector<double> > testStatistics = zeros(dataSet.size(),dataSet.size());
    QVector<QVector<double> > criticalValues = zeros(dataSet.size(),dataSet.size());

    //Initialize distribution vector for which the critical value is determined
    QVector<double> distribution;

    //The ideal numbered values that we would like to have
    double ideal01;
    int ideal01High, ideal01Low;

    //Vectors to store the averaged wells
    int endTime, startTime;

    QVector<double> avgControlCols, avgUnknownCols, avgCMinusUCols;
    avgControlCols = sumMat(subMat(dataSet,0,dataSet.size()-1,CONTROLSTART,CONTROLEND));
    avgUnknownCols = sumMat(subMat(dataSet,0,dataSet.size()-1,UNKNOWNSTART,UNKNOWNEND));

    avgCMinusUCols.resize(dataSet.size());
    for(i=0; i<dataSet.size();i++)
    {
        avgCMinusUCols[i] = (avgControlCols[i] - avgUnknownCols[i])/NUMWELLS;
        if (avgCMinusUCols[i] < 0)
            avgCMinusUCols[i] = -1*avgCMinusUCols[i];
    }

    //Fill the triangular matrix testStatistics in O(N^2) time, as opposed to O(N^3)
    for (startTime = 0; startTime < dataSet.size()-1;startTime++)
    {
        //Fill the "almost diagonal" element (startTime, startTime+1)
        testStatistics[startTime][startTime+1] = avgCMinusUCols[startTime] + avgCMinusUCols[startTime+1];

        for (endTime=startTime+2; endTime<dataSet.size();endTime++)
        {
            //Remaining values on this row satisfy the following recursion relation
            testStatistics[startTime][endTime] = testStatistics[startTime][endTime-1] + avgCMinusUCols[endTime];
        }
    }

    distribution.resize(allControlDistributions.size());

    //Set the ideal numbers
    ideal01 = CI * distribution.size() +1;
    ideal01High = ceil(ideal01);
    ideal01Low = floor(ideal01);

    //Loop through all the rows in the dataSet
    for(endTime = 1; endTime < dataSet.size(); endTime++)
    {
        //Loop from 0 through the current iteration endTime
        for(startTime = 0; startTime < endTime; startTime++)
        {
            startTime = startTime;
            endTime = endTime;
            int tt = dataSet.size();
            tt = tt;

            //Calculate the 1% critical value for this time range (startTime - endTime)
            //Loop through the columns of allControlDistributions
            for(i=0; i<allControlDistributions.size(); i++)
            {
                distribution[i] = sumVecPartial(allControlDistributions[i],startTime,endTime);
            }

            //Sort the results in ascending order
            qSort(distribution);

            //Determine 1% Critical Value
            if (ideal01High >= distribution.size())
            {
                criticalValues[startTime][endTime] = distribution[distribution.size()-1];
                //If the ideal number is not an integer, perform a weighted average
            }
            else if (ideal01High != ideal01Low)
            {
                criticalValues[startTime][endTime] = ((ideal01High-ideal01)*distribution[ideal01Low-1]) + ((-1*(ideal01Low-ideal01)*distribution[ideal01High-1]));
                //The ideal number is indeed an integer
            }
            else
            {
                criticalValues[startTime][endTime] = (distribution[ideal01-1] + distribution[ideal01])/2.0;
            }
            tt = tt;
        }
    }

    QVector<QVector<QVector<double> > > results;
    results.resize(2);
    results[0] = testStatistics;
    results[1] = criticalValues;


    return results;



}

//Function: GetAllControlDistributions
//Preconditions: controls is a 3D vector containing double data. Must contain at least one entry
//Postcondition: vector (#data rows x 140 col) containing absolute values of the differences of the averages of control and unknown data
QVector<QVector<double> > movingWindow::getAllControlDistributions(QVector<QVector<QVector<double> > > controls, int maxTime)
{

    int i,j,k,l;
    const int constK = 4;
    const int NUMDATACOLS = 8;

    //Set up arrays of permutations to be used in statistical analysis
    //NchooseK with n = [1 2 3 4 5 6 7 8], k =4
    //result is a [70x4] matrix
    QVector<QVector<double> > allWells = NchooseK(genVec(1,NumDataCols,1),NumWells);

    //controlWells is composed of rows 0-34 of allWells (35 rows)
    QVector<QVector<double> > controlWells;

    for (i=0; i< PERMS; i++)
    {
        controlWells.resize(i+1);
        controlWells[i] = allWells[i];
    }

    //unknownWells is composed of rows 69-35 of allWells
    QVector<QVector<double> > unknownWells;

    for (i=allWells.size(); i > PERMS; i--)
    {
        unknownWells.resize(allWells.size()-i+1);
        unknownWells[allWells.size()-i] = allWells[i-1];
    }

    //Set the maximum time length
    //Loop through the controls vector and check the number of rows in each vector
    //maxTime is to be set to the number of rows in the vector with the fewest number of rows
    for (i = 0; i< controls.size();i++)
    {
        if (maxTime  > controls[i].size())
            maxTime = controls[i].size();
    }


    //Now maxTime == the number of rows of the vector with the fewest rows
    //Go through controls again and trim each vector tocontain maxTime rows
    //Then loop through all well permutations and compute the average control and unknown vectors
    //Append the absolute values of the point-wise differences to the vector allControlDistributions

    QVector<QVector<double> > allControlDistributions;
    QVector<double> absDifference;
    absDifference.resize(1);
    double controlSum, unknownSum;
    allControlDistributions.resize(1);

    //Loop through the control vector
    for(i=0;i<controls.size();i++)
    {

        controls[i].resize(maxTime);
        //Loop through permutations
        for(j=0; j < PERMS; j++)
        {
            //Loop through vector rows
            for (k = 0; k< controls[i].size();k++)
            {
                controlSum = 0;
                unknownSum = 0;

                //Loop through permutations column
                for (l=0;l<constK;l++)
                {
                    //Add data point to sums
                    controlSum += controls[i][k][controlWells[j][l]-1];
                    unknownSum += controls[i][k][unknownWells[j][l]-1];
                }

                //Average the sums, take the difference, take the absolute value
                absDifference.resize(k+1);
                absDifference[k] = (controlSum-unknownSum)/constK;

                if (absDifference[k] < 0)
                    absDifference[k] = -1*absDifference[k];
            }
            //Add difference vector to allControlDistributions
            allControlDistributions.resize(i*PERMS+j+1);
            allControlDistributions[i*PERMS+j] = absDifference;

        }
    }

    return allControlDistributions;


}
//Function: ConvertRawData
//Preconditions: arr is a matrix of type double, and contains raw impedance data collected by data acquisition program
//Postconditions: returns a matrix of type double containing normalized impedance data
//                      normalized data is computed by dividing each data value by the value in the first row
QVector<QVector<double> > movingWindow::convertRawData(QVector<QVector<double> > arr)
{
    QVector<QVector<double> > normalizedData;
    int i,j;

    normalizedData.resize(arr.size());

    for(i=0;i<arr.size();i++)
    {
        normalizedData[i].resize(arr[i].size());
        for(j=0;j<arr[i].size();j++)
            normalizedData[i][j] = arr[i][j]/arr[0][j];
    }

    return normalizedData;
}

//Function: LoadData
//Preconditions: filename is a path to a tab-delimited text file with data of type double arranged
//                      in rows and cols
//                      numcols is the number of data columns in the file (should  always be 8)
//Postconditions: file is opened and parsed, data is returned in a matrix of doubles
QVector<QVector<double> > movingWindow::loadData(QTextStream *filename, int numCols)
{
    QVector<QVector<double> > data;
    QString rxStr;
    QStringList lineArr;
    int i,j;

    //file check
    //if (filename. == false)
    filename->seek(0);   //set cursor to beginning of file

    i = 0;

    //Loop through file, adding data lines to array
    while(filename->atEnd()==false)
    {
        data.resize(i+1);
        data[i].resize(numCols);
        rxStr = filename->readLine();
        lineArr = rxStr.split(QRegExp("\t"));

        for(j=0;j<lineArr.size();j++)
            data[i][j] = lineArr[j].toDouble();

        i++;

    }


    return convertRawData(data);


}

//Function: loadControlData
//Preconditions: filename is a path to a tab-delimited text file with data of type double arranged
//                      in rows and cols
//                      numcols is the number of data columns in the file (should  always be 8)
//Postconditions: file is opened and parsed, data is returned in a matrix of doubles
QVector<QVector<double> >movingWindow::loadControlData(QString filname, int numCols)
{
    QVector<QVector<double> > data;
    QString rxStr;
    QStringList lineArr;
    QFile file(filname);
    int i,j;

    //file check
    if (file.exists() == false)
        return data;

    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream filestream(&file);

    i = 0;

    //Loop through file, adding data lines to array
    while(filestream.atEnd()==false)
    {
        data.resize(i+1);
        data[i].resize(numCols);
        rxStr = filestream.readLine();
        lineArr = rxStr.split(QRegExp("\t"));

        for(j=0;j<lineArr.size();j++)
            data[i][j] = lineArr[j].toDouble();

        i++;

    }

    file.close();


    return convertRawData(data);

}

//function: nChooseK
//Preconditions: n is a vector of int; k is an int
//                  1 < k < n.size()
//Postconditions: matrix returned with n choose k permutations
QVector<QVector<double> > movingWindow::NchooseK(QVector<int> n, int k)
{

    QVector<QVector<double> > invP;
    QVector<QVector<double> > P;
    QVector<double> row;
    int h,i,j;

    row.resize(1);
    invP.resize(1);
    P.resize(1);

    QVector<int> mask;
    mask.resize(n.size());

    for (i=0;i<= n.size()-k-1;i++)
        mask[i] = 0;

    for (i = n.size()-k; i < n.size(); i++)
        mask[i] = 1;

    h = 0;

    do{
        j = 0;

        for(i=0;i<mask.size();i++)
        {
            if (mask[i])
            {
                row.resize(j+1);
                row[j] = n[i];
                j+=1;
            }
        }
        invP.resize(h+1);
        invP[h] = row;
        h+=1;
    }while(nextPermutation(mask,mask.size()-1));


    for(i=0;i<invP.size();i++)
    {
         P.resize(i+1);
         P[i] = invP[invP.size()-1-i];
    }

    return P;
}

//Function: subMat
//Preconditions: mat is a matrix of any type
//                      0 <= rowStart < rowEnd < mat.size()
//                  0 <= colStart < colEnd < mat[i].size()
//Postconditions: returns a matrix of rows from rowStart to rowEnd, and cols from colStart to colEnd
 QVector<QVector<double> > movingWindow::subMat(QVector<QVector<double> > mat, int rowStart, int rowEnd, int colStart, int colEnd)
 {
    QVector<QVector<double> > tempMat;
    int i;

    for (i=rowStart; i<= rowEnd; i++)
    {
        tempMat.resize(i-rowStart+1);
        tempMat[i-rowStart] = subVec(mat[i],colStart,colEnd);
    }

    return tempMat;
 }

//Function: SumVecPartial
//Preconditions: vec is a vector of any type for which the operator += has been overloaded
//Postconditions: the sum of all the elements in the vector is returned
double movingWindow::sumVecPartial(QVector<double> vec, int first, int last)
{
    double sum = 0;
    int i;

    for (i = first; i <=last;i++)
        sum += vec[i];

    return sum;
}

//Function: SubVec
//Preconditions: vec is a vector of any type; start and end are ints
//                      0 <= start < vec.size - 1
//                      start < end < vec.size
//Postconditions: a vector from index start to end of the original vector is returned
QVector<double> movingWindow::subVec(QVector<double> vec, int startIndex, int endIndex)
{
    QVector<double> tempVec;
    int i;

    tempVec.resize(endIndex-startIndex+1);

    for(i = startIndex;i <= endIndex; i++)
        tempVec[i-startIndex] = vec[i];


    return tempVec;
}

//Function: zeros
//Preconditions: rows and cols are ints representing the dimensions of the generated matrix
//Postconditions: matrix of type "type", size [rows x cols], and filled with zeros (0) generated
QVector<QVector<double> > movingWindow::zeros(int rows, int cols)
{
    QVector<QVector<double> > mat(rows);
    int i,j;

    for(i=0;i<rows;i++)
    {
        mat[i].resize(cols);
        for(j=0;j<cols;j++)
            mat[i][j] = 0;
    }

    return mat;

}

//Function: GenVec
//Preconditions: start is int start value
//                  end is int end value; end > start
//                  inc is int increment value; inc > 0
//Postconditions: generates an int vector with values from start to end, increasing by inc
QVector<int> movingWindow::genVec(int startValue, int endValue, int increment)
{
    QVector<int> vec;
    int i = startValue;

    while(i <= endValue)
    {
        vec.resize(i-startValue + 1);
        vec[i-startValue] = i;
        i = i + increment;
    }

    return vec;
}

//Function: sumVec
//Preconditions: vec is a vector of any type for which  the operator += has been overloaded
//Postconditions: the sum of all the elements in the vector is returned
double movingWindow::sumVec(QVector<double> arr)
{
    double sum = 0;
    int i;

    for(i=0; i < arr.size();i++)
    {
        sum += arr[i];
    }

    return sum;
}

//Function: sumMat
//Preconditions: mat is a matrix of any type for which the += operator has been overloaded
//Postconditions: returns a vector containing the sums of the colums of the matrix
QVector<double> movingWindow::sumMat(QVector<QVector<double> > mat)
{
   QVector<double> sum;
   int i;

   sum.resize(mat.size());

   for (i=0; i < mat.size(); i++)
       sum[i] = sumVec(mat[i]);

   return sum;


}

bool movingWindow::nextPermutation(QVector<int> &values, int size)
{
    int i,j,k;

    if (size == 1)
        return false;

    i = size;

    while(true)
    {
        k = i;
        i = i -1;

        if(values[i] < values[k])
        {
            j = size;

            while (!(values[i] < values[j]))
                j = j -1;

            swap(values,i,j);
            reverse(values,k,size);
            return true;
        }

        if (i == 0)
        {
            reverse(values, 1, size);
            return false;
        }
    }

}

void movingWindow::swap(QVector<int> &values, int i, int j)
{
    int temp = values[i];
    values[i] = values[j];
    values[j] = temp;
}

void movingWindow::reverse(QVector<int> &values, int first, int last)
{
    int i = first;
    int j = last;

    while (i <= j)
    {
        swap(values, i,j);
        i = i + 1;
        j = j - 1;
    }
}
