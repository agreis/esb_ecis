//Header file for moving window analysis class
//Ported from VB version in previous projects (7/23/2013)


#ifndef MOVINGWINDOW_H
#define MOVINGWINDOW_H

#include <QVector>
#include <QObject>
#include <QTextStream>

class movingWindow : public QObject
{
    Q_OBJECT

public:
    movingWindow();
    bool isSampleSafe(QVector<QVector<int> > arr);
    QVector<QVector<double> > convertRawData(QVector<QVector<double> > arr);
    QVector<QVector<double > > loadData(QTextStream *filname, int numCols);
    QVector<QVector<double > > loadControlData(QString filname, int numCols);
    QVector<int> genVec(int startValue, int endValue, int increment);
    QVector<QVector<double> > zeros(int rows, int cols);
    double sumVec(QVector<double> arr);
    double sumVecPartial(QVector<double> vec, int first, int last);
    QVector<double> subVec(QVector<double> vec, int startIndex, int endIndex);
    QVector<double> sumMat(QVector<QVector<double> > mat);
    QVector<QVector<double> > subMat(QVector<QVector<double> > mat, int rowStart, int rowEnd, int colStart, int colEnd);
    QVector<QVector<double> > NchooseK(QVector<int> n, int k);
    bool nextPermutation(QVector<int> &values, int size);
    void swap(QVector<int> &values, int i, int j);
    void reverse(QVector<int> &values, int first, int last);
    QVector<QVector<double> > getAllControlDistributions(QVector<QVector<QVector<double> > > controls, int maxTime);
    QVector<QVector<QVector<double> > > triangles(QVector<QVector<double> > dataSet, QVector<QVector<double> > allControlDistributions, double CI);
    QVector<QVector<int> > displayResults(QVector<QVector<QVector<double> > > analysis, QString resultsFile);
    int movingWindowAnalysis(QString cfgFile, QTextStream *testFile, QString resultFile, double CI);

signals:

    void txAnalysisComplete(bool isSafe);

public slots:

    void rxAnalysisRequest(QString cfgF, QTextStream *testF, QString resF, double n);

public:
    bool controlsLoaded;

private:
    int MINCONTAMINATED;
    int NumDataCols;
    int NumWells;
    int PERMS;


    QVector<QVector<QVector<double> > > controls;


};

#endif // MOVINGWINDOW_H
