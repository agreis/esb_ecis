#include "powercontrol.h"
#include "dlgmessagebox.h"


powerControl::powerControl(QObject *parent)
{
    initBatteryIO();
}


void powerControl::powerDown()
{
    char txBuff[6];

    //Build command buffer
    txBuff[0] = 'U';
    txBuff[1] = '$';
    txBuff[2] = 'P';
    txBuff[3] = 'W';
    txBuff[4] = 'D';
    txBuff[5] = '!';

    //Write data to UART
    write(serialPtrUartBatt,txBuff,6);
    sleep(2);
}

//Read battery / ACK
void powerControl::requestBattery()
{

    //Read battery, check response
    QString response = readBattery();

    //Ensure that ACK was found
    if (response.indexOf("Ok.") == -1)
    {
        emit txBatteryCharge(ERR_BAT_BADACK);
        return;
    }

    QStringList lst = response.split("Ok.");
    QString battCharge = lst.at(0);

    //If error, try again
    if(QString::compare(battCharge,"??")==0)
    {
        response = readBattery();

        //Ensure that ACK was found
        if (response.indexOf("Ok.") == -1)
        {
            emit txBatteryCharge(ERR_BAT_BADACK);
            return;
        }

        lst = response.split("Ok.");
        battCharge = lst.at(0);

        if(QString::compare(battCharge,"??")!=0)
            emit txBatteryCharge(battCharge.toInt());
        else
            emit txBatteryCharge(ERR_BAT_INVALID);

    }
    else
        emit txBatteryCharge(battCharge.toInt());

}

void powerControl::initBatteryIO()
{
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //Configure UART 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    int err;
    struct termios uart1_attributes;

    //Attempt to Open UART1
    if((serialPtrUartBatt = open("/dev/ttyO1", O_RDWR | O_NONBLOCK)) < 0)
    {
        //Error opening UART
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("Battery Error");
        msgBox->setInfo("Error opening UART connection to the battery circuit");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }

    //Set Port attributes
    tcgetattr(serialPtrUartBatt, &uart1_attributes);   //Load current attributes into uart1_attributes object
    uart1_attributes.c_iflag = 0;
    uart1_attributes.c_oflag = 0;
    uart1_attributes.c_lflag = 0;
    uart1_attributes.c_cc[VMIN] = 0;    //Minimum number of bytes to return from read
    uart1_attributes.c_cc[VTIME] = 100; //Read Timeout
    cfsetospeed(&uart1_attributes, B57600);     //Baudrates
    cfsetispeed(&uart1_attributes, B57600);     //Baudrates

    int retVal = tcsetattr(serialPtrUartBatt, TCSANOW, &uart1_attributes);

    //Set new attributes
    if (retVal != 0)
    {
        //If setup returned error
        qDebug() << "BATTERY ERROR";
        int errsv = errno;
        const char *str = strerror(errsv);
        //mssgbox.setText(QString::fromLocal8Bit(str));

        serialPtrUartBatt = -1;

        //Use set to apply timeout
        FD_ZERO(&set);
        FD_SET(serialPtrUartBatt,&set);


    }
}

//Helper function, perform I/O to read battery
QString powerControl::readBattery()
{
    char txBuff[6];

    //Build command buffer
    txBuff[0] = 'U';
    txBuff[1] = '$';
    txBuff[2] = 'A';
    txBuff[3] = 'C';
    txBuff[4] = 'K';
    txBuff[5] = '!';


    //Write data to UART
    write(serialPtrUartBatt,txBuff,6);
    sleep(1);

    //Read response
    QString rxMssg = "";
    char c;

    QTime timer;
    timer.start();

    while ((rxMssg.length() < 6) && (timer.elapsed() < BATT_COM_TIMEOUT))
        if(read(serialPtrUartBatt,&c,1) > 0)
            rxMssg.append(c);



    if(rxMssg.length() <= 0)
    {
        //Try one more time
        write(serialPtrUartBatt,txBuff,6);
        sleep(1);

        //Read response
        rxMssg = "";

        QTime timer2;
        timer2.start();

        while ((rxMssg.length() < 5) && (timer.elapsed() < BATT_COM_TIMEOUT))
            if(read(serialPtrUartBatt,&c,1) > 0)
                rxMssg.append(c);

        if (rxMssg.length() <=0)
            emit  txBatteryCharge(ERR_BAT_TIMEOUT);
        else
            return rxMssg;

    }
    else
    {
        return rxMssg;
    }
}
