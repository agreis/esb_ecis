#ifndef POWERCONTROL_H
#define POWERCONTROL_H

#include <QObject>
#include <QDebug>
#include <QTime>

//UART
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/types.h>
#include <fcntl.h>

//Battery read errors
#define ERR_BAT_INVALID -1
#define ERR_BAT_TIMEOUT -2
#define ERR_BAT_BADACK -3

#define BATT_COM_TIMEOUT 50 //ms

class powerControl: public QObject
{
    Q_OBJECT

public:
    explicit powerControl(QObject *parent = 0);

public slots:
    void powerDown();
    void requestBattery();

signals:
    void txBatteryCharge(int percentFull);

private:
    void initBatteryIO();
    QString readBattery();
    int serialPtrUartBatt;
    fd_set set;
    struct timeval timeout;
};

#endif // POWERCONTROL_H
