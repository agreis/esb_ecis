#include "teccontrol.h"
#include "Simple_GPIO.h"
#include "dlgmessagebox.h"
#include <QDebug>
#include <QThread>

//Serial Includes
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

tecControl::tecControl()
{
    int retVal, err,i;
    struct termios uart2_attributes;

    //Initialize global vars
    serialPtrTec = -1;

    currSetpoint = -1;
    tecEnabled = false;
    connected = false;
    initialized = false;

    for (i=0;i<5;i++)
        measTrack[i] = -1;

    measTrackCount = 0;

    //Set up UART 2 (TEC) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //Attempt to Open UART2
    if((serialPtrTec = open("/dev/ttyO2", O_RDWR | O_NONBLOCK)) < 0)
    {
        //Error opening UART
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("Heater Subsystem Error");
        msgBox->setInfo("Error opening UART connection to the TEC circuit");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
    }
    //Set Port attributes
    tcgetattr(serialPtrTec, &uart2_attributes);   //Load current attributes into uart2_attributes object
    uart2_attributes.c_iflag = 0;
    uart2_attributes.c_oflag = 0;
    uart2_attributes.c_lflag = 0;
    uart2_attributes.c_cc[VMIN] = 0;    //Minimum number of bytes to return from read
    uart2_attributes.c_cc[VTIME] = TEC_READ_TIMEOUT; //Read Timeout
    cfsetospeed(&uart2_attributes, B9600);  //B57600);     //Baudrates
    cfsetispeed(&uart2_attributes, B9600);  //B57600);     //Baudrates

    retVal = tcsetattr(serialPtrTec, TCSANOW, &uart2_attributes);

    //Set new attributes
    if (retVal != 0)
    {
        //If setup returned error
        int errsv = errno;
        const char *str = strerror(errsv);
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("Heater Subsystem Error");
        msgBox->setInfo("Error configuring the TEC circuit");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();



        serialPtrTec = -1;

    }


}

//Slot: Performs Ack for the detection of hardware
// ACK is the same as temperature measure. Response is:
// [Byte 0] = Temp MSB
// [Byte 1] = Temp LSB
// [Byte 2] = 1
// [Byte 3] = \0

void tecControl::tecAckStart()
{

    //If ECIS subsystem is not initialized
    if (serialPtrTec == -1)
    {
        //DEBUG~~~~~~
        dlgMessageBox *msgBox= new dlgMessageBox();
        msgBox->setTitle("TEC ERR");
        msgBox->setInfo("ERR #1");
        msgBox->setOKButton(true,"Ok");
        msgBox->setCancelButton(false,"Cancel");
        msgBox->exec();
        //~~~~~~~~~~

        connected = false;
        emit tecAckComplete(false);
        initialized = true;
        return;
    }
    else //Else, perform temperature measurement
    {
        unsigned char txBuff[6], rxBuff[3], tempLSB, tempMSB;
        unsigned int temperature;
        QString rxStr;

        //Flush the serial port buffers
        tcflush(serialPtrTec,TCIOFLUSH);

        //Build command buffer
        txBuff[0] = 0x24;
        txBuff[1] = 0x20;
        txBuff[2] = 0x00;
        txBuff[3] = 0x00;
        txBuff[4] = 0x00;
        txBuff[5] = 0xFF;

        //write data to UART - discard first try
        write(serialPtrTec, txBuff, 6);
        sleep(1);
        read(serialPtrTec,rxBuff,3);

        int tries = 0;
        connected = false;
        double tempFinal = 0;

        //Allow several tries for the connection
        while ((connected == false) && (tries < NUM_CONNECT_ATTEMPTS))
        {
            //Flush the serial port buffers
            tcflush(serialPtrTec,TCIOFLUSH);

            //write data to UART
            write(serialPtrTec, txBuff, 6);
            sleep(1);

            //read and extract temperature data
            //bytesRead = read(serialPtrTec,rxBuff,3);
            int bytesRead = readData(rxBuff,3);

            //Failed to read data (either no returned data or invalid status byte)
            if ((bytesRead == 0) || (! validStatus(rxBuff[2])))
            {
                tries = tries+1;
            }
            else
            {
                tempMSB = rxBuff[0];
                tempLSB = rxBuff[1];

                //Parse temperature data
                temperature = 0;
                temperature = (temperature << 8) + tempMSB;
                temperature = (temperature << 8) + tempLSB;

                tempFinal = (double)temperature / 100;


                connected = true;
                initialized = true;
            }

        }

        if (!connected)
        {
            connected = false;
            emit tecAckComplete(false);
            initialized = true;
            return;
        }
        else
        {
            emit tecAckComplete(true);
            emit tecTempMeasureComplete(tempFinal);
            qDebug() << "TEC ACK";
            return;
        }

    }

}

//Slot: Performs temperature measurement
void tecControl::tecTempMeasureStart()
{
    //If TEC subsystem is not initialized
    if (!connected)
    {
        return;
    }
    else //Else, perform temperature measurement
    {

        unsigned char txBuff[6], rxBuff[3], tempLSB, tempMSB;
        unsigned int temperature;

        //Flush the serial port buffers
        tcflush(serialPtrTec,TCIOFLUSH);

        //Build command buffer
        txBuff[0] = 0x24;
        txBuff[1] = 0x20;
        txBuff[2] = 0x00;
        txBuff[3] = 0x00;
        txBuff[4] = 0x00;
        txBuff[5] = 0xFF;

        //write data to UART
        write(serialPtrTec, txBuff, 6);
        sleep(1);

        //read and extract temperature data
        int bytesRead = readData(rxBuff,3); //read(serialPtrTec,rxBuff,3);

        //If no bytes were read, try again
        if (bytesRead == 0)
        {
            //Flush the serial port buffers
            tcflush(serialPtrTec,TCIOFLUSH);

            //write data to UART
            write(serialPtrTec, txBuff, 6);
            sleep(1);

            //read and extract temperature data
            bytesRead = readData(rxBuff,3); //read(serialPtrTec,rxBuff,3);
        }

        if (bytesRead == 0)
        {
            return;
        }

        tempMSB = rxBuff[0];
        tempLSB = rxBuff[1];

        //Data was read

        //Check to see if status byte is valid
        if( ! validStatus(rxBuff[2]))
            return;

        //Parse temperature data
        temperature = 0;
        temperature = (temperature << 8) + tempMSB;
        temperature = (temperature << 8) + tempLSB;

        double tempFinal = (double)temperature /100;


        emit tecTempMeasureComplete(tempFinal);

    }
}

//keeps track of the last five measurements and determines if thermal runaway conditions are found
//Note that under typical operation the ESB system performs measurements using a 1 minute cycle
//This has been updated since the measurement cycle is now  5 seconds

//Curent algorithm requires error to be above threshold with diverging slope
void tecControl::runawayCheck(double measurement)
{
    int i;
    bool fiveMinutesAcq = true;

    //return if count has not reached 12 (1 minute)
    if (((float)measTrackCount)/12 < 1.0)
    {
        measTrackCount = measTrackCount+1;
        return;
    }

    measTrackCount = 0;



    //shift measurements
    measTrack[4] = measTrack[3];
    measTrack[3] = measTrack[2];
    measTrack[2] = measTrack[1];
    measTrack[1] = measTrack[0];
    measTrack[0] = measurement;

    //check to ensure measurement array is full
    for (i=0;i<5;i++)
    {
        if (measTrack[i] == -1)
            fiveMinutesAcq = false;
    }

    //If the TEC is enabled and enough measurements have been taken
    if ((tecEnabled) && (fiveMinutesAcq))
    {  
        //calculate slope
        double slope = measTrack[0] - measTrack[4];

        //calculate error
        double error = measTrack[0] -currSetpoint;
    }


}

//Check to see if status byte is valid
//byte 0 must be 1, byte 6 must be 0
bool tecControl::validStatus(unsigned char statusByte)
{
    if (!(statusByte & 0x01))
        return false;
    if (statusByte & 0x20)
        return false;

    return true;
}


//Read data from port with length len
//returns the number of bytes read, or -1 for error
int tecControl::readData(unsigned char *buff, int len)
{
    char c;
    int i = 0;

    QTime timer;
    timer.start();

    while ((i < len) && (timer.elapsed() < TEC_READ_TIMEOUT))
    {
        if(read(serialPtrTec,&c,1) > 0)
        {
            buff[i] = c;
            i++;
        }
    }

    return i;

}

//Slot: Enable TEC module
void tecControl::tecEnableStart()
{
    //If ECIS subsystem is not initialized
    if (serialPtrTec == -1)
    {

    }
    else //Else, perform serial communication
    {
        qDebug() << "TEC EN START";
        unsigned char txBuff[6], rxBuff[3];

        //Flush the serial port buffers
        tcflush(serialPtrTec,TCIOFLUSH);

        //Build command buffer
        txBuff[0] = 0x24;
        txBuff[1] = 0x10;
        txBuff[2] = 0x01;
        txBuff[3] = 0x00;
        txBuff[4] = 0x00;
        txBuff[5] = 0xFF;

        //write data to UART
        write(serialPtrTec, txBuff, 6);
        sleep(1);

        readData(rxBuff,3);

        tecEnabled = true;


    }
}

//Slot: Disable TEC module
void tecControl::tecDisableStart()
{
    //If ECIS subsystem is not initialized
    if (serialPtrTec == -1)
    {

    }
    else //Else, perform serial communication
    {
        qDebug() << "TEC DIS START";
        unsigned char txBuff[6], rxBuff[3];
        QString rxStr;

        //Flush the serial port buffers
        tcflush(serialPtrTec,TCIOFLUSH);

        //Build command buffer
        txBuff[0] = 0x24;
        txBuff[1] = 0x10;
        txBuff[2] = 0x02;
        txBuff[3] = 0x00;
        txBuff[4] = 0x00;
        txBuff[5] = 0xFF;

        //write data to UART
        write(serialPtrTec, txBuff, 6);
        sleep(1);

        readData(rxBuff,3);

        tecEnabled =false;

    }
}

//Slot: Set TEC setpoint to specified value (in degrees C, roundoff to nearest .01)
void tecControl::tecSetpointSet(double setpoint)
{
    //If ECIS subsystem is not initialized
    if (serialPtrTec == -1)
    {

    }
    else
    {
        qDebug() << "TEC SETPOINT SET";

        int temp, i;
        unsigned char txBuff[6], rxBuff[3], tempLSB, tempMSB;
        QString rxStr;

        //Format temperature for transmit
        temp = (int)(setpoint*100);
        tempLSB = temp & 0xFF;
        temp = temp >> 8;
        tempMSB = temp & 0xFF;

        //Flush the serial port buffers
        tcflush(serialPtrTec,TCIOFLUSH);


        //Build command buffer
        txBuff[0] = 0x24;
        txBuff[1] = 0x10;
        txBuff[2] = 0x00;
        txBuff[3] = tempMSB;
        txBuff[4] = tempLSB;
        txBuff[5] = 0xFF;

        //write data to UART
        write(serialPtrTec, txBuff, 6);
        sleep(1);

        readData(rxBuff,3); //

        //Set up vars for thermal runaway checks
        currSetpoint = setpoint;

        for (i=0;i<5;i++)
            measTrack[i] = -1;


    }
}
