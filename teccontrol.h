#ifndef TECCONTROL_H
#define TECCONTROL_H

#include <QObject>
#include <QDebug>
#include <QTime>

#define TEC_READ_TIMEOUT 10 //units of 0.1 seconds
#define NUM_CONNECT_ATTEMPTS 4

/*
 *  Notes about TEC interface:
 *
 *  All commands follow the fomat "$" + SER_COMM1 + SER_COMM0 + DATA1 + DATA0 + 0xFF
 *
 *  COMM1       COMM0       Description
 *  -----------------------------------
 *  10h         00h         Set setpoint
 *  10h         01h         Enable Output
 *  10h         02h         Disable Output
 *  10h         04h         Clear overcurrent condition
 *  20h         XXh         Read temperature (default)
 *  30h         NNh         Set PI parameters
 *  40h         XXh         get TEC #
 *  80h         NNh         Read PI parameters
 *
 *  Return packet is always the same:
 *
 *  TEMPERATURE_MSB + TEMPERATURE_LSB + CURRENT STATUS
 *
 *  Status Byte:
 *  0   -   "Ok" flag
 *  1   -   Hi-temp state
 *  2   -   disabled state
 *  3   -   Not used
 *  4   -   Not used
 *  5   -   Not used
 *  6   -   always "0"
 *  7   -   Overcurrent state
 *
 *
 */

class tecControl : public QObject
{

    Q_OBJECT

public:
    tecControl();
    bool isConnected() { return connected; }
    bool isInitialized() { return initialized; }
signals:

    void tecTempMeasureComplete(double temp);
    void tecTempSetpointComplete(double setpt);
    void tecAckComplete(bool ack);

public slots:

    void tecAckStart();
    void tecTempMeasureStart();
    void tecEnableStart();
    void tecDisableStart();
    void tecSetpointSet(double setpoint);

private:

    bool connected;
    bool initialized;
    int serialPtrTec;
    double measTrack[5];
    int measTrackCount;
    double currSetpoint;
    bool tecEnabled;
    void runawayCheck(double measurement);
    bool validStatus(unsigned char statusByte);
    int readData(unsigned char *buff, int len);


};

#endif // TECCONTROL_H
