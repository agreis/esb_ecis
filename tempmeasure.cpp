#include "tempmeasure.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;

tempMeasure::tempMeasure()
{
}

int tempMeasure::readHeatsinkTemp()
{
    int file;
    char *filename = "/dev/i2c-3";


    if ((file = open(filename,O_RDWR)) < 0)
    {
        close(file);
        return -1;
    }

    int addr =0x68;  //I2C address of the device

    if (ioctl(file, I2C_SLAVE,addr) < 0)
    {
        close(file);
        return -1;
    }


    char buf[8] = {0,0,0,0,0,0,0,0};
   // buf[0] = 0xFF;

    int numBytes = write(file,buf,1);
    if (numBytes != 1)
    {
        close(file);
        return -1;
    }


    numBytes = read(file,buf,8);
    if (numBytes != 8)
    {
        close(file);
        return -1;

    }

    close(file);

    //Parse data
    char msb, lsb;
}
