//Class Definition for I2C temperature measurement system

#ifndef TEMPMEASURE_H
#define TEMPMEASURE_H

class tempMeasure
{
private:


public:
    tempMeasure();
    int readHeatsinkTemp();

};

#endif // TEMPMEASURE_H
