#include "testdata.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QDateTime>
#include <QFile>
#include <QDir>


using namespace std;

testData::testData()
{

    currentState = INIT;
    insMarker = false;

    autoScale = true;
    normalized = false;
    enNormalized = false;
    yMin = 0;
    yMax = 4000;
    minReading = 0;
    maxReading = 0;
    minNormReading = 1;
    maxNormReading = 1;
    controlCfgPath = "/home/root/Resources/";
    testRecordPath = "/home/root/Resources/records.txt";
    testName = "";
    instSerial = "";
    chipDisconnect = false;
    warningDisplayed= false;
    maxImpRun = 4000;
    minImpRun = 300;
    analysisRunning = false;
    controlName ="";
    shutoffTime = 60;
    enableAutoShutoff = 1;
    rcTol = 10;

    version = SOFTWARE_VER;
    date = SOFTWARE_DATE;
    updated = false;


    //Debug vars
    DEBUG_sbcOnly = false;

}

bool testData::initSaveFiles(QString name)
{
    //Check for directories and create if not present
    if(QDir("/home/root/data/").exists() == false)
        QDir().mkdir("/home/root/data/");

    if(QDir("/home/root/raw/").exists() == false)
        QDir().mkdir("/home/root/raw/");

    savePath = "/home/root/data/" + name + ".txt";
    resultsPath = "/home/root/data/" + name + "_ImpResults.txt";
    posResultsPath = "/home/root/data/" + name + "_PosCtrlImpResults.txt";
    rawPath = "/home/root/raw/" + name + ".txt";
    testName = name;

    //If file exists, return false (no overwrite)
    QDir::setCurrent("/home/root/raw");
    rawFile.setFileName(name + ".txt");

    if (rawFile.exists())
        return false;

    rawFile.open(QIODevice::ReadWrite | QIODevice::Text);   //Need R/W for moving window


    QDir::setCurrent("/home/root/data");
    saveFile.setFileName(name + ".txt");

    if (saveFile.exists())
        return false;

    saveFile.open(QIODevice::WriteOnly | QIODevice::Text);

    //Get Date and Time
    //Read date and time for default file name


    clockCtrl = new clockcontroliic();
    clockCtrl->readDateTime();
    struct dt_clock dt = clockCtrl->dateTime;

    QString timeStr =  QString::number(dt.date)+"-"+QString::number(dt.month)+
            "-"+QString::number(dt.year) + " "+QString::number(dt.hours)+
            ":"+QString::number(dt.minutes);

    QTime testDur;




    int hours = (int)testDuration/60;
    int mins = testDuration%60;

    testDur.setHMS(hours,mins,0);
    QString durStr = testDur.toString("hh:mm:ss");

    //Convert instrument serial number /software version to a string
    QString serialStr = instSerial;
    QString softwareStr = SOFTWARE_VER;


    saveStream.setDevice(&saveFile);
    rawStream.setDevice(&rawFile);
    //QTextStream rawStream(&rawFile);




    //Create header for save file
    saveStream << "ESB - ECIS Test\r\n\r\n";
    saveStream << "Test Start: " + timeStr + "\r\n";
    saveStream << "Instrument Serial: " + serialStr + "\r\n";
    saveStream << "Software Version: " + softwareStr + "\r\n";
    saveStream << "Test Duration (hh:mm:ss): " + durStr + "\r\n\r\n\r\n";
    saveStream << "\t\tImpedance\n";
    saveStream << "\t\tControl Port\t\t\t\tSample Port\r\n";
    saveStream << "\tTime(mins)\tCE #1\tCE #2\tCE #3\tCE #4\tSE #1\tSE #2\tSE #3\tSE #4\tTemp (C)\r\n";


    sampNum= 0;

    return true;
}

bool testData::closeSaveFiles()
{
    rawFile.close();
    saveFile.close();

}

//Call this to perform the initial normalization point insertion Adds a line to the raw file without affecting the timing
bool testData::insertNormPoint()
{
    //Insert line into raw file


    QString tempStr = QString::number(recentDataArr[0])+ "\t" + QString::number(recentDataArr[1])+ "\t" + QString::number(recentDataArr[2])+ "\t" + QString::number(recentDataArr[3])+ "\t" + QString::number(recentDataArr[4])+ "\t" + QString::number(recentDataArr[5])+ "\t" + QString::number(recentDataArr[6])+ "\t" + QString::number(recentDataArr[7]) + "\n";

    rawStream << tempStr;


    return true;

}

//Call this function to ensure all relevant vars are cleared if user returns to application starting state
void testData::reInit()
{

    closeSaveFiles();

    savePath = "";
    resultsPath = "";
    posResultsPath = "";
    rawPath = "";
    testName = "";
    chipDisconnect = false;
    warningDisplayed = false;
    minReading =0;
    maxReading = 0;
}

bool testData::addSaveFileEntry(double (&data)[8])
{

    sampNum = sampNum +1;
    QString tempStr;
    int error = 0;

    if (insMarker == true)
    {
        tempStr = ">>>\t" + QString::number(sampNum-1) + "\t" + QString::number(data[0])+ "\t" + QString::number(data[1])+ "\t" + QString::number(data[2])+ "\t" + QString::number(data[3])+ "\t" + QString::number(data[4])+ "\t" + QString::number(data[5])+ "\t" + QString::number(data[6])+ "\t" + QString::number(data[7]) + "\t" + QString::number(recentTemp) + "\r\n";

        insMarker = false;
    }
    else
    {
        tempStr = "\t" + QString::number(sampNum-1) + "\t" + QString::number(data[0])+ "\t" + QString::number(data[1])+ "\t" + QString::number(data[2])+ "\t" + QString::number(data[3])+ "\t" + QString::number(data[4])+ "\t" + QString::number(data[5])+ "\t" + QString::number(data[6])+ "\t" + QString::number(data[7]) + "\t" + QString::number(recentTemp) + "\r\n";
    }

    //Open text file for appending
    if (saveFile.isOpen() == false)
    {
        saveFile.open(QIODevice::Append | QIODevice::Text);
        QTextStream  saveStream(&saveFile);

    }


    //Make sure the file opened alright
    if(saveFile.isOpen() == false)
    {
        QByteArray ba = savePath.toLatin1();
        error =1;
    }

    saveStream << tempStr;
    saveStream.flush();

    //Check that data was written
    if (saveStream.status() != QTextStream::Ok)
    {
        error = 1;
    }

    if (error == 1)
    {
        error = 3;

    }


    measLeft = measLeft - 1;

    return true;
}

bool testData::addRawFileEntry(double (&data)[8])
{
    if (rawFile.isOpen() == false)
    {
        QFile rawFile(rawPath);
        QTextStream rawStream(&rawFile);
    }


    QString tempStr = QString::number(data[0])+ "\t" + QString::number(data[1])+ "\t" + QString::number(data[2])+ "\t" + QString::number(data[3])+ "\t" + QString::number(data[4])+ "\t" + QString::number(data[5])+ "\t" + QString::number(data[6])+ "\t" + QString::number(data[7]) + "\n";

    rawStream << tempStr;
    rawStream.flush();

    return true;
}

//Call this function to renormalize the raw data at the last measured point
void testData::startPosControlRaw()
{

    rawFile.close();

    //If file exists, return false (no overwrite)
    QDir::setCurrent("/home/root/raw");
    rawFile.setFileName(testName + ".txt");



    if(rawFile.remove() == false)
    {
        int x =1;   //error removing file
    }

    rawFile.open(QIODevice::ReadWrite | QIODevice::Text);   //Need R/W for moving window

    QTextStream rawStream(&rawFile);

    QString tempStr = QString::number(recentDataArr[0])+ "\t" + QString::number(recentDataArr[1])+ "\t" + QString::number(recentDataArr[2])+ "\t" + QString::number(recentDataArr[3])+ "\t" + QString::number(recentDataArr[4])+ "\t" + QString::number(recentDataArr[5])+ "\t" + QString::number(recentDataArr[6])+ "\t" + QString::number(recentDataArr[7]) + "\n";

    rawStream << tempStr;
    rawStream.flush();

}

//Attempts to open config file and load application globals with read values
//If no file is found, the default values will be loaded
bool testData::readConfigFile()
{

    QFile cfgFile("/home/root/ESB.cfg");

    //Set all defaults
    controlDuration = 60;
    testDuration = 60;
    posCtrlDuration = 60;
    movWinConfidence = 99.9;
    tempSetpoint = 35;
    tecEn = true;
    minImpedance = 350;
    maxImpedance = 7500;
    validImpedanceRange = 2000;
    timeDelay = 5;
    instSerial = "";
    maxImpRun = 4000;
    minImpRun = 300;
    controlName = "";
    shutoffTime = 60;
    enableAutoShutoff = true;
    rcTol = 10;


    //file does not exist
    if (cfgFile.exists() == false)
    {

        cfgFile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream cfgStream(&cfgFile);

        //create new configuration file

        cfgStream << "ControlDuration\t" << controlDuration << "\n";
        cfgStream << "TestDuration\t" << testDuration << "\n";
        cfgStream << "PositiveControlDuration\t" << posCtrlDuration << "\n";
        cfgStream << "MovWinConf\t"<< movWinConfidence << "\n";
        cfgStream << "Temp\t"<< tempSetpoint << "\n";
        cfgStream << "TecEn\t"<< tecEn << "\n";
        cfgStream << "MinImpedance\t" << minImpedance << "\n";
        cfgStream << "MaxImpedance\t" << maxImpedance << "\n";
        cfgStream << "ValidImpedanceRange\t" << validImpedanceRange << "\n";
        cfgStream << "TimeDelay\t" << timeDelay << "\n";
        cfgStream << "InstrumentSerial\t" << instSerial << "\n";
        cfgStream << "MaxImpedanceRuntime\t" << maxImpRun << "\n";
        cfgStream << "MinImpedanceRuntime\t" << minImpRun << "\n";
        cfgStream << "ControlName\t" << controlName << "\n";
        cfgStream << "ShutoffTime\t" << shutoffTime << "\n";
        cfgStream << "EnableAutoShutoff\t" << enableAutoShutoff << "\n";
        cfgStream << "RCTol\t" << rcTol<< "\n";
        cfgStream << "SoftwareVersion\t" << version << "\t" << date;

        cfgFile.close();


    }
    else
    {

        //File exists, read values to global vars

        cfgFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream cfgStream(&cfgFile);

        while (!cfgStream.atEnd())
        {
            QString line = cfgStream.readLine();
            QStringList tokens = line.split('\t');

            if (QString::compare("ControlDuration",tokens.at(0))==0)
                controlDuration = tokens.at(1).toInt();

            if (QString::compare("TestDuration",tokens.at(0))==0)
                testDuration = tokens.at(1).toInt();

            if (QString::compare("PositiveControlDuration",tokens.at(0))==0)
                posCtrlDuration = tokens.at(1).toInt();

            if (QString::compare("MovWinConf",tokens.at(0))==0)
                movWinConfidence = tokens.at(1).toDouble();

            if (QString::compare("Temp",tokens.at(0))==0)
                tempSetpoint = tokens.at(1).toInt();

            if (QString::compare("TecEn",tokens.at(0))==0)
            {
                if (tokens.at(1).toInt() == 1)
                    tecEn = true;
                else
                    tecEn = false;
            }

            if (QString::compare("MinImpedance",tokens.at(0))==0)
                minImpedance = tokens.at(1).toInt();

            if (QString::compare("MaxImpedance",tokens.at(0))==0)
                maxImpedance = tokens.at(1).toInt();

            if (QString::compare("ValidImpedanceRange",tokens.at(0))==0)
                validImpedanceRange = tokens.at(1).toInt();

            if (QString::compare("TimeDelay",tokens.at(0))==0)
                timeDelay = tokens.at(1).toInt();

            if (QString::compare("InstrumentSerial",tokens.at(0))==0)
                instSerial = tokens.at(1);

            if (QString::compare("MaxImpedanceRuntime",tokens.at(0))==0)
                maxImpRun = tokens.at(1).toInt();

            if (QString::compare("MinImpedanceRuntime",tokens.at(0))==0)
                minImpRun = tokens.at(1).toInt();

            if (QString::compare("ControlName",tokens.at(0))==0)
                controlName = tokens.at(1);

            if (QString::compare("ShutoffTime",tokens.at(0))==0)
                shutoffTime = tokens.at(1).toInt();

            if (QString::compare("EnableAutoShutoff",tokens.at(0))==0)
            {
                if (tokens.at(1).toInt() == 1)
                    enableAutoShutoff = true;
                else
                    enableAutoShutoff = false;
            }

            if (QString::compare("RCTol",tokens.at(0))==0)
                rcTol = tokens.at(1).toDouble();

            if (QString::compare("SoftwareVersion",tokens.at(0))==0)
            {
                version = tokens.at(1);
                date = tokens.at(2);
            }


        }

        cfgFile.close();


    }

    //Check to see if software was updated
    if (QString::compare(version,SOFTWARE_VER) != 0)
    {
        updated = true;
        version = SOFTWARE_VER;
        date = SOFTWARE_DATE;
        writeConfigFile();
    }


    controlCfgPath = "/home/root/Resources/" + controlName;
    return true;
}

bool testData::writeConfigFile()
{

    //create new configuration file
    QFile cfgFile("/home/root/ESB.cfg");
    cfgFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream cfgStream(&cfgFile);

    //create new configuration file

    cfgStream << "ControlDuration\t" << controlDuration << "\n";
    cfgStream << "TestDuration\t" << testDuration << "\n";
    cfgStream << "PositiveControlDuration\t" << posCtrlDuration << "\n";
    cfgStream << "MovWinConf\t"<< movWinConfidence << "\n";
    cfgStream << "Temp\t"<< tempSetpoint << "\n";
    cfgStream << "TecEn\t"<< tecEn << "\n";
    cfgStream << "MinImpedance\t" << minImpedance << "\n";
    cfgStream << "MaxImpedance\t" << maxImpedance << "\n";
    cfgStream << "ValidImpedanceRange\t" << validImpedanceRange << "\n";
    cfgStream << "TimeDelay\t" << timeDelay << "\n";
    cfgStream << "InstrumentSerial\t" << instSerial << "\n";
    cfgStream << "MaxImpedanceRuntime\t" << maxImpRun << "\n";
    cfgStream << "MinImpedanceRuntime\t" << minImpRun << "\n";
    cfgStream << "ControlName\t" << controlName << "\n";
    cfgStream << "ShutoffTime\t" << shutoffTime << "\n";
    cfgStream << "EnableAutoShutoff\t" << enableAutoShutoff << "\n";
    cfgStream << "RCTol\t" << rcTol<< "\n";
    cfgStream << "SoftwareVersion\t" << version << "\t" << date;


    cfgFile.close();

    controlCfgPath = "/home/root/Resources/" + controlName;

    return true;
}

void testData::insertMarker()
{
    insMarker = true;
}

//Utility function adds test results to test records file
 void testData::addResultToRecords(bool contaminated)
 {
     //Get Date and Time
     clockCtrl = new clockcontroliic();
     clockCtrl->readDateTime();
     struct dt_clock dt = clockCtrl->dateTime;

     QString timeStr =  QString::number(dt.date)+"-"+QString::number(dt.month)+
             "-"+QString::number(dt.year+2000) + " "+QString::number(dt.hours)+
             ":"+QString::number(dt.minutes);


     //Add entry to file
     QFile recordFile(testRecordPath);

     recordFile.open(QIODevice::Append | QIODevice::Text);
     QTextStream recordStream(&recordFile);

     if (contaminated)
         recordStream << testName + "\t\t" + timeStr + "\t\t" + "CONTAMINATED\n";
     else
         recordStream << testName + "\t\t" + timeStr + "\t\t" + "NO CONTAMINATION DETECTED\n";

     recordFile.close();
 }
