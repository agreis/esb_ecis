//Description:Class Definition for testData
//  Contains references to all files, contains global application vars, provides abstraction for file I/O
//Author: Alex Greis
//Date: 7/2/2013

#ifndef TESTDATA_H
#define TESTDATA_H

#include <fstream>
#include <iostream>
#include <QtCore>
#include <QFile>
#include <usbmounter.h>
#include "clockcontroliic.h"

#define SOFTWARE_VER "3.0.7"
#define SOFTWARE_DATE "11/16/2018"

class testData
{

public:

    testData();

    void insertMarker();
    bool initSaveFiles(QString name);
    bool closeSaveFiles();
    bool addSaveFileEntry(double (&data)[8]);
    bool addRawFileEntry(double (&data)[8]);
    bool insertNormPoint();
    bool readConfigFile();
    bool writeConfigFile();
    void reInit();
    void addResultToRecords(bool contaminated);
    void startPosControlRaw();

    enum applicationState
    {
        INIT,
        IMP_CHECK_IN_PROGRESS,
        IMP_CHECK_FAIL,
        SAMPLE_NAME_INPUT,
        PRE_EXPOSURE,
        PRE_EXPOSURE_COMPLETE,
        TEST_EXPOSURE,
        TEST_EXPOSURE_COMPLETE,
        POSITIVE_CONTROL_EXPOSURE,
        POSITIVE_CONTROL_COMPLETE,
        RC_TEST
    };

    //Global test vars
    int controlDuration;
    int testDuration;
    int posCtrlDuration;
    double movWinConfidence;
    int tempSetpoint;
    bool tecEn;
    int minImpedance;   //precheck
    int maxImpedance;   //precheck
    int validImpedanceRange;
    int timeDelay;
    applicationState currentState;
    double recentDataArr[8];
    QString instSerial;
    int maxImpRun;      //runtime error
    int minImpRun;      //runtime error
    QString controlName;    //Control configuration file name
    int shutoffTime;        //idle time till shutoff
    bool enableAutoShutoff; //enable autoshutoff if idle
    double rcTol;           //Tolerance on the RC Test
    QString version;
    QString date;
    bool updated;

    //Global runtime vars
    int secondsLeft;    //seconds left in current test state
    int measLeft;       //number of measurements remaining for current test state
    double recentTemp;  //most recent temperature measurement
    int numMeas;        //Used to keep track of how many measurement intervals have been performed, used to determine when to execute runtime analysis
    bool chipDisconnect;    //global flag that is set when a chip disconnect interrupt is detected
    bool warningDisplayed;  //global flag indicating whether or not disconnect popup has already been displayed since triggered
    bool analysisRunning;   //designates that the moving window algorithm is running

    //Graph variables
    bool autoScale, normalized, enNormalized;
    double maxReading, minReading;
    double maxNormReading, minNormReading;
    double yMax, yMin;

    //Save file paths
    QString savePath;
    QString rawPath;
    QString resultsPath;
    QString posResultsPath;
    QString controlCfgPath;
    QString testRecordPath;
    QString testName;


    //DEBUG VARS
    bool DEBUG_sbcOnly; //Used if debugging with only sbc, disables hardware checks and simulates measurements


    int sampNum;


    QTextStream saveStream;
    QTextStream rawStream;

private:

    QFile rawFile;   //raw file for input into moving window algorithm
    QFile saveFile;  //Save file with formatting/headers/etc, test output
    QFile  cfgFile;   //Config file for application settings




    bool insMarker;
    clockcontroliic *clockCtrl;


};

#endif // TESTDATA_H
