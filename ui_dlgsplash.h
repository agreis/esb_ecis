/********************************************************************************
** Form generated from reading UI file 'dlgsplash.ui'
**
** Created: Wed Nov 29 15:15:03 2017
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSPLASH_H
#define UI_DLGSPLASH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_dlgSplash
{
public:
    QLabel *label;

    void setupUi(QDialog *dlgSplash)
    {
        if (dlgSplash->objectName().isEmpty())
            dlgSplash->setObjectName(QString::fromUtf8("dlgSplash"));
        dlgSplash->resize(1024, 600);
        label = new QLabel(dlgSplash);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(330, 260, 351, 91));
        QFont font;
        font.setPointSize(24);
        label->setFont(font);

        retranslateUi(dlgSplash);

        QMetaObject::connectSlotsByName(dlgSplash);
    } // setupUi

    void retranslateUi(QDialog *dlgSplash)
    {
        dlgSplash->setWindowTitle(QApplication::translate("dlgSplash", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("dlgSplash", "Loading...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class dlgSplash: public Ui_dlgSplash {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGSPLASH_H
