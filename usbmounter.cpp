#include "usbmounter.h"

usbMounter::usbMounter()
{
}

//Scan the list of disks, if a USB drive is connected try to mount
void usbMounter::updateUsbMount()
{
    QProcess proc;

    proc.start("fdisk -l");

    //Wait for the process to complete
    proc.waitForFinished(3000);

    //Read output
    QString response = QString::fromAscii(proc.readAll());
    QStringList lines = response.split("\n");
    for (int i=0; i< lines.size();i++)
    {
        //look for /dev/* entries
        if (QString::compare(lines.at(i).section("",0,5),"/dev/") == 0)
        {
            //We known that /dev/mmcblk0p1 and /dev/mmcblk0p2 are both system devices, but
            //Assume any other dev is a usb storage drive
            bool systemDrive = false;

            if (QString::compare(lines.at(i).section("",0,14),"/dev/mmcblk0p1") == 0)
                systemDrive = true;
            if (QString::compare(lines.at(i).section("",0,14),"/dev/mmcblk0p2") == 0)
                systemDrive = true;

            //Found a USB disk
            if (!systemDrive)
            {
                //Build a command based upon the disk name
                QStringList dlist = lines.at(i).split(" ");
                QString command = "mount " + dlist.at(0) + " /home/root/usb/";

                //Attempt to mount
                proc.start(command);
                proc.waitForFinished(7000);

                //Read output
                QString mount_response = QString::fromAscii(proc.readAllStandardError());

                emit txUsbStatus(true);
                return;
            }
        }
    }

    emit txUsbStatus(false);

}
