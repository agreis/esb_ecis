#ifndef USBMOUNTER_H
#define USBMOUNTER_H

#include <QObject>
#include <QDebug>
#include <QProcess>

class usbMounter: public QObject
{
    Q_OBJECT

public:
    usbMounter();
public slots:
    void updateUsbMount();
signals:
    void txUsbStatus(bool mounted);
};

#endif // USBMOUNTER_H
